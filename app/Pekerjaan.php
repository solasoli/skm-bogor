<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
  protected $table = "mst_pekerjaan";
  protected $primaryKey = "id";
  public $timestamps = false;
}
