<?php


namespace App;


use App\Traits\SoftDelete;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SrvPertanyaan extends Model
{
    protected $table = 'srv_pertanyaan';

    use SoftDeletes;
    use SoftDelete;

    protected $fillable = [
        'guid',
        'id_tipe_pertanyaan',
        'nama',
        'id_skpd',
        'is_custom',
        'created_by',
        'updated_by',
        'id_pertanyaan_unsur'
    ];

    public function srvPertanyaanUnsur()
    {
        return $this->belongsTo(SrvPertanyaanUnsur::class, 'id_pertanyaan_unsur');
    }

    public function srvGroup()
    {
        return $this->belongsToMany(SrvGroup::class, 'srv_grup_pertanyaan', 'id_pertanyaan', 'id_grup');
    }

    public function srvJawaban()
    {
        return $this->belongsToMany(SrvJawaban::class, 'srv_survey_hasil_detail', 'id_pertanyaan', 'id_jawaban');
    }

    public function srvSurveyHasil()
    {
        return $this->belongsToMany(SrvSurveyHasil::class, 'srv_susrvey_hasil_detail', 'id_pertanyaan', 'id_hasil');
    }
}
