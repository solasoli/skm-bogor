<?php

namespace App\Srv;

use Illuminate\Database\Eloquent\Model;

class GrupPertanyaan extends Model
{
  protected $table = "srv_grup_pertanyaan";
  public $timestamps = false;
}
