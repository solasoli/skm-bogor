<?php

namespace App\Srv;

use App\SrvGroup;
use Illuminate\Database\Eloquent\Model;

class SurveyHasil extends Model
{
  protected $table = "srv_survey_hasil";
  public $timestamps = false;

  
  public function group()
  {
      return $this->belongsTo(SrvGroup::class, 'id_grup');
  }
}
