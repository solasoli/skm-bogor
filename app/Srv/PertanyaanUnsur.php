<?php

namespace App\Srv;

use Illuminate\Database\Eloquent\Model;

class PertanyaanUnsur extends Model
{
  protected $table = "srv_pertanyaan_unsur";
  public $timestamps = false;
}
