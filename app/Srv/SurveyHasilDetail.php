<?php

namespace App\Srv;

use Illuminate\Database\Eloquent\Model;

class SurveyHasilDetail extends Model
{
  protected $table = "srv_survey_hasil_detail";
  public $timestamps = false;
}
