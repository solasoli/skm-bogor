<?php

namespace App;

use App\Traits\SoftDelete;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use SoftDelete;
    const ID_ADMIN = 1;

    protected $table = "acl_role";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function getMenu()
    {
        $menu = Menu::whereHas('permissions', function($permissionQuery) {
            if (!isAdmin())
                $permissionQuery->where('view', 1);

            return $permissionQuery;
        })->with('childrenMenu');

        if(!$this->id == self::ID_ADMIN)
            $menu->where('id_role', $this->id);

        return $menu->get();
    }

    public function isAbleToAccessMenuWithRoute($routeName)
    {
        $menu = $this->getMenu();
        
        return $menu->filter(function(Menu $menuItem) use ($routeName) {
            return $menuItem->hasMenuWithUrl($routeName);
        })->isNotEmpty();
    }


}
