<?php


namespace App\Traits;


use Illuminate\Database\Eloquent\Builder;

trait SoftDelete
{
    protected static function boot()
    {
        parent::boot();

        $className = self::class;
        $tableName = (new $className())->table;
        static::addGlobalScope('soft_delete', function (Builder $builder) use ($tableName) {
            $builder->where($tableName. '.is_deleted', false);
        });
    }
}
