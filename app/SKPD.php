<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SKPD extends Model
{
  protected $table = "mst_skpd";

  protected $fillable = ['name'];
  protected $primaryKey = "id";
  public $timestamps = false;


  public function srvGroup() {
      return $this->hasMany(SrvGroup::class, 'id_skpd');
  }

  public function users()
  {
      return $this->hasMany(User::class, 'id_skpd');
  }
}
