<?php

namespace App\Http\Controllers;

use App\SrvGroup;
use App\SrvSurveyHasil;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

class RespondentController extends Controller
{
    public function dataTable()
    {
        $query = SrvSurveyHasil::query();

        $skpdIds = get_skpd_by_login()->pluck('id');

        $srvGroupIds = SrvGroup::whereIn('id_skpd', $skpdIds)->pluck('id');

        return datatables($query->whereIn('id_grup', $srvGroupIds))->toJson();
    }
}
