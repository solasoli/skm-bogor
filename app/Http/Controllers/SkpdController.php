<?php

namespace App\Http\Controllers;

use App\SKPD;
use Illuminate\Http\Request;
use Datatables;
use Illuminate\Support\Facades\DB;

class SkpdController extends Controller
{
    public function index()
    {
        return view('master/skpd');
    }

    public function getSkpd()
    {
        $data = DB::table('mst_skpd')->selectRaw('id, name')->get();

        return Datatables::of($data)
        ->addColumn('aksi', function($data) {
            $button = '<a href="/master/skpd/edit/'. $data->id .'" class="btn btn-warning btn-sm">Edit</a>';
            $button .= '<a href="/master/skpd/delete/'. $data->id .'" class="btn btn-danger btn-sm">Delete</a>';
            return $button;
        })
        ->rawColumns(['aksi'])
        ->make(true);
    }

    public function store(Request $request) 
    {
        $data = SKPD::create(['name' => $request->skpd]);
        
        if($data) {
            return redirect('/master/skpd')->with(['successAdd' => 'Data bethasil ditambahkan']);
        }
    }

    public function destroy($id)
    {
        $data = SKPD::find($id)->delete();
        
        if($data) {
            return redirect('/master/skpd')->with(['successDel' => 'Data bethasil ditambahkan']);
        }
    }

    public function edit($id)
    {
        $data = DB::table('mst_skpd')->selectRaw('*')->where('id', $id)->get();

        return view('/master/skpd-edit', [
            'data' => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = DB::table('mst_skpd')
            ->where('id', $id)
            ->update(['name' => $request->skpd]);

        if($data) {
            return redirect('/master/skpd')->with(['successUpdt' => 'Data bethasil ditambahkan']);
        }
    }

    public function skpdWithoutSurvey($year)
    {
        $skpds = SKPD::whereDoesntHave('srvGroup', function ($srvGroupQuery) use($year) {
            return $srvGroupQuery->where('tahun', $year);
        });

        return datatables($skpds)->toJson();
    }

}
