<?php

namespace App\Http\Controllers\Srv;

Use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables;
use Validator;
use Auth;
use Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;
use App\Srv\Pertanyaan;
use App\Srv\PertanyaanTipe;
use App\Srv\Jawaban;
use App\Konfigurasi;
use App\Srv\PertanyaanUnsur;

date_default_timezone_set('Asia/Jakarta');

class PertanyaanController extends Controller
{
    public function index()
    {
      $konfigurasi = Konfigurasi::first();
      return view('srv.pertanyaan-list',[
        'konfigurasi' => $konfigurasi
      ]);
    }

    public function create()
    {
      $default_jawaban = get_default_jawaban();
      $skpd = get_skpd_by_login();
      $konfigurasi = Konfigurasi::first();
      $pertanyaan_tipe = PertanyaanTipe::where('is_deleted', 0)->get();
      $pertanyaan_unsur = PertanyaanUnsur::where('is_deleted', 0)->get();

      return view('srv.pertanyaan-form',[
        'skpd' => $skpd,
        'default_jawaban' => $default_jawaban,
        'pertanyaan_tipe' => $pertanyaan_tipe,
        'konfigurasi' => $konfigurasi,
        'pertanyaan_unsur' => $pertanyaan_unsur
      ]);
    }

    public function store(Request $request)
    {
      $logged_user = Auth::user();

      $validator = Validator::make($request->all(), [
        //'skpd' => 'required',
        'pertanyaan' => 'required',
        'pertanyaan_unsur' => 'required',
        'pilihan.*' => 'required',
        'jawaban.*' => 'required',
        'bobot.*' => 'required|numeric',
      ],[
        'pertanyaan.required' => 'Pertanyaan harus diisi!',
        'pilihan.required' => 'Pilihan harus diisi!',
        'jawaban.required' => 'Jawaban harus diisi!',
        'pertanyaan_unsur.required' => 'Unsur Layanan harus diisi!',
        'bobot.required' => 'Bobot harus diisi!',
        'bobot.numeric' => 'Bobot harus diisi angka!',
      ]);

      if($validator->fails()){
        return back()
              ->withErrors($validator)
              ->withInput();
      }

      $id_tipe_pertanyaan = PertanyaanTipe::where('guid', $request->input('tipe_pertanyaan'))->get()->first();

      //check if tipe pertanyaan is exist?
      //make sure id pertanyaan is exist
      $id_tipe_pertanyaan = isset($id_tipe_pertanyaan->id) ? $id_tipe_pertanyaan->id : 0;

      //if not exist then show 404 , make sure id is retrievable
      $id_tipe_pertanyaan = PertanyaanTipe::findOrFail($id_tipe_pertanyaan);
      $id_tipe_pertanyaan = $id_tipe_pertanyaan->id;



      /* Proses insert ke table srv_pertanyaan */
      $t = new Pertanyaan;
      $t->nama = $request->input('pertanyaan');
      $t->id_skpd = $request->input('skpd');
      $t->id_pertanyaan_unsur = $request->input('pertanyaan_unsur');
      $t->guid = Str::uuid();
      $t->id_tipe_pertanyaan = $id_tipe_pertanyaan;
      $t->created_at = date('Y-m-d H:i:s');
      $t->created_by = Auth::id();
      $t->updated_at = NULL;
      $t->updated_by = 0;
      $t->deleted_at = NULL;
      $t->deleted_by = 0;
      $t->is_deleted = 0;
      $t->save();

      foreach($request->input('pilihan') AS $idx => $val){
        /* Proses insert ke table srv_jawaban */
        $t2 = new Jawaban;
        $t2->id_pertanyaan = $t->id;
        $t2->pilihan = $val;
        $t2->nama = $request->input('jawaban')[$idx];
        $t2->bobot = $request->input('bobot')[$idx];
        $t2->is_deleted = 0;
        $t2->save();
      }

      $request->session()->flash('message', "Data berhasil disimpan!");
      return redirect('/srv/pertanyaan');
    }

    public function edit($id)
    {
      $logged_user = Auth::user();

      $data = $this->get_pertanyaan_by_id($id);
      $skpd = get_skpd_by_login();
      $konfigurasi = Konfigurasi::first();
      $pertanyaan_tipe = PertanyaanTipe::where('is_deleted', 0)->get();
      $pertanyaan_unsur = PertanyaanUnsur::where('is_deleted', 0)->get();

      return view('srv.pertanyaan-form', [
        'data' => $data,
        'skpd' => $skpd,
        'konfigurasi' => $konfigurasi,
        'pertanyaan_tipe' => $pertanyaan_tipe,
        'pertanyaan_unsur' => $pertanyaan_unsur
      ]);
    }

    public function update(Request $request, $id)
    {
      $logged_user = Auth::user();

      $validator = Validator::make($request->all(), [
        //'skpd' => 'required',
        'pertanyaan' => 'required',
        'pertanyaan_unsur' => 'required',
        'pilihan.*' => 'required',
        'jawaban.*' => 'required',
        'bobot.*' => 'required|numeric',
      ],[
        'pertanyaan.required' => 'Pertanyaan harus diisi!',
        'pilihan.required' => 'Pilihan harus diisi!',
        'jawaban.required' => 'Jawaban harus diisi!',
        'pertanyaan_unsur.required' => 'Unsur Layanan harus diisi!',
        'bobot.required' => 'Bobot harus diisi!',
        'bobot.numeric' => 'Bobot harus diisi angka!',
      ]);

      if($validator->fails()){
        return back()
              ->withErrors($validator)
              ->withInput();
      }

      $id_tipe_pertanyaan = PertanyaanTipe::where('guid', $request->input('tipe_pertanyaan'))->get()->first();

      //check if tipe pertanyaan is exist?
      //make sure id pertanyaan is exist
      $id_tipe_pertanyaan = isset($id_tipe_pertanyaan->id) ? $id_tipe_pertanyaan->id : 0;

      //if not exist then show 404 , make sure id is retrievable
      $id_tipe_pertanyaan = PertanyaanTipe::findOrFail($id_tipe_pertanyaan);
      $id_tipe_pertanyaan = $id_tipe_pertanyaan->id;

      /* STEP 1: Update pertanyaan di table srv_pertanyaan */
      $t = Pertanyaan::findOrFail($id);
      $t->nama = $request->input('pertanyaan');
      $t->id_pertanyaan_unsur = $request->input('pertanyaan_unsur');
      //$t->id_skpd = $request->input('skpd');
      $t->id_tipe_pertanyaan = $id_tipe_pertanyaan;
      $t->updated_at = date('Y-m-d H:i:s');
      $t->updated_by = Auth::id();
      $t->save();

      /* STEP 2: Soft delete srv_jawaban where id_pertanyaan = $id */
      DB::table('srv_jawaban')
        ->where('id_pertanyaan', $id)
        ->update(['is_deleted' => 1]);

      /* STEP 3: Insert data ke srv_jawaban */
      foreach($request->input('pilihan') AS $idx => $val){
        $t3 = new Jawaban;
        $t3->id_pertanyaan = $t->id;
        $t3->pilihan = $val;
        $t3->nama = $request->input('jawaban')[$idx];
        $t3->bobot = $request->input('bobot')[$idx];
        $t3->is_deleted = 0;
        $t3->save();
      }

      $request->session()->flash('message', "Data berhasil diubah!");
      return redirect("/srv/pertanyaan/edit/{$id}");

    }

    public function destroy(Request $request, $id)
    {
      $logged_user = Auth::user();

      /* Soft delete srv_pertanyaan */
      $t = Pertanyaan::findOrFail($id);
      $t->deleted_at = date('Y-m-d H:i:s');
      $t->deleted_by = Auth::id();
      $t->is_deleted = 1;
      $t->save();

      /* Soft delete srv_jawaban where id_pertanyaan = $id */
      DB::table('srv_jawaban')
        ->where('id_pertanyaan', $id)
        ->update(['is_deleted' => 1]);

      $request->session()->flash('message', "Data berhasil dihapus!");
      return redirect('/srv/pertanyaan');
    }

    public function list_datatables_api()
    {
      $data = DB::table("srv_pertanyaan AS p")
      ->select(DB::raw("p.id, p.nama AS pertanyaan, /*s.name AS skpd,*/ pt.nama AS tipe_pertanyaan, pu.nama AS pertanyaan_unsur"))
      //->join('mst_skpd AS s', 'p.id_skpd', '=', 's.id')
      ->join('srv_pertanyaan_tipe AS pt', 'p.id_tipe_pertanyaan', '=', 'pt.id')
      ->join('srv_pertanyaan_unsur AS pu', 'p.id_pertanyaan_unsur', '=', 'pu.id')
      ->where('p.is_deleted', 0)
      ->where('p.is_custom', 0);
      return Datatables::of($data)->make(true);
    }

    public function get_pertanyaan_by_id($id)
    {
      $data = DB::table("srv_pertanyaan AS p")
      ->select(DB::raw("p.id, p.id_pertanyaan_unsur, p.nama AS pertanyaan, pu.nama AS pertanyaan_unsur,p.id_skpd, p.id_tipe_pertanyaan, pt.nama AS tipe_pertanyaan, /*s.name AS skpd,*/ j.pilihan, j.nama AS jawaban, j.bobot"))
      ->join('srv_jawaban AS j', 'j.id_pertanyaan', '=', 'p.id')
      //->join('mst_skpd AS s', 'p.id_skpd', '=', 's.id')
      ->join('srv_pertanyaan_unsur AS pu', 'p.id_pertanyaan_unsur', '=', 'pu.id')
      ->join('srv_pertanyaan_tipe AS pt', 'p.id_tipe_pertanyaan', '=', 'pt.id')
      ->where('p.is_deleted', 0)
      ->where('j.is_deleted', 0)
      ->where('p.id', $id)
      ->orderBy('p.id', 'ASC')
      ->get();

      return $data;
    }
}
