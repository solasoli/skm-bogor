<?php

namespace App\Http\Controllers\Srv;

Use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables;
use Validator;
use Auth;
use Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;
use App\Pendidikan;
use App\Pekerjaan;
use App\Srv\Grup;
use App\Srv\Pertanyaan;
use App\Srv\Jawaban;
use App\Srv\SurveyHasil;
use App\Srv\SurveyHasilDetail;

date_default_timezone_set('Asia/Jakarta');

class SurveyController extends Controller
{
    public function index()
    {
    }

    public function base_create($slug) {

      $grup = Grup::where("is_deleted", 0)->where("slug", $slug)->first();

      $grup = Grup::findOrFail($grup->id);

      $pertanyaan = DB::table('srv_pertanyaan AS p')
      ->select(DB::raw('p.id, p.guid, p.nama AS pertanyaan,p.id_tipe_pertanyaan'))
      ->join('srv_grup_pertanyaan AS gp', 'p.id', '=', 'gp.id_pertanyaan')
      ->join('srv_grup AS g', 'g.id', '=', 'gp.id_grup')
      ->where('p.is_deleted', 0)
      ->where('gp.is_deleted', 0)
      ->where('g.is_deleted', 0)
      ->where('g.slug', $slug)
      ->orderBy('gp.id', 'ASC')
      ->get();

      $jawaban = DB::table('srv_pertanyaan AS p')
      ->select(DB::raw('p.id AS id_pertanyaan, j.id AS id_jawaban, p.nama AS pertanyaan, j.pilihan, j.nama AS label'))
      ->join('srv_grup_pertanyaan AS gp', 'p.id', '=', 'gp.id_pertanyaan')
      ->join('srv_grup AS g', 'g.id', '=', 'gp.id_grup')
      ->join('srv_jawaban AS j', 'p.id', '=', 'j.id_pertanyaan')
      ->where('p.is_deleted', 0)
      ->where('gp.is_deleted', 0)
      ->where('g.is_deleted', 0)
      ->where('j.is_deleted', 0)
      ->where('g.slug', $slug)
      ->groupBy("j.id")
      ->orderBy('p.id', 'ASC')
      ->get();

      $max_bobot = DB::table('srv_pertanyaan AS p')
      ->select(DB::raw('MAX(bobot) AS max_bobot'))
      ->join('srv_grup_pertanyaan AS gp', 'p.id', '=', 'gp.id_pertanyaan')
      ->join('srv_grup AS g', 'g.id', '=', 'gp.id_grup')
      ->join('srv_jawaban AS j', 'p.id', '=', 'j.id_pertanyaan')
      ->where('p.is_deleted', 0)
      ->where('gp.is_deleted', 0)
      ->where('g.is_deleted', 0)
      ->where('j.is_deleted', 0)
      ->where('g.slug', $slug)
      ->orderBy('p.id', 'ASC')
      ->first();


      // echo $pertanyaan->count();
      // die();

      $pendidikan = Pendidikan::where('is_deleted', 0)->get();
      $pekerjaan = Pekerjaan::where('is_deleted', 0)->get();
      return [
        'pendidikan' => $pendidikan,
        'pekerjaan' => $pekerjaan,
        'pertanyaan' => $pertanyaan,
        'jawaban' => $jawaban,
        'max_bobot' => isset($max_bobot->max_bobot) ? $max_bobot->max_bobot : 0 ,
        'jml_pertanyaan' => $pertanyaan->count(),
        'grup' => $grup
      ];
    }

    public function preview($slug)
    {

      $data = $this->base_create($slug);
      $data['is_preview'] = true;
      return view('srv.survey-form', $data);
    }

    public function create($slug)
    {
      $data = $this->base_create($slug);

      if($data['grup']->is_publish == 0){
        return view('srv.survey_404');
      } elseif($data['grup']->is_publish == 1) {
        return view('srv.survey-form', $data);
      }
    }

    public function store(Request $request, $slug)
    {

      $validator = Validator::make($request->all(), [
        'nama' => 'required',
        'alamat' => 'required',
        'usia' => 'required|numeric',
        'jk' => 'required',
        'pendidikan' => 'required',
        'pekerjaan' => 'required',
        'email' => 'email | nullable',
        'no_tlp' => 'required',

      ],[
        'nama.required' => 'Nama harus diisi !',
        'alamat.required' => 'Alamat harus diisi !',
        'usia.required' => 'Usia harus diisi !',
        'usia.numeric' => 'Usia harus diisi angka !',
        'jk.required' => 'Jenis kelamin harus diisi !',
        'pendidikan.required' => 'Pendidikan terakhir harus diisi !',
        'pekerjaan.required' => 'Pekerjaan harus diisi !',
        'no_tlp.required' => 'No. Telepon harus diisi !',
      ]);

      if($validator->fails()){
        return back()
              ->withErrors($validator)
              ->withInput();
      }

      $id_grup = Grup::where('slug', $slug)->where("is_deleted", 0)->first();
      $id_grup = isset($id_grup->id) ? $id_grup->id : 0;
      $id_grup = Grup::findOrFail($id_grup);
      $id_grup = $id_grup->id;

      /* Insert ke table srv_survey_hasil */
      $t = new SurveyHasil;
      $t->id_grup = $id_grup;
      // $t->jml_bobot diisinya nanti
      $t->nama = $request->input('nama');
      $t->alamat = $request->input('alamat');
      $t->usia = $request->input('usia');
      $t->jk = $request->input('jk');
      $t->id_pendidikan = $request->input('pendidikan');
      $t->id_pekerjaan = $request->input('pekerjaan');
      $t->email = $request->input('email');
      $t->no_tlp = $request->input('no_tlp');
      $t->created_at = date('Y-m-d H:i:s');
      $t->is_deleted = 0;
      $t->save();

      $jml_bobot = 0;
      $maximum_bobot = 0;
      if(!is_null($request->input('jawaban'))){
        foreach($request->input('jawaban') AS $idx => $val){


          $x = explode('_', $val);
          $id_pertanyaan = $x[0];
          $id_jawaban = $x[1];

          $pertanyaan = DB::table("srv_pertanyaan AS p")
          ->select(DB::raw("p.id, p.guid, MAX(j.bobot) max_bobot"))
          ->join("srv_jawaban AS j", "p.id", "j.id_pertanyaan")
          ->where("j.is_deleted",0)
          ->where("p.is_deleted",0)
          ->where("p.id", $id_pertanyaan)
          ->groupBy("p.id")
          ->get()
          ->first();
          if(isset($pertanyaan->id)){

            $jawaban = Jawaban::where('id', $id_jawaban)->first();
            if(isset($jawaban->id)){
              $maximum_bobot = $pertanyaan->max_bobot;

              /* Insert ke table srv_survey_hasil_detail */
              $t2 = new SurveyHasilDetail;
              $t2->id_hasil = $t->id;
              $t2->id_pertanyaan = $id_pertanyaan;
              $t2->id_jawaban = $id_jawaban;
              $t2->is_deleted = 0;
              $t2->save();

              $jml_bobot += $jawaban->bobot;
            }
          }
        }
      }


      if(!is_null($request->input('jawaban_bintang'))){
        foreach($request->input('jawaban_bintang') AS $idx => $val){
          //get pertanyaan first
          $pertanyaan = DB::table("srv_pertanyaan AS p")
          ->select(DB::raw("p.id, p.guid, MAX(j.bobot) max_bobot"))
          ->join("srv_jawaban AS j", "p.id", "j.id_pertanyaan")
          ->where("j.is_deleted",0)
          ->where("p.is_deleted",0)
          ->where("guid", $idx)
          ->groupBy("p.id")
          ->get()
          ->first();


          if(isset($pertanyaan->id)){
            $jawaban = Jawaban::where("id_pertanyaan", $pertanyaan->id)->where("bobot", $val)->get()->first();

            if(isset($jawaban->id)){
              $maximum_bobot += $pertanyaan->max_bobot;
              /* Insert ke table srv_survey_hasil_detail */
              $t2 = new SurveyHasilDetail;
              $t2->id_hasil = $t->id;
              $t2->id_pertanyaan = $pertanyaan->id;
              $t2->id_jawaban = $jawaban->id;
              $t2->is_deleted = 0;
              $t2->save();

              $jml_bobot += $val > $pertanyaan->max_bobot ? $pertanyaan->max_bobot : $val;
            }

          }
        }
      }


      /* Masukan jml_bobot di table srv_survey_hasil */
      DB::table('srv_survey_hasil')
        ->where('id', $t->id)
        ->update(['jml_bobot' => $jml_bobot,'max_bobot' => $maximum_bobot]);

        $id_survey = Crypt::encryptString($t->id);
        return redirect("/survey/terimakasih/{$id_survey}");
    }

    public function edit($id)
    {
      $crypt_id = $id;
      $id = Crypt::decryptString($id);

      $r = SurveyHasil::findOrFail($id);

      return view('srv.survey_thankyoupage-form', [
        'id_survey' => $id,
        'crypt_id' => $crypt_id,
        'data' => $r,
      ]);
    }

    public function update(Request $request, $id)
    {
      $id = Crypt::decryptString($id);

      $r = SurveyHasil::findOrFail($id);
      $r->kritik_saran = $request->input('kritik_saran');
      $r->save();

      $id_survey = Crypt::encryptString($id);
      return redirect("/survey/share/{$id_survey}");
    }


    public function share($id)
    {
      $id = Crypt::decryptString($id);

      $r = SurveyHasil::findOrFail($id);
      $grup = Grup::findOrFail($r->id_grup);
      return view('srv.survey_share', [
        'grup' => $grup,
      ]);
    }

    public function destroy(Request $request, $id)
    {
    }

    public function list_datatables_api()
    {
    }
}
