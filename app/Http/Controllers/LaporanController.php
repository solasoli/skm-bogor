<?php

namespace App\Http\Controllers;

Use App\Http\Controllers\Controller;
use App\Services\IKMReportingService;
use App\Services\QuestionElementValuesService;
use App\SKPD;
use App\SrvGroup;
use App\SrvPertanyaanUnsur;
use Illuminate\Http\Request;
use Datatables;
use Validator;
use Auth;
use Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;
use App\Pendidikan;
use App\Pekerjaan;
use App\Srv\Grup;
use App\Srv\Pertanyaan;
use App\Srv\Jawaban;
use App\Srv\SurveyHasil;
use App\Srv\SurveyHasilDetail;
use App\Srv\Penilaian;
use App\SrvSurveyHasil;

date_default_timezone_set('Asia/Jakarta');

class LaporanController extends Controller
{
    public function srv_tahunan(Request $request)
    {
      $skpd = get_skpd_by_login();
      $survey = Grup::where('is_deleted', 0)->get();
      $hit_survey_bulanan = [];
      $jumlah_available_bulan = 0;
      $selected_survey = [];
      $total_survey_tahunan = "";
      $total_bobot_per_unsur = [];
      $penilaian = [];
      $penilaian['nilai_interval_konversi'] = 0;
      $penilaian['show_bintang'] = 0;

      // jika sudah submit
      if($request->input('skpd') > 0){
        $selected_survey = Grup::findOrfail($request->input('survey'));

        /* START total pengisian survey tahunan */
        $total_survey_tahunan = DB::table('srv_survey_hasil AS hs')
        ->select(DB::raw("hs.id"))
        ->join('srv_grup AS g', 'g.id', '=', 'hs.id_grup')
        ->where('g.id_skpd', $request->input('skpd'))
        ->whereYear('hs.created_at', $request->input('tahun'))
        ->where('g.id', $request->input('survey'))
        ->where('hs.is_deleted', 0)
        ->get()->count();
        /* END */

        /* START Hit survey per bulan */
        $hasil_survey = DB::select(DB::raw(
          "SELECT MONTH(hs.created_at) AS bulan, CAST((SUM(hs.jml_bobot)/SUM(hs.max_bobot)*4) AS DECIMAL(12,2)) AS hit from srv_survey_hasil AS hs
          JOIN srv_grup AS g
          	ON g.id = hs.id_grup
          WHERE hs.is_deleted = 0 AND
          g.id = {$request->input('survey')} AND
          g.id_skpd = {$request->input('skpd')} AND
          YEAR(hs.created_at) = {$request->input('tahun')}
          GROUP BY month(hs.created_at)"
        ));

        foreach($hasil_survey AS $idx => $row){
          $hit_survey_bulanan[$row->bulan] = $row->hit;
          $jumlah_available_bulan++;
        }

        for($i=1; $i<=12; $i++){
          $hit_survey_bulanan[$i] = isset($hit_survey_bulanan[$i]) ? $hit_survey_bulanan[$i] : 0;
        }
        //$hit_survey_bulanan
        /* END */

        /* START Score per unsur pertanyaan*/
        $total_bobot_per_unsur = DB::table('srv_pertanyaan AS p')
        ->select(DB::raw("up.id AS id_unsur, up.nama AS unsur, ((SUM(j.bobot)/SUM(mb.max_bobot))*4) AS score"))
        ->join('srv_pertanyaan_unsur AS up', 'p.id_pertanyaan_unsur', '=', 'up.id')
        ->join('srv_jawaban AS j', 'j.id_pertanyaan', '=', 'p.id')
        ->join('srv_grup_pertanyaan AS gp', 'gp.id_pertanyaan', '=', 'p.id')
        ->join('srv_grup AS g', 'g.id', '=', 'gp.id_grup')
        ->join('srv_survey_hasil_detail AS shd', 'shd.id_jawaban', '=', 'j.id')
        ->join('srv_survey_hasil AS sh', function($join) {
          $join->on('sh.id', '=', 'shd.id_hasil')
          ->on('sh.id_grup', '=', 'g.id');
        })
        ->join(DB::raw("(SELECT
          MAX(bobot) max_bobot, jb.id_pertanyaan
          FROM srv_jawaban jb
          INNER JOIN srv_pertanyaan pb ON jb.id_pertanyaan = pb.id
          INNER JOIN srv_grup_pertanyaan gp ON gp.id_pertanyaan = pb.id
          WHERE gp.id_grup = '{$request->input('survey')}'
          GROUP BY jb.id_pertanyaan) AS mb"), 'mb.id_pertanyaan', '=', 'shd.id_pertanyaan')
        ->where('g.id', $request->input('survey'))
        ->where('g.id_skpd', $request->input('skpd'))
        ->whereYear('sh.created_at', $request->input('tahun'))
        //->where('p.is_deleted', 0)
        ->where('up.is_deleted', 0)
        //->where('j.is_deleted', 0)
        ->where('gp.is_deleted', 0)
        ->where('g.is_deleted', 0)
        ->where('sh.is_deleted', 0)
        ->where('shd.is_deleted', 0)
        ->groupBy('g.id')
        ->groupBy('up.id')
        ->orderBy('up.id', 'ASC')
        ->get();
        /* END */

        /* START IKM rumus => SUM(jml_bobot)/SUM(max_bobot)*100 */
        $q_hs = DB::table('srv_survey_hasil AS sh')
        ->select(DB::raw('SUM(sh.jml_bobot) AS sum_jml_bobot, SUM(sh.max_bobot) AS sum_max_bobot'))
        ->join('srv_grup AS g', 'g.id', '=', 'sh.id_grup')
        ->where('sh.is_deleted', 0)
        ->where('g.is_deleted', 0)
        ->where('g.id', $request->input('survey'))
        ->where('g.id_skpd', $request->input('skpd'))
        ->whereYear('sh.created_at', $request->input('tahun'))
        ->get()
        ->first();

        if(isset($q_hs->sum_jml_bobot)){
          $ikm = ($q_hs->sum_jml_bobot / $q_hs->sum_max_bobot) * 4;
          $ikm = round($ikm, 2);
          $penilaian['nilai_interval_konversi'] = $ikm;
          $penilaian['show_bintang'] = $ikm;
        }
        /* END */
      }

      return view('laporan.srv_tahunan', [
        'skpd' => $skpd,
        'survey' => $survey,
        'request' => $request->input(),
        'hit_survey_bulanan' => $hit_survey_bulanan,
        'jumlah_available_bulan' => $jumlah_available_bulan,
        'total_survey_tahunan' => $total_survey_tahunan,
        'selected_survey' => $selected_survey,
        'total_bobot_per_unsur' => $total_bobot_per_unsur,
        'penilaian' => $penilaian
      ]);
    }


    public function srv_bulanan(Request $request)
    {
      $skpd = get_skpd_by_login();
      $survey = Grup::where('is_deleted', 0)->get();
      $hit_survey_bulanan = [];
      $selected_survey = [];
      $total_survey_tahunan = "";
      $total_bobot_per_unsur = [];
      $kritiksaran = [];
      $penilaian = [];
      $penilaian['nilai_interval_konversi'] = 0;
      $penilaian['show_bintang'] = 0;

      // jika sudah submit
      if($request->input('skpd') > 0){
        $selected_survey = Grup::findOrfail($request->input('survey'));
        $bulan = $request->input('bulan')/1;

        /* START total pengisian survey tahunan */
        $total_survey_tahunan = DB::table('srv_survey_hasil AS hs')
        ->select(DB::raw("hs.id"))
        ->join('srv_grup AS g', 'g.id', '=', 'hs.id_grup')
        ->where('g.id_skpd', $request->input('skpd'))
        ->whereYear('hs.created_at', $request->input('tahun'))
        ->whereMonth('hs.created_at', $bulan)
        ->where('g.id', $request->input('survey'))
        ->where('hs.is_deleted', 0)
        ->get()->count();
        /* END */

        /* START Hit survey per bulan */
        $hasil_survey = DB::select(DB::raw(
          "SELECT DAY(hs.created_at) AS tanggal, CAST((SUM(hs.jml_bobot)/SUM(hs.max_bobot)*4) AS DECIMAL(12,2)) AS hit from srv_survey_hasil AS hs
          JOIN srv_grup AS g
            ON g.id = hs.id_grup
          WHERE hs.is_deleted = 0 AND
          g.id = {$request->input('survey')} AND
          g.id_skpd = {$request->input('skpd')} AND
          YEAR(hs.created_at) = {$request->input('tahun')} AND
          MONTH(hs.created_at) = {$bulan}
          GROUP BY DAY(hs.created_at)"
        ));

        foreach($hasil_survey AS $idx => $row){
          $hit_survey_bulanan[$row->tanggal] = $row->hit;
        }

        for($i=1; $i<=12; $i++){
          $hit_survey_bulanan[$i] = isset($hit_survey_bulanan[$i]) ? $hit_survey_bulanan[$i] : 0;
        }
        //$hit_survey_bulanan
        /* END */

        /* START Score per unsur pertanyaan*/
        $total_bobot_per_unsur = DB::table('srv_pertanyaan AS p')
        ->select(DB::raw("up.id AS id_unsur, up.nama AS unsur, ((SUM(j.bobot)/SUM(mb.max_bobot))*4) AS score"))
        ->join('srv_pertanyaan_unsur AS up', 'p.id_pertanyaan_unsur', '=', 'up.id')
        ->join('srv_jawaban AS j', 'j.id_pertanyaan', '=', 'p.id')
        ->join('srv_grup_pertanyaan AS gp', 'gp.id_pertanyaan', '=', 'p.id')
        ->join('srv_grup AS g', 'g.id', '=', 'gp.id_grup')
        ->join('srv_survey_hasil_detail AS shd', 'shd.id_jawaban', '=', 'j.id')
        ->join('srv_survey_hasil AS sh', function($join) {
          $join->on('sh.id', '=', 'shd.id_hasil')
          ->on('sh.id_grup', '=', 'g.id');
        })
        ->join(DB::raw("(SELECT
          MAX(bobot) max_bobot, jb.id_pertanyaan
          FROM srv_jawaban jb
          INNER JOIN srv_pertanyaan pb ON jb.id_pertanyaan = pb.id
          INNER JOIN srv_grup_pertanyaan gp ON gp.id_pertanyaan = pb.id
          WHERE gp.id_grup = '{$request->input('survey')}'
          GROUP BY jb.id_pertanyaan) AS mb"), 'mb.id_pertanyaan', '=', 'shd.id_pertanyaan')
        ->where('g.id', $request->input('survey'))
        ->where('g.id_skpd', $request->input('skpd'))
        ->whereYear('sh.created_at', $request->input('tahun'))
        ->whereMonth('sh.created_at', $bulan)
        //->where('p.is_deleted', 0)
        ->where('up.is_deleted', 0)
        //->where('j.is_deleted', 0)
        ->where('gp.is_deleted', 0)
        ->where('g.is_deleted', 0)
        ->where('sh.is_deleted', 0)
        ->where('shd.is_deleted', 0)
        ->groupBy('g.id')
        ->groupBy('up.id')
        ->orderBy('up.id', 'ASC')
        ->get();
        /* END */

        /* START IKM rumus => SUM(jml_bobot)/SUM(max_bobot)*100 */
        $q_hs = DB::table('srv_survey_hasil AS sh')
        ->select(DB::raw('SUM(sh.jml_bobot) AS sum_jml_bobot, SUM(sh.max_bobot) AS sum_max_bobot'))
        ->join('srv_grup AS g', 'g.id', '=', 'sh.id_grup')
        ->where('sh.is_deleted', 0)
        ->where('g.is_deleted', 0)
        ->where('g.id', $request->input('survey'))
        ->where('g.id_skpd', $request->input('skpd'))
        ->whereYear('sh.created_at', $request->input('tahun'))
        ->whereMonth('sh.created_at', $bulan)
        ->get()
        ->first();

        if($q_hs->sum_jml_bobot != 0 || $q_hs->sum_max_bobot != 0) {
          if(isset($q_hs->sum_jml_bobot)){
            $ikm = ($q_hs->sum_jml_bobot / $q_hs->sum_max_bobot) * 4;
            $ikm = round($ikm, 2);
            $penilaian['nilai_interval_konversi'] = $ikm;
            $penilaian['show_bintang'] = $ikm ;
          }
        }else {

        }

        $kritiksaran = DB::table('srv_survey_hasil AS sh')
        ->select(DB::raw('sh.kritik_saran, sh.created_at, sh.nama'))
        ->join("mst_pendidikan AS p", "p.id","=","sh.id_pendidikan")
        ->join("mst_pekerjaan AS pj", "pj.id","=","sh.id_pekerjaan")
        ->where('sh.is_deleted', 0)
        ->where("sh.id_grup", $request->input("survey"))
        ->whereRaw(DB::raw("(sh.kritik_saran IS NOT NULL AND sh.kritik_saran != '')"))
        ->orderBy('sh.id', 'ASC')
        ->whereYear('sh.created_at', $request->input('tahun'))
        ->whereMonth('sh.created_at', $bulan)
        ->get();

        /* END */
      }

      return view('laporan.srv_bulanan', [
        'skpd' => $skpd,
        'survey' => $survey,
        'request' => $request->input(),
        'hit_survey_bulanan' => $hit_survey_bulanan,
        'total_survey_tahunan' => $total_survey_tahunan,
        'selected_survey' => $selected_survey,
        'total_bobot_per_unsur' => $total_bobot_per_unsur,
        'penilaian' => $penilaian,
        'kritiksaran' => $kritiksaran
      ]);
    }

    public function print_responden_bulanan(Request $request) 
    {
      $skpd = $request->skpd;
      $tahun = $request->tahun;
      $survey = $request->survey;
      $bulan = $request->bulan;
      
      // print all responden
      $all_responden = $request->all_responden;
      // print all survey
      $all_survey = $request->all_survey;
      
      if($all_responden != null) {
        $f_hs = $kritiksaran = DB::table('srv_survey_hasil AS sh')
        ->select(DB::raw('sh.nama, sh.alamat, sh.usia, sh.jk, sh.email, p.nama AS pendidikan, pj.nama AS pekerjaan'))
        ->join("mst_pendidikan AS p", "p.id","=","sh.id_pendidikan")
        ->join("mst_pekerjaan AS pj", "pj.id","=","sh.id_pekerjaan")
        ->join("srv_grup AS gr", "gr.id", '=', "sh.id_grup")
        ->join("mst_skpd AS sk", 'sk.id', '=', 'gr.id_skpd')
        ->where('sh.is_deleted', 0)
        ->where('sk.id', $skpd)
        ->orderBy('sh.id', 'ASC')
        ->get();
      } else if($all_survey != null) {
        $f_hs = $kritiksaran = DB::table('srv_survey_hasil AS sh')
        ->select(DB::raw('sh.nama, sh.alamat, sh.usia, sh.jk, sh.email, p.nama AS pendidikan, pj.nama AS pekerjaan'))
        ->join("mst_pendidikan AS p", "p.id","=","sh.id_pendidikan")
        ->join("mst_pekerjaan AS pj", "pj.id","=","sh.id_pekerjaan")
        ->join("srv_grup AS gr", "gr.id", '=', "sh.id_grup")
        ->join("mst_skpd AS sk", 'sk.id', '=', 'gr.id_skpd')
        ->where('sh.is_deleted', 0)
        ->where('sk.id', $skpd)
        ->where('sh.id_grup', $survey)
        ->orderBy('sh.id', 'ASC')
        ->whereYear('sh.created_at', $tahun)
        ->get();
      } else {
        $f_hs = $kritiksaran = DB::table('srv_survey_hasil AS sh')
        ->select(DB::raw('sh.nama, sh.alamat, sh.usia, sh.jk, sh.email, p.nama AS pendidikan, pj.nama AS pekerjaan'))
        ->join("mst_pendidikan AS p", "p.id","=","sh.id_pendidikan")
        ->join("mst_pekerjaan AS pj", "pj.id","=","sh.id_pekerjaan")
        ->where('sh.is_deleted', 0)
        ->where("sh.id_grup", $request->input("survey"))
        ->orderBy('sh.id', 'ASC')
        ->whereYear('sh.created_at', $request->input('tahun'))
        ->whereMonth('sh.created_at', $bulan)
        ->get();
      }

      $data = [
        'title' => 'Laporan responden bulanan'
      ];

      return view('laporan.print_responden_bulanan', compact('data', 'tahun', 'f_hs'));
    }

    public function get_tahun_skpd(Request $request, $id_skpd)
    {
        $tahun = Grup::where("is_deleted", 0)->select("tahun")->where("id_skpd", $id_skpd)
        ->groupBy("tahun")
        ->orderBy("tahun")
        ->get();

        return response()->json($tahun);
    }

    public function get_pensurvey(Request $request, $id_grup){

      $bulan = $request->input("bulan")/1;
      $data = DB::table('srv_survey_hasil AS sh')
      ->select(DB::raw('sh.nama, sh.alamat, sh.usia, sh.jk, p.nama AS pendidikan, pj.nama AS pekerjaan, sh.email'))
      ->join("mst_pendidikan AS p", "p.id","=","sh.id_pendidikan")
      ->join("mst_pekerjaan AS pj", "pj.id","=","sh.id_pekerjaan")
      ->where('sh.is_deleted', 0)
      ->where("sh.id_grup", $id_grup)
      ->whereYear('sh.created_at', $request->input('tahun'))
      ->whereMonth('sh.created_at', $bulan)
      ->orderBy('sh.id', 'ASC');

      return Datatables::of($data)->make(true);
    }

    public function ikm(Request $request)
    {
      $skpd = get_skpd_by_login();
      $ikm_tahunan_asc = [];
      $ikm_tahunan_desc = [];

      $selectedSkpd = isAdmin() ? $request->get('skpd') ?? 1 : getLoggedInUser()->id_skpd;
      $selectedYear = (int) $request->get('tahun');

      // jika sudah submit
      if($selectedSkpd > 0){
        /* rumus => SUM(jml_bobot)/SUM(max_bobot)*100  */
        $ikm_tahunan_asc = app('IKMReportingService')
            ->withSkpd($selectedSkpd)
            ->orderBy('ikm', IKMReportingService::ORDER_DIRECTION_ASC);

        $ikm_tahunan_desc =  app('IKMReportingService')
            ->withSkpd($selectedSkpd)
            ->orderBy('ikm', IKMReportingService::ORDER_DIRECTION_DESC);

        $ikm_tahunan_asc = $selectedYear > 0 ? $ikm_tahunan_asc->withYear($selectedYear)->get() : $ikm_tahunan_asc->get();
        $ikm_tahunan_desc = $selectedYear > 0 ? $ikm_tahunan_desc->withYear($selectedYear)->get() : $ikm_tahunan_desc->get();
      }

      return view('laporan.ikm', [
        'skpd' => $skpd,
        'request' => $request->input(),
        'ikm_tahunan_asc' => $ikm_tahunan_asc,
        'ikm_tahunan_desc' => $ikm_tahunan_desc,
         'selectedSkpd' => $selectedSkpd,
         'selectedYear' => $selectedYear,
      ]);
    }

    public function ikm_tingkat_kota(Request $request)
    {
      // $skpd = get_skpd_by_login();
      $option_tahun = Grup::select('tahun')->where('is_deleted', 0)->groupBy('tahun')->orderBy('tahun', 'ASC')->get();

      $ikmPerCityYearly = [];

      // jika sudah submit
      if($request->input('tahun') > 0){
        /* rumus => SUM(jml_bobot)/SUM(max_bobot)*100  */
        $ikmPerCityYearly = app('IKMReportingService')
            ->withYear( $request->input('tahun'))
            ->orderBy('id_skpd', 'asc')
            ->showAllSkpd()
            ->groupBySKPD()->get(null);
      }


      return view('laporan.ikm_tingkat_kota', [
        'option_tahun' => $option_tahun,
        'request' => $request->input(),
        'ikmPerCityYearly' => $ikmPerCityYearly
      ]);
    }

    public function unsurPelayananSurvey(Request $request)
    {
        $skpd = get_skpd_by_login();
        $selectedSkpd = isAdmin() ? $request->get('skpd') ?? 1 : getLoggedInUser()->id_skpd;
        $selectedYear = $request->get('tahun');
        $selectedSurvey = $request->get('survey');

        $unsurPertanyaan = SrvPertanyaanUnsur::all();

        $selectedSkpdData = Skpd::find($selectedSkpd);
        $selectedSurveyData = SrvGroup::find($selectedSurvey);
        return view('laporan.unsur_pelayanan_survey', compact('skpd', 'selectedSkpd', 'selectedYear', 'unsurPertanyaan', 'selectedSurvey',
        'selectedSkpdData', 'selectedSurveyData'));
    }

    public function unsurTertimbang(Request $request)
    {
      $tahun = DB::table('srv_grup')
        ->selectRaw('tahun')
        ->distinct('tahun')
        ->where('is_deleted', 0)
        ->get();  

      $unsur = DB::table('srv_pertanyaan_unsur')
        ->selectRaw('*')
        ->get();
        
        return view('laporan.unsur_tertimbang', [
          'tahun' => $tahun,
          'unsur' => $unsur
          ]);
        }
        
    public function getScoreTertimbang($tahun)
    {
      $total_bobot_per_unsur = DB::table('srv_pertanyaan AS p')
      ->select(DB::raw("up.id AS id_unsur, up.nama AS unsur, ((SUM(j.bobot)/SUM(mb.max_bobot))*4) AS score"))
      ->join('srv_pertanyaan_unsur AS up', 'p.id_pertanyaan_unsur', '=', 'up.id')
      ->join('srv_jawaban AS j', 'j.id_pertanyaan', '=', 'p.id')
      ->join('srv_grup_pertanyaan AS gp', 'gp.id_pertanyaan', '=', 'p.id')
      ->join('srv_grup AS g', 'g.id', '=', 'gp.id_grup')
      ->join('srv_survey_hasil_detail AS shd', 'shd.id_jawaban', '=', 'j.id')
      ->join('srv_survey_hasil AS sh', function($join) {
        $join->on('sh.id', '=', 'shd.id_hasil')
        ->on('sh.id_grup', '=', 'g.id');
      })
      ->join(DB::raw("(SELECT
        MAX(bobot) max_bobot, jb.id_pertanyaan
        FROM srv_jawaban jb
        INNER JOIN srv_pertanyaan pb ON jb.id_pertanyaan = pb.id
        INNER JOIN srv_grup_pertanyaan gp ON gp.id_pertanyaan = pb.id
        GROUP BY jb.id_pertanyaan) AS mb"), 'mb.id_pertanyaan', '=', 'shd.id_pertanyaan')
      ->whereYear('sh.created_at', $tahun)
      //->where('p.is_deleted', 0)
      ->where('up.is_deleted', 0)
      //->where('j.is_deleted', 0)
      ->where('gp.is_deleted', 0)
      ->where('g.is_deleted', 0)
      ->where('sh.is_deleted', 0)
      ->where('shd.is_deleted', 0)
      ->groupBy('g.id')
      ->groupBy('up.id')
      ->orderBy('up.id', 'ASC')
      ->get();
        
      return response()->json($total_bobot_per_unsur);
    }
        
    public function unsurPelayananSurveyPerBulan(Request $request)
    {
        $selectedYear = $request->get('tahun', now()->year);
        $selectedMonthFrom = $request->get('monthFrom', now()->startOfYear()->month);
        $selectedMonthTo = $request->get('monthTo', now()->startOfYear()->addMonths(5)->month);

        $unsurPertanyaan = SrvPertanyaanUnsur::all();

        $monthlyUnsurPertanyaan = $this->getSurveyElementAverageValuesMonthly($selectedYear, $selectedMonthFrom, $selectedMonthTo);
        return view('laporan.unsur_pelayanan_survey_pertahun', compact('selectedYear', 'selectedMonthFrom', 'selectedMonthTo', 'monthlyUnsurPertanyaan', 'unsurPertanyaan'));
    }

    public function unsurPelayananSurveyPerTahun(Request $request)
    {
        $skpd = get_skpd_by_login();
        $selectedSkpd = isAdmin() ? $request->get('skpd') ?? 1 : getLoggedInUser()->id_skpd;
        $selectedYear = $request->get('tahun', now()->year);
        $selectedSurvey = $request->get('survey');

        $unsurPertanyaan = SrvPertanyaanUnsur::all();

        $monthlyUnsurPertanyaan = $this->getSurveyElementAverageValuesMonthly($selectedYear, 1, 12, [$selectedSurvey]);

        // Quick Hack, LOL
        foreach ($unsurPertanyaan as $key => $value) {

          $sum  = $monthlyUnsurPertanyaan->sum(function($data) use($value){
              $found =  $data->data->where('unsurPertanyaan.id', $value->id)->first();

              return $found != null ? $found->value : null;
          });

          $unsurPertanyaan[$key]->kinerja_unit = $this->getStatusFromValue($sum / 12);
        }

        $selectedSkpdData = Skpd::find($selectedSkpd);
        $selectedSurveyData = SrvGroup::find($selectedSurvey);

        return view('laporan.unsur_pelayanan_survey_perbulan', compact('skpd', 'selectedSkpd',
        'selectedYear', 'selectedSurvey',
        'monthlyUnsurPertanyaan', 'unsurPertanyaan',
        'selectedSkpdData', 'selectedSurveyData'
      ));
    }

    public function rekapitulasiNilaiSkm(Request $request)
    {
        $skpd = Skpd::where('is_deleted',0)->get();
        $selectedYear = $request->get('tahun') ?? now()->year;

        $bobot = DB::table('srv_grup AS g')
        ->selectRaw('SUM((sh.jml_bobot/max_bobot))/COUNT(g.id) as bobot, skpd.name, skpd.id')
        ->join('srv_survey_hasil as sh', 'g.id', '=', 'sh.id_grup')
        ->join('mst_skpd as skpd', 'skpd.id', '=', 'g.id_skpd')
        ->whereRaw(DB::raw('jml_bobot > 0 && max_bobot > 0'))
        ->where('g.is_deleted', 0)
        ->whereRaw(DB::raw('YEAR(sh.created_at) = '.$selectedYear))
        ->groupBy('skpd.id')
        ->get();

        return view('laporan.rekapitulasi_nilai_skm', compact('skpd', 'selectedYear', 'bobot'));
    }

    public function opdSurveys($idOpd, Request $request)
    {
        $year = $request->get('tahun');

        $surveyQuery = SrvGroup::where('id_skpd', $idOpd);

        if($year)
            $surveyQuery->where('tahun', $year);

        /** @var SrvGroup | null $surveys */
        $surveys = $surveyQuery->select('id', 'nama', 'slug')
            ->get();

        return response()->json([
            'surveys' => $surveys
        ]);
    }

    private function getSurveyElementAverageValuesMonthly($year, $monthFrom, $monthTo, $skpdIds = [])
    {

        $unsurPertanyaan = SrvPertanyaanUnsur::all();

        $monthlyUnsurPertanyaan = collect([]);

        for ($i = $monthFrom ; $i <= $monthTo ; $i++)
        {

            $data = (object) [
                'month' => $i,
                'data' => collect([]),
                'nilaiSurvey' => 0,
                'nilaiSkm' => 0,
                'mutuPelayanan' => '',
                'kinerjaUnit' => ''
            ];

            foreach ($unsurPertanyaan as $unsur) {

                /* @var SrvPertanyaanUnsur $unsur*/

                $questionElementValuesService = new QuestionElementValuesService($unsur);
                $averageServiceValue = $questionElementValuesService->withYear($year)->withMonth($i);

                if(!empty($skpdIds))
                    $averageServiceValue->withSurveyIds($skpdIds);

                $averageServiceValue = $averageServiceValue->getAverageServiceValue();

                $data->data->push((object) [
                    'unsurPertanyaan' => $unsur,
                    'value' => $averageServiceValue
                ]);

                $data->nilaiSurvey += $averageServiceValue;
            }


            $data->nilaiSkm = $data->nilaiSurvey / $unsurPertanyaan->count() * 25;
            $data->mutuPelayanan = $this->getMutuPelayananFromValue($data->nilaiSurvey / $unsurPertanyaan->count());
            $data->kinerjaUnit = $this->getStatusFromValue($data->nilaiSurvey / $unsurPertanyaan->count());

            $monthlyUnsurPertanyaan->push($data);
        }

        return $monthlyUnsurPertanyaan;
    }

    private function getStatusFromValue($value)
    {
        if($value >= 0 && $value <= 1.75 && $value != null)
            $status = 'Tidak Memuaskan';
        elseIf($value > 1.75 && $value <= 2.50)
            $status = 'Kurang Memuaskan';
        elseIf($value > 2.50 && $value <= 3.25)
            $status = 'Memuaskan';
        elseIf($value > 3.25 && $value <= 4.00)
            $status = 'Sangat Memuaskan';
        elseif($value == null)
            $status = 'Tidak Ada Data';
        else
            $status = '';

        return $status;
    }

    private function getMutuPelayananFromValue($value)
    {
        if($value >= 0 && $value <= 1.75 && $value != null)
            $status = 'D';
        elseIf($value > 1.75 && $value <= 2.50)
            $status = 'C';
        elseIf($value > 2.50 && $value <= 3.25)
            $status = 'B';
        elseIf($value > 3.25 && $value <= 4.00)
            $status = 'A';
        elseif($value == null)
            $status = 'Tidak Ada Data';
        else
            $status = '';

        return $status;
    }

    public function dashboard(Request $request)
    {
      $skpd = get_skpd_by_login();
      $ikm_tahunan_asc = [];
      $ikm_tahunan_desc = [];

      $selectedSkpd = isAdmin() ? $request->get('skpd') ?? 1 : getLoggedInUser()->id_skpd;
      $selectedYear = (int) $request->get('tahun');

      $selectedYearForOPDWithEmptySurvey = $request->get('empty_survey_year') ?? now()->year;
      $selectedOPDYearlyReportingYear = $request->get('opd_reporting_year') ?? now()->year;

      // jika sudah submit
      if($selectedSkpd > 0){
        /* rumus => SUM(jml_bobot)/SUM(max_bobot)*100  */
        $ikm_tahunan_asc = app('IKMReportingService')
            ->withSkpd($selectedSkpd)
            ->orderBy('ikm', IKMReportingService::ORDER_DIRECTION_ASC);

        $ikm_tahunan_desc =  app('IKMReportingService')
            ->withSkpd($selectedSkpd)
            ->orderBy('ikm', IKMReportingService::ORDER_DIRECTION_DESC);

        $ikm_tahunan_asc = $selectedYear > 0 ? $ikm_tahunan_asc->withYear($selectedYear)->get() : $ikm_tahunan_asc->get();
        $ikm_tahunan_desc = $selectedYear > 0 ? $ikm_tahunan_desc->withYear($selectedYear)->get() : $ikm_tahunan_desc->get();
      }

      $option_tahun = Grup::select('tahun')->where('is_deleted', 0)->groupBy('tahun')->orderBy('tahun', 'ASC')->get();

      $ikmPerCityYearly = [];

      // jika sudah submit
      if($request->input('tahun') > 0){
        /* rumus => SUM(jml_bobot)/SUM(max_bobot)*100  */
        $ikmPerCityYearly = app('IKMReportingService')
            ->withYear( $request->input('tahun'))
            ->orderBy('id_skpd', 'asc')
            ->showAllSkpd()
            ->groupBySKPD()->get(null);
      }

      $query = SrvSurveyHasil::query();

        $skpdIds = get_skpd_by_login()->pluck('id');

        $srvGroupIds = SrvGroup::whereIn('id_skpd', $skpdIds)->pluck('id');

        $query->whereIn('id_grup', $srvGroupIds);

        $chartPendidikan = (clone $query)->selectRaw('count(id) as total, id_pendidikan')
            ->groupBy('id_pendidikan')->get();

        $chartPendidikan = $chartPendidikan->map(function ($surveyData) {
            return [
                'total' => $surveyData->total,
                'label' => Pendidikan::PENDIDIKAN_NAME_FROM_ID[$surveyData->id_pendidikan],
            ];
        });

        $chartUmur = (clone $query)->selectRaw('usia, count(id) as total')
            ->groupBy('usia')->get()->toArray();

        $respondentChart =  [
            'gender' => [
                'male' => (clone $query)->where('jk', 'L')->count(),
                'female' => (clone $query)->where('jk', 'P')->count()
            ],
            'pendidikan' => $chartPendidikan,
            'umur' => $chartUmur
        ];

        $skpd = get_skpd_by_login();
        $ikm_tahunan_asc = [];
        $ikm_tahunan_desc = [];

        $selectedOPDYearlyReportingYear = $request->get('opd_reporting_year') ?? now()->year;
        $selectedYearForOPDWithEmptySurvey = $request->get('empty_survey_year') ?? now()->year;
        $availableYearForOPDReportYearly = Grup::select('tahun')->where('is_deleted', 0)->groupBy('tahun')->orderBy('tahun', 'ASC')->get();

        /* rumus => SUM(jml_bobot)/SUM(max_bobot)*100  */
        $ikm_tahunan_asc = app('IKMReportingService')
            ->withSkpd($selectedSkpd)
            ->orderBy('ikm', IKMReportingService::ORDER_DIRECTION_ASC);

        $ikm_tahunan_desc =  app('IKMReportingService')
            ->withSkpd($selectedSkpd)
            ->orderBy('ikm', IKMReportingService::ORDER_DIRECTION_DESC);

        $ikm_tahunan_asc = $selectedYear > 0 ? $ikm_tahunan_asc->withYear($selectedYear)->get() : $ikm_tahunan_asc->get();
        $ikm_tahunan_desc = $selectedYear > 0 ? $ikm_tahunan_desc->withYear($selectedYear)->get() : $ikm_tahunan_desc->get();

      return view('home', [
        'skpd' => $skpd,
        'request' => $request->input(),
        'ikm_tahunan_asc' => $ikm_tahunan_asc,
        'ikm_tahunan_desc' => $ikm_tahunan_desc,
        'selectedSkpd' => $selectedSkpd,
        'selectedYear' => $selectedYear,
        'option_tahun' => $option_tahun,
        'request' => $request->input(),
        'ikmPerCityYearly' => $ikmPerCityYearly
      ], compact('ikm_tahunan_asc',
      'ikm_tahunan_desc',
      'skpd',
      'availableYearForOPDReportYearly',
      'selectedOPDYearlyReportingYear',
      'selectedYearForOPDWithEmptySurvey',
      'respondentChart'));
    }

    public function lapor_ikm_get() {
      $data = DB::table('srv_survey_hasil AS sh')
      ->select(DB::raw('skpd.name AS opd, gr.tahun, gr.nama AS survey'))
      ->join('srv_grup AS gr', 'gr.id', '=', 'sh.id_grup')
      ->join('mst_skpd AS skpd', 'skpd.id', '=', 'gr.id_skpd')
      ->orderBy('sh.id')
      ->get();

      return response()->json($data);
    }

    public function get_data_survey($id_survey, $tahun, $opd)
    {
      $data = DB::table('srv_grup AS gr')
        ->selectRaw('COUNT(gr.id) AS jumlah_survey, count(case when sh.jk="P" then 1 end) as Perempuan, count(case when sh.jk="L" then 1 end) as Laki_laki, COUNT(CASE when sh.id_pendidikan=2 THEN 1 END) as sd, COUNT(CASE when sh.id_pendidikan=2 THEN 1 END) as smp, COUNT(CASE when id_pendidikan=3 THEN 1 END) as sma, COUNT(CASE when sh.id_pendidikan=4 THEN 1 END) as d1, COUNT(CASE when id_pendidikan=5 THEN 1 END) as d2, COUNT(CASE when sh.id_pendidikan=6 THEN 1 END) as d3, COUNT(CASE when id_pendidikan=7 THEN 1 END) as s1, COUNT(CASE when sh.id_pendidikan=8 THEN 1 END) as s2, COUNT(CASE when id_pendidikan=9 THEN 1 END) as s1')
        ->join('srv_survey_hasil AS sh', 'gr.id', '=', 'sh.id_grup')
        ->where('gr.id', $id_survey)
        ->where('gr.tahun', $tahun)
        ->where('sh.is_deleted', 0)
        ->get();

        $q_hs = DB::table('srv_survey_hasil AS sh')
        ->select(DB::raw('SUM(sh.jml_bobot) AS sum_jml_bobot, SUM(sh.max_bobot) AS sum_max_bobot'))
        ->join('srv_grup AS g', 'g.id', '=', 'sh.id_grup')
        ->where('sh.is_deleted', 0)
        ->where('g.is_deleted', 0)
        ->where('g.id', $id_survey)
        ->where('g.id_skpd', $opd)
        ->whereYear('sh.created_at', $tahun)
        ->get()
        ->first();

        if(isset($q_hs->sum_jml_bobot)){
          $ikm = ($q_hs->sum_jml_bobot / $q_hs->sum_max_bobot) * 4;
          $ikm = round($ikm, 2);
          $penilaian['nilai_interval_konversi'] = $ikm;
          $penilaian['show_bintang'] = $ikm;
        }

      return response()->json([$data, $penilaian, $q_hs]);
    }

    public function get_seluruh_data_survey($tahun, $opd) 
    {
      $data = DB::table('srv_grup AS gr')
        ->selectRaw('COUNT(gr.id) AS jumlah_survey, count(case when sh.jk="P" then 1 end) as Perempuan, count(case when sh.jk="L" then 1 end) as Laki_laki, COUNT(CASE when sh.id_pendidikan=2 THEN 1 END) as sd, COUNT(CASE when sh.id_pendidikan=2 THEN 1 END) as smp, COUNT(CASE when id_pendidikan=3 THEN 1 END) as sma, COUNT(CASE when sh.id_pendidikan=4 THEN 1 END) as d1, COUNT(CASE when id_pendidikan=5 THEN 1 END) as d2, COUNT(CASE when sh.id_pendidikan=6 THEN 1 END) as d3, COUNT(CASE when id_pendidikan=7 THEN 1 END) as s1, COUNT(CASE when sh.id_pendidikan=8 THEN 1 END) as s2, COUNT(CASE when id_pendidikan=9 THEN 1 END) as s1')
        ->join('srv_survey_hasil AS sh', 'gr.id', '=', 'sh.id_grup')
        ->where('gr.id_skpd', $opd)
        ->where('gr.tahun', $tahun)
        ->where('sh.is_deleted', 0)
        ->get();

        $q_hs = DB::table('srv_survey_hasil AS sh')
        ->select(DB::raw('SUM(sh.jml_bobot) AS sum_jml_bobot, SUM(sh.max_bobot) AS sum_max_bobot'))
        ->join('srv_grup AS g', 'g.id', '=', 'sh.id_grup')
        ->where('sh.is_deleted', 0)
        ->where('g.is_deleted', 0)
        ->where('g.id_skpd', $opd)
        ->whereYear('sh.created_at', $tahun)
        ->get()
        ->first();

        if(isset($q_hs->sum_jml_bobot)){
          $ikm = ($q_hs->sum_jml_bobot / $q_hs->sum_max_bobot) * 4;
          $ikm = round($ikm, 2);
          $penilaian['nilai_interval_konversi'] = $ikm;
          $penilaian['show_bintang'] = $ikm;
        }

        return response()->json([$data, $penilaian]);
    }

    public function lapor_ikm(){
      $opd = get_skpd_by_login();
      
      return view('laporan/lapor_ikm', compact('opd'));
    }

}

