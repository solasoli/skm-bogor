<?php

namespace App\Http\Controllers;

use App\Pendidikan;
use App\Services\IKMReportingService;
use App\SKPD;
use App\Srv\Grup;
use App\SrvGroup;
use App\SrvSurveyHasil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use App\Peminjaman;
use App\Simpanan;
use App\BayarPertama;
use App\Konfigurasi;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $skpd = get_skpd_by_login();
        $ikm_tahunan_asc = [];
        $ikm_tahunan_desc = [];
        $ikmPerCityYearly = collect([]);

        $selectedSkpd = isAdmin() ? $request->get('skpd') ?? 1 : getLoggedInUser()->id_skpd;
        $selectedYear = $request->get('tahun') ?? now()->year;
        $selectedOPDYearlyReportingYear = $request->get('opd_reporting_year') ?? now()->year;
        $selectedYearForOPDWithEmptySurvey = $request->get('empty_survey_year') ?? now()->year;
        $availableYearForOPDReportYearly = Grup::select('tahun')->where('is_deleted', 0)->groupBy('tahun')->orderBy('tahun', 'ASC')->get();

        /* rumus => SUM(jml_bobot)/SUM(max_bobot)*100  */
        $ikm_tahunan_asc = app('IKMReportingService')
            ->withSkpd($selectedSkpd)
            ->orderBy('ikm', IKMReportingService::ORDER_DIRECTION_ASC);

        $ikm_tahunan_desc =  app('IKMReportingService')
            ->withSkpd($selectedSkpd)
            ->orderBy('ikm', IKMReportingService::ORDER_DIRECTION_DESC);

        $ikm_tahunan_asc = $selectedYear > 0 ? $ikm_tahunan_asc->withYear($selectedYear)->get() : $ikm_tahunan_asc->get();
        $ikm_tahunan_desc = $selectedYear > 0 ? $ikm_tahunan_desc->withYear($selectedYear)->get() : $ikm_tahunan_desc->get();

        // jika sudah submit
        if($request->input('tahun') > 0){
            /* rumus => SUM(jml_bobot)/SUM(max_bobot)*100  */
            $ikmPerCityYearly = app('IKMReportingService')
                ->withYear( $request->input('tahun'))
                ->orderBy('id_skpd', 'asc')
                ->showAllSkpd()
                ->groupBySKPD()->get(null);
        }else{
            $ikmPerCityYearly = app('IKMReportingService')
                ->orderBy('id_skpd', 'asc')
                ->groupBySKPD()
                ->showAllSkpd();

            $ikmPerCityYearly = $selectedOPDYearlyReportingYear > 0 ? $ikmPerCityYearly->withYear($selectedOPDYearlyReportingYear)->get() : $ikmPerCityYearly->get();
        }

        // Chart
        $query = SrvSurveyHasil::query();

        $skpdIds = get_skpd_by_login()->pluck('id');

        $srvGroupIds = SrvGroup::whereIn('id_skpd', $skpdIds)->pluck('id');

        $query->whereIn('id_grup', $srvGroupIds);

        $chartPendidikan = (clone $query)->selectRaw('count(id) as total, id_pendidikan')
            ->groupBy('id_pendidikan')->get();

        $chartPendidikan = $chartPendidikan->map(function ($surveyData) {
            return [
                'total' => $surveyData->total,
                'label' => Pendidikan::PENDIDIKAN_NAME_FROM_ID[$surveyData->id_pendidikan],
            ];
        });

        $chartUmur = (clone $query)->selectRaw('usia, count(id) as total')
            ->groupBy('usia')->get()->toArray();

        $respondentChart =  [
            'gender' => [
                'male' => (clone $query)->where('jk', 'L')->count(),
                'female' => (clone $query)->where('jk', 'P')->count()
            ],
            'pendidikan' => $chartPendidikan,
            'umur' => $chartUmur
        ];

        return view('home', compact('ikm_tahunan_asc',
            'ikm_tahunan_desc',
            'selectedYear',
            'selectedSkpd',
            'skpd',
            'ikmPerCityYearly',
            'availableYearForOPDReportYearly',
            'selectedOPDYearlyReportingYear',
            'selectedYearForOPDWithEmptySurvey',
            'respondentChart'
        ));
    }

    public function tentang()
    {
      $konfigurasi = konfigurasi::first();

      return view('tentang', [
        'data'=> $konfigurasi
      ]);
    }

    public function skema()
    {
      $konfigurasi = konfigurasi::first();

      return view('skema', [
        'data'=> $konfigurasi
      ]);
    }
}
