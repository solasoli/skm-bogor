<?php

namespace App\Http\Middleware;

use Closure;

class AuthSkpd
{
    /**
     * This middleware expect SKPD id placed on route parameter called id_skpd
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $idSkpd = $request->route('id_skpd');

        if(!isAdmin() && $idSkpd != getLoggedInUser()->id_skpd)
            return response()->json([ "message" => "Tidak bisa mengakses skpd yang bukan milik anda"], 403);

        return $next($request);
    }
}
