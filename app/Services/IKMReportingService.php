<?php

namespace App\Services;

use App\IkmOpd;
use App\SKPD;
use App\SrvGroup;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class IKMReportingService
{
    const ORDER_DIRECTION_ASC = 'asc';
    const ORDER_DIRECTION_DESC = 'desc';

    private $skpd = null;
    private $year = null;
    private $showAllSkpd = false;
    private $groupBy = 'g.id';

    private $order = [
        'column' => 'g.id',
        'direction' => self::ORDER_DIRECTION_ASC
    ];

    private $requiredColumns;

    public function __construct()
    {
        $this->requiredColumns = collect([
            'g.id',
            'g.id_skpd',
            'skpd.name AS skpd_name',
            'g.nama AS survey',
            DB::raw("SUM(sh.jml_bobot) AS sum_jml_bobot"),
            DB::raw("SUM(sh.max_bobot) AS sum_max_bobot"),
            DB::raw("(ROUND(SUM(sh.jml_bobot) / SUM(sh.max_bobot)*4, 2)) AS ikm"),
        ]);
    }

    /**
     * @param int $skpdId
     * @return $this
     */
    public function withSkpd(int $skpdId)
    {
        $this->skpd = $skpdId;

        return $this;
    }

    public function showAllSkpd($show = true)
    {
        $this->showAllSkpd = $show;

        return $this;
    }

    public function withYear(int $year)
    {
        $this->year = $year;

        return $this;
    }

    public function orderBy($column, $direction)
    {
        $this->order = [
            'column' => $column,
            'direction' => $direction
        ];

        return $this;
    }

    public function groupBySurveyGroup()
    {
        $this->groupBy = 'g.id';

        if(!$this->requiredColumns->search('g.nama AS survey', true))
            $this->requiredColumns->push('g.nama AS survey');

        return $this;
    }

    public function groupBySKPD()
    {
        $this->groupBy = 'g.id_skpd';
        $this->requiredColumns = $this->requiredColumns->filter(function ($column) {
            return $column != 'g.nama AS survey';
        });
        return $this;
    }

    /**
     * @param int $limit
     * @return \Illuminate\Support\Collection
     */
    public function get($limit = 20)
    {
        if($limit == null) {
            $ikm = $this->generateQuery()->get();
        }
        else {
            $ikm = $this->generateQuery()->limit($limit)->get();
        }

        return $this->mapResultIntoIkmOpds($ikm);
    }

    public function paginate($page, $itemPerPage)
    {
        $skm = $this->generateQuery()->paginate($itemPerPage, ['*'], 'page', $page);

        return $this->mapResultIntoIkmOpds($skm);
    }

    private function generateQuery()
    {
        $query = DB::table('srv_survey_hasil AS sh')
            ->select($this->requiredColumns->toArray()) //
            ->join('srv_grup AS g', 'g.id', '=', 'sh.id_grup')
            ->join('mst_skpd AS skpd', 'skpd.id', '=', 'g.id_skpd')
            ->where('sh.is_deleted', 0)
            ->where('g.is_deleted', 0)
            ->groupBy($this->groupBy)
            ->orderBy($this->order['column'], $this->order['direction']);

        if($this->hasSkpdFilter())
            $query->where('g.id_skpd', $this->skpd);

        if($this->hasYearFilter())
            $query->whereYear('sh.created_at', $this->year);

        return $query;
    }

    private function hasYearFilter()
    {
        return $this->year != null;
    }

    private function hasSkpdFilter()
    {
        return $this->skpd != null;
    }

    /**
     * @param $result
     * @param $srvGroups
     * @return \Illuminate\Support\Collection
     */
    private function mapResultIntoIkmOpds($result)
    {
        /** @var Collection $srvGroups */
        $srvGroups = SrvGroup::whereIn('id', $result->pluck('id'))->get();
        /** @var Collection $mstSkpds */
        $mstSkpds = SKPD::whereIn('id', $result->pluck('id_skpd'))->get();

        if($this->showAllSkpd) {
            $mstSkpdsDidntPublishAnySurvey = SKPD::whereNotIn('id', $result->pluck('id_skpd'))->whereHas('users')->get();
            $mstSkpds = $mstSkpds->merge($mstSkpdsDidntPublishAnySurvey);
        }

        return $mstSkpds->map(function ($skpd) use ($result, $srvGroups) {

            $resultItem = $result->where('id_skpd', $skpd->id)->first();
            $jumlahBobot = $resultItem->sum_jml_bobot ?? 0;
            $boboxMaximal = $resultItem->sum_max_bobot ?? 0;
            $selectedGroup = $resultItem != null ? $srvGroups->where('id', $resultItem->id)->first(): null;
            $ikm = $resultItem->ikm ?? 0;

            return new IkmOpd($skpd, $jumlahBobot, $boboxMaximal, $ikm, $selectedGroup);
        });
    }
}
