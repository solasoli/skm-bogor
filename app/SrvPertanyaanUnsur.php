<?php

namespace App;

use App\Services\QuestionElementValuesService;
use App\Traits\SoftDelete;
use Illuminate\Database\Eloquent\Model;

class SrvPertanyaanUnsur extends Model
{
    use SoftDelete;

    const UNSUR_PERSYARATAN_ID = 1;
    const UNSUR_SISTEM_MEKANISME_PROSEDUR_ID = 2;
    const UNSUR_WAKTU_PENYELESAIAN_ID = 3;
    const UNSUR_BIAYA_TARIF_ID = 4;
    const UNSUR_PRODUK_SPESIFIKASI_JENIS_PELAYANAN_ID = 5;
    const UNSUR_KOMPETENSI_PELAKSANAAN_ID = 6;
    const UNSUR_PERILAKU_PELAKSANAAN_ID = 7;
    const UNSUR_PENANGANAN_PENGADUAN_ID = 8;
    const UNSUR_SARANA_ID = 9;
    const UNSUR_LAINNYA_ID = 10;

    protected $table = 'srv_pertanyaan_unsur';

    protected $fillable = [
        'nama',
        'guid'
    ];

    public function srvPertanyaan()
    {
        return $this->hasMany(SrvPertanyaan::class, 'id_pertanyaan_unsur');
    }

    public function getAverageServiceValue(array $surveyIds = [], $forYear = null)
    {
        $questionElementValuesService = (new QuestionElementValuesService($this));

        if(!empty($surveyIds))
            $questionElementValuesService->withSurveyIds($surveyIds);

        if($forYear)
            $questionElementValuesService->withYear($forYear);

        return $questionElementValuesService->getAverageServiceValue();
    }

    public function getAverageServiceStatus(array $surveyIds = [], $forYear = null)
    {
        $questionElementValuesService = (new QuestionElementValuesService($this));

        if(!empty($surveyIds))
            $questionElementValuesService->withSurveyIds($surveyIds);

        if($forYear)
            $questionElementValuesService->withYear($forYear);

        return $questionElementValuesService->getAverageServiceStatus();
    }
}
