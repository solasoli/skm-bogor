<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
  protected $table = "acl_permission";
  protected $primaryKey = "id";
  public $timestamps = false;
  protected $fillable = [
      'id_menu',
      'id_role',
      'view',
      'edit',
      'add',
      'delete'
  ];

  public function menu()
  {
      return $this->belongsTo(Menu::class, 'id_menu');
  }
}
