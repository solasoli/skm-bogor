<?php

namespace App;

use App\Traits\SoftDelete;
use Illuminate\Database\Eloquent\Model;

class SrvSurveyHasil extends Model
{
    use SoftDelete;

    protected $table = 'srv_survey_hasil';

    protected $casts = [
        'is_deleted' => 'bool'
    ];

    public function srvGroup()
    {
        return $this->hasMany(SrvSurveyHasil::class, 'id_grup');
    }
}
