<?php

namespace App;

use App\Traits\SoftDelete;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class SrvGroup extends Model
{
    use SoftDelete;

    protected $table = 'srv_grup';

    protected $casts = [
        'is_deleted' => 'bool'
    ];

    public function srvSurveyHasil()
    {
        return $this->hasMany(SrvSurveyHasil::class, 'id_grup');
    }

    public function mstSkpd()
    {
        return $this->belongsTo(SKPD::class, 'id_skpd');
    }

    public function srvPertanyaan()
    {
        return $this->belongsToMany(SrvPertanyaan::class, 'srv_grup_pertanyaan', 'id_grup', 'id_pertanyaan')
            ->where('srv_grup_pertanyaan.is_deleted', 0);
    }
}
