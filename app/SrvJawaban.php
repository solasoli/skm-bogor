<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class SrvJawaban extends Model
{
    protected $table = 'srv_jawaban';

    public function srvSurveyHasil()
    {
        return $this->belongsToMany(SrvSurveyHasil::class, 'srv_survey_hasil_detail', 'id_jawaban', 'id_hasil');
    }
}
