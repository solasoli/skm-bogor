<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPertanyaanUnsurSrvPertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('srv_pertanyaan', function (Blueprint $table) {
            $table->integer("id_pertanyaan_unsur");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('srv_pertanyaan', function (Blueprint $table) {
            //
        });
    }
}
