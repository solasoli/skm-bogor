<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAclMenuAdditional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acl_menu', function (Blueprint $table) {
            $table->boolean("have_child")->default(0);
            $table->integer("id_parent")->default(0);
            $table->integer("level")->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acl_menu', function (Blueprint $table) {
            //
        });
    }
}
