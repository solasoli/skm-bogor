<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableSrvPenilaian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('srv_penilaian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('nilai_persepsi');
            $table->decimal('nilai_interval_from', 6, 4);
            $table->decimal('nilai_interval_to', 6, 4);
            $table->decimal('nilai_interval_konversi_from', 6, 2);
            $table->decimal('nilai_interval_konversi_to', 6, 2);
            $table->char('mutu_pelayanan', 5);
            $table->string('kinerja_unit_pelayanan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('srv_penilaian');
    }
}
