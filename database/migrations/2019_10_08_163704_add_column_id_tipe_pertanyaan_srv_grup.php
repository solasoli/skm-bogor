<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIdTipePertanyaanSrvGrup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('srv_grup', function (Blueprint $table) {
          $table->bigInteger("id_tipe_pertanyaan")->after("id_skpd");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('srv_grup', function (Blueprint $table) {
            //
        });
    }
}
