<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('srv_pertanyaan', function (Blueprint $table) {
            $table->foreign('id_pertanyaan_unsur')
                ->references('id')->on('srv_pertanyaan_unsur');
        });

        Schema::table('srv_jawaban', function (Blueprint $table) {
            $table->foreign('id_pertanyaan')
                ->references('id')->on('srv_pertanyaan');
        });

        Schema::table('srv_survey_hasil_detail', function (Blueprint $table) {
            $table->foreign('id_hasil')
                ->references('id')->on('srv_hasil');
            $table->foreign('id_pertanyaan')
                ->references('id')->on('srv_pertanyaan');
            $table->foreign('id_jawaban')
                ->references('id')->on('srv_jawaban');
        });

        Schema::table('srv_survey_hasil', function (Blueprint $table) {
            $table->foreign('id_grup')
                ->references('id')->on('srv_grup');
        });

        Schema::table('srv_grup_pertanyaan', function (Blueprint $table) {
            $table->foreign('id_grup')
                ->references('id')->on('srv_grup');
            $table->foreign('id_pertanyaan')
                ->references('id')->on('srv_pertanyaan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('srv_pertanyaan', function (Blueprint $table) {
            $table->dropIndex('srv_pertanyaan_id_pertanyaan_unsur_foreign');
        });

        Schema::table('srv_jawaban', function (Blueprint $table) {
            $table->dropIndex('srv_jawaban_id_pertanyaan_foreign');
        });

        Schema::table('srv_survey_hasil_detail', function (Blueprint $table) {
            $table->dropIndex('srv_survey_hasil_detail_id_hasil_foreign');
            $table->dropIndex('srv_survey_hasil_detail_id_pertanyaan_foreign');
            $table->dropIndex('srv_survey_hasil_detail_id_jawaban_foreign');
        });

        Schema::table('srv_survey_hasil', function (Blueprint $table) {
            $table->dropIndex('srv_survey_hasil_id_grup_foreign');
        });

        Schema::table('srv_grup_pertanyaan', function (Blueprint $table) {
            $table->dropIndex('srv_grup_pertanyaan_id_grup_foreign');
            $table->dropIndex('srv_grup_pertanyaan_id_pertanyaan_foreign');
        });
    }
}
