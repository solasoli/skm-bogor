<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedReportMenusForAclMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();

        $parent = \App\Menu::create([
            'nama' => 'Laporan Rata-Rata',
            'url' => '#',
            'slug' => 'laporan_rata_rata'
        ]);

        $childrenMenu = collect([
            [
                'nama' => 'Unsur Pelayanan Survey',
                'url' => '/laporan/unsur_pelayanan_survey',
                'slug' => 'laporan_detil',
                'id_parent' => $parent->id,
                'level' => 2
            ],
            [
                'nama' => 'Unsur Pelayanan Survey Pertahun',
                'url' => '/laporan/unsur_pelayanan_survey_pertahun',
                'slug' => 'laporan_detil',
                'id_parent' => $parent->id,
                'level' => 2
            ],
            [
                'nama' => 'Unsur Pelayanan Perbulan',
                'url' => '/laporan/unsur_pelayanan_perbulan',
                'slug' => 'laporan_detil',
                'id_parent' => $parent->id,
                'level' => 2
            ],
            [
                'nama' => 'Rekapitulasi Nila SKM PerSKPD',
                'url' => '/laporan/rekapitulasi_nilai_skm',
                'slug' => 'laporan_detil',
                'id_parent' => $parent->id,
                'level' => 2
            ],
        ])->map(function ($childMenuArray) {
            return \App\Menu::create($childMenuArray);
        });

        $aclPermissionCollection = collect([
            [
                'id_menu' => $parent->id,
                'id_role' => 2,
                'view' => 1
            ]
        ]);

        $aclPermissionCollection = $aclPermissionCollection
            ->merge($childrenMenu->map(function (\App\Menu $childMenu) {
                return [
                    'id_menu' => $childMenu->id,
                    'id_role' => 2,
                    'view' => 1
                ];
            }));

        \App\Permission::insert($aclPermissionCollection->toArray());

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::beginTransaction();
        $menus = collect([
            [
                ['nama', 'Laporan Rata-Rata'],
                ['url', '#'],
                ['slug', 'laporan_rata_rata']
            ],
            [
                ['nama' ,  'Unsur Pelayanan Survey'],
                ['url' ,  '/laporan/unsur_pelayanan_survey'],
                ['slug' ,  'laporan_detil'],
                ['level' ,  2],
            ],
            [
                ['nama' ,  'Unsur Pelayanan Survey Pertahun'],
                ['url' ,  '/laporan/unsur_pelayanan_survey_pertahun'],
                ['slug' ,  'laporan_detil'],
                ['level' ,  2],
            ],
            [
                ['nama' ,  'Unsur Pelayanan Perbulan'],
                ['url' ,  '/laporan/unsur_pelayanan_perbulan'],
                ['slug' ,  'laporan_detil'],
                ['level' ,  2],
            ],
            [
                ['nama' ,  'Rekapitulasi Nila SKM PerSKPD'],
                ['url' ,  '/laporan/rekapitulasi_nilai_skm'],
                ['slug' ,  'laporan_detil'],
                ['level' ,  2],
            ]
        ]);

        $menus->each(function ($menuArray) {
            \App\Menu::where($menuArray)->delete();
        });

        \App\Permission::doesntHave('menu')->delete();
        DB::commit();
    }
}
