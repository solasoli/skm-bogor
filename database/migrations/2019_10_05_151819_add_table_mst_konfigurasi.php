<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableMstKonfigurasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_konfigurasi', function (Blueprint $table) {
            $table->text("tentang")->nullable();
            $table->text("skema")->nullable();
            $table->text("default_jawaban")->nullable();
        });

        //setting jawaban 
        $jawaban = array(
            'A' => [
                'label' => 'Sangat Baik',
                'bobot' => 4
            ],
            'B' => [
                'label' => 'Baik',
                'bobot' => 3
            ],
            'C' => [
                'label' => 'Kurang Baik',
                'bobot' => 2
            ],
            'D' => [
                'label' => 'Buruk',
                'bobot' => 1
            ]
        );

        $jawaban = json_encode($jawaban);

        //url to skema
        $skema = '/img/skemaskm.png';

        DB::insert("INSERT INTO mst_konfigurasi(tentang,skema,default_jawaban) VALUES('Pemerintah Daerah sebagai penyedia layanan dituntut untuk senantiasa memberikan pelayanan prima kepada masyarakat. Keberhasilan Pemerintah Daerah dalam menjalankan peranannya tersebut sangat ditentukan pada tingkat kualitas pelayanan publik yang disediakan.
Untuk mengetahui sejauh mana pelayanan publik mampu memenuhi harapan masyarakat di suatu daerah maka diperlukan upaya-upaya untuk selalu memperbaiki pelayanan sehingga sesuai dengan perkembangan zaman dan harapan masyarakat pada saat ini.
Salah satu upaya untuk memperbaiki dan meningkatkan kualitas pelayanan publik tersebut adalah dengan melakukan evaluasi untuk menilai kinerja pelayanan yang disediakan oleh berbagai Perangkat Daerah melalui pelaksanaan Survei Kepuasan Masyarakat.
Kualitas pelayanan publik merupakan sebuah indikator utama dalam penyelenggaraan pemerintahan yang baik. Berhasil atau tidaknya pelayanan kepada masyarakat tidak akan pernah dapat diketahui tanpa pernah dilakukan suatu pengukuran. Pengukuran kepuasan masyarakat merupakan elemen penting dalam proses evaluasi kinerja dimana tujuan akhir yang hendak dicapai adalah menyediakan pelayanan yang lebih baik, lebih efisien, dan lebih efektif berbasis dari kebutuhan masyarakat.
Suatu pelayanan dinilai memuaskan apabila pelayanan tersebut dapat memenuhi kebutuhan dan harapan pengguna layanan. Kepuasan masyarakat dapat juga dijadikan acuan bagi berhasil atau tidaknya pelaksanaan program yang dilaksanakan pada suatu penyelenggara pelayanan publik.

Survei Kepuasan Masyarakat (SKM) adalah pengukuran secara komprehensif kegiatan tentang tingkat kepuasan masyarakat yang diperoleh dari hasil pengukuran atas pendapat masyarakat.

Melalui survei ini diharapkan mendorong partisipasi masyarakat sebagai pengguna layanan dalam menilai kinerja penyelenggara pelayanan publik serta mendorong penyelenggara pelayanan publik untuk meningkatkan kualitas pelayanan dan melakukan pengembangan melalui inovasi-inovasi pelayanan publik.','{$skema}','{$jawaban}');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_konfigurasi');
    }
}
