<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Doctrine\DBAL\Driver\PDOMySql\Driver;

class AddBobotStartEndGrupBobot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('srv_grup_bobot', function (Blueprint $table) {
            $table->renameColumn('bobot', 'bobot_start');
            $table->integer("bobot_end")->nullable()->default(0)->after('bobot');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('srv_grup_bobot', function (Blueprint $table) {
            //
        });
    }
}
