<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableSrvJawaban extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('srv_jawaban', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('id_pertanyaan');
        $table->string('pilihan');
        $table->string('nama');
        $table->integer('bobot');
        $table->boolean('is_deleted');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('srv_jawaban');
    }
}
