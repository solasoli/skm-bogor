<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Str;

class AddColumnGuidSrvPertanyaanUnsur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("TRUNCATE TABLE srv_pertanyaan_unsur;");
        Schema::table('srv_pertanyaan_unsur', function (Blueprint $table) {
            $table->char('guid', 36)->unique()->after("nama");
        });



        DB::insert("INSERT INTO srv_pertanyaan_unsur(nama,guid) VALUES('Persyaratan','".Str::uuid()."'),('Sistem, Mekanisme dan Prosedur','".Str::uuid()."'),('Waktu Penyelasaian','".Str::uuid()."'),('Biaya / Tarif','".Str::uuid()."'),('Produk Spesifikasi Jenis Pelayanan','".Str::uuid()."'),('Kompetensi Pelaksana','".Str::uuid()."'),('Perilaku Pelaksana','".Str::uuid()."'),('Penanganan Pengaduan','".Str::uuid()."'),('Sarana','".Str::uuid()."')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('srv_pertanyaan_unsur', function (Blueprint $table) {
            //
        });
    }
}
