<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaxBobotHasilSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('srv_survey_hasil', function (Blueprint $table) {
            $table->integer("max_bobot")->nullable()->default(0)->after("jml_bobot");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('srv_survey_hasil', function (Blueprint $table) {
            //
        });
    }
}
