<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableSrvSurveyHasilDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('srv_survey_hasil_detail', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('id_hasil');
        $table->bigInteger('id_pertanyaan');
        $table->bigInteger('id_jawaban');
        $table->boolean('is_deleted');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
