<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoTlpHasilSurey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        DB::statement('ALTER TABLE srv_survey_hasil MODIFY email VARCHAR(200) null');
        Schema::table('srv_survey_hasil', function (Blueprint $table) {
            $table->string("no_tlp")->after("email")->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('srv_survey_hasil', function (Blueprint $table) {
            //
        });
    }
}
