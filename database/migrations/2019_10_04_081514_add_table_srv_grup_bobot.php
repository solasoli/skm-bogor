<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableSrvGrupBobot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('srv_grup_bobot', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('id_grup');
        $table->integer('bobot');
        $table->string('nama');
        $table->dateTime('created_at')->nullable();
        $table->integer('created_by');
        $table->dateTime('updated_at')->nullable();
        $table->integer('updated_by');
        $table->dateTime('deleted_at')->nullable();
        $table->integer('deleted_by');
        $table->boolean('is_deleted');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('srv_grup_bobot');
    }
}
