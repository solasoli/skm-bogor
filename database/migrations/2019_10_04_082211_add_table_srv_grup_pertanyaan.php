<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableSrvGrupPertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('srv_grup_pertanyaan', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('id_grup');
        $table->bigInteger('id_pertanyaan');
        $table->boolean('is_deleted');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('srv_grup_pertanyaan');
    }
}
