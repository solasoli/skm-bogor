-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2020 at 02:46 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skm-sukabumi`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl_menu`
--

CREATE TABLE `acl_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `have_child` tinyint(1) NOT NULL DEFAULT '0',
  `id_parent` int(11) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `acl_menu`
--

INSERT INTO `acl_menu` (`id`, `nama`, `url`, `slug`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `is_deleted`, `have_child`, `id_parent`, `level`) VALUES
(1, 'Tentang SKM', '/dashboard/tentang', 'dashboard_tentang', NULL, 0, NULL, 0, NULL, 0, 0, 0, 0, 1),
(2, 'Skema SKM', '/dashboard/skema', 'dashboard_skema', NULL, 0, NULL, 0, NULL, 0, 0, 0, 0, 1),
(3, 'Survey', '#', 'survey', NULL, 0, NULL, 0, NULL, 0, 0, 1, 0, 1),
(4, 'Buat Pertanyaan', '/srv/pertanyaan', 'srv_pertanyaan', NULL, 0, NULL, 0, NULL, 0, 0, 0, 3, 2),
(5, 'Buat Survey', '/srv/grup_pertanyaan', 'srv_grup_pertanyaan', NULL, 0, NULL, 0, NULL, 0, 0, 0, 3, 2),
(6, 'Penerbitan', '/srv/penerbitan', 'srv_penerbitan', NULL, 0, NULL, 0, NULL, 0, 0, 0, 3, 2),
(7, 'IKM', '#', 'ikm', NULL, 0, NULL, 0, NULL, 0, 0, 1, 0, 1),
(8, 'Per Tahun', '/laporan/srv_tahunan', 'laporan_srv_tahunan', NULL, 0, NULL, 0, NULL, 0, 0, 0, 7, 2),
(9, 'Per Bulan', '/laporan/srv_bulanan', 'laporan_srv_bulanan', NULL, 0, NULL, 0, NULL, 0, 0, 0, 7, 2),
(10, 'Per OPD', '/laporan/ikm', 'laporan_ikm', NULL, 0, NULL, 0, NULL, 0, 0, 0, 7, 2),
(11, 'IKM Tingkat Kota', '/laporan/ikm_tingkat_kota', 'laporan_ikm_tingkat_kota', NULL, 0, NULL, 0, NULL, 0, 0, 0, 7, 2),
(12, 'Konfigurasi', '#', 'konfigurasi', NULL, 0, NULL, 0, NULL, 0, 0, 1, 0, 1),
(13, 'Tentang SKM', '/konfigurasi/tentang', 'konfigurasi_tentang', NULL, 0, NULL, 0, NULL, 0, 0, 0, 12, 2),
(14, 'Skema SKM', '/konfigurasi/skema', 'konfigurasi_skema', NULL, 0, NULL, 0, NULL, 0, 0, 0, 12, 2),
(15, 'System', '#', 'system', NULL, 0, NULL, 0, NULL, 0, 0, 1, 0, 1),
(16, 'Role', '/acl/role', 'acl_role', NULL, 0, NULL, 0, NULL, 0, 0, 0, 15, 2),
(17, 'Menu', '/acl/menu', 'acl_menu', NULL, 0, NULL, 0, NULL, 0, 0, 0, 15, 2),
(18, 'Permission', '/acl/permission', 'acl_permission', NULL, 0, NULL, 0, NULL, 0, 0, 0, 15, 2),
(19, 'User', '/acl/user', 'acl_user', NULL, 0, NULL, 0, NULL, 0, 0, 0, 15, 2);

-- --------------------------------------------------------

--
-- Table structure for table `acl_permission`
--

CREATE TABLE `acl_permission` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `view` tinyint(1) NOT NULL,
  `add` tinyint(1) NOT NULL,
  `edit` tinyint(1) NOT NULL,
  `delete` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `acl_permission`
--

INSERT INTO `acl_permission` (`id`, `id_menu`, `id_role`, `view`, `add`, `edit`, `delete`) VALUES
(1, 1, 2, 1, 0, 0, 0),
(2, 2, 2, 1, 0, 0, 0),
(3, 3, 2, 1, 0, 0, 0),
(4, 4, 2, 1, 0, 0, 0),
(5, 5, 2, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `acl_role`
--

CREATE TABLE `acl_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `acl_role`
--

INSERT INTO `acl_role` (`id`, `nama`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `is_deleted`) VALUES
(1, 'Super Administrator', NULL, 0, NULL, 0, NULL, 0, 0),
(2, 'Administrator', NULL, 0, NULL, 0, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_09_21_111220_user_add_username', 1),
(5, '2019_09_21_113233_add_table_acl', 1),
(6, '2019_09_21_113431_add_table_acl_slug', 1),
(7, '2019_09_21_113644_add_column_id_role_user', 1),
(8, '2019_10_04_080503_add_table_srv_grup', 1),
(9, '2019_10_04_081514_add_table_srv_grup_bobot', 1),
(10, '2019_10_04_082211_add_table_srv_grup_pertanyaan', 1),
(11, '2019_10_04_082850_add_table_srv_jawaban', 1),
(12, '2019_10_04_083155_add_table_srv_pertanyaan', 1),
(13, '2019_10_05_151819_add_table_mst_konfigurasi', 1),
(14, '2019_10_05_172631_add_column_id_skpd_table_srv_pertanyaan', 1),
(15, '2019_10_05_174151_add_table_skpd', 1),
(16, '2019_10_05_174631_add_column_id_skpd_table_users', 1),
(17, '2019_10_06_103613_add_column_is_deleted_table_users', 1),
(18, '2019_10_06_140222_add_column_id_skpd_srv_grup', 1),
(19, '2019_10_06_173044_drop_column_timestamp_srv_grup_bobot', 1),
(20, '2019_10_07_065643_add_column_slug_grup_pertanyaan', 1),
(21, '2019_10_07_070734_add_column_guid_table_pertanyaan', 1),
(22, '2019_10_07_101045_add_table_mst_pendidikan', 1),
(23, '2019_10_07_101545_add_table_mst_pekerjaan', 1),
(24, '2019_10_07_120440_add_table_srv_survey_hasil', 1),
(25, '2019_10_07_121226_add_table_srv_survey_hasil_detail', 1),
(26, '2019_10_07_150630_add_table_srv_pertanyaan_tipe', 1),
(27, '2019_10_07_151221_add_column_id_tipe_pertanyaan_srv_pertanyaan', 1),
(28, '2019_10_08_072540_adding_default_jawaban_stuff_on_konfigurasi', 1),
(29, '2019_10_08_083825_update_default_jawaban_by_id_tipe_pertanyaan', 1),
(30, '2019_10_08_084739_add_guid_to_pertanyaan_tipe', 1),
(31, '2019_10_08_152128_add_column_kritik_saran_srv_survey', 1),
(32, '2019_10_08_152548_add_column_tahun_srv_grup', 1),
(33, '2019_10_08_163704_add_column_id_tipe_pertanyaan_srv_grup', 1),
(34, '2019_10_09_084605_add_table_pertanyaan_unsur', 1),
(35, '2019_10_09_090525_add_column_pertanyaan_unsur_srv_pertanyaan', 1),
(36, '2019_10_09_160729_add_column_guid_srv_pertanyaan_unsur', 1),
(37, '2019_10_10_131133_add_is_publish_table_grup_survey', 1),
(38, '2019_10_12_141359_add_max_bobot_hasil_survey', 1),
(39, '2019_10_13_144641_add_bobot_start_end_grup_bobot', 1),
(40, '2019_10_20_144152_remove_unique_slug', 1),
(41, '2019_10_24_033404_add_table_srv_penilaian', 1),
(42, '2019_10_24_132428_add_column_is_custom_srv_pertanyaan', 1),
(43, '2019_11_30_174819_add_acl_menu_additional', 1),
(44, '2020_02_29_035210_add_no_tlp_hasil_surey', 1),
(45, '2020_04_26_002927_seed_report_menus_for_acl_menu_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_konfigurasi`
--

CREATE TABLE `mst_konfigurasi` (
  `tentang` text COLLATE utf8mb4_unicode_ci,
  `skema` text COLLATE utf8mb4_unicode_ci,
  `default_jawaban` text COLLATE utf8mb4_unicode_ci,
  `default_pertanyaan_tipe_selected` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_pertanyaan_tipe_hide` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mst_konfigurasi`
--

INSERT INTO `mst_konfigurasi` (`tentang`, `skema`, `default_jawaban`, `default_pertanyaan_tipe_selected`, `default_pertanyaan_tipe_hide`) VALUES
('Pemerintah Daerah sebagai penyedia layanan dituntut untuk senantiasa memberikan pelayanan prima kepada masyarakat. Keberhasilan Pemerintah Daerah dalam menjalankan peranannya tersebut sangat ditentukan pada tingkat kualitas pelayanan publik yang disediakan.\nUntuk mengetahui sejauh mana pelayanan publik mampu memenuhi harapan masyarakat di suatu daerah maka diperlukan upaya-upaya untuk selalu memperbaiki pelayanan sehingga sesuai dengan perkembangan zaman dan harapan masyarakat pada saat ini.\nSalah satu upaya untuk memperbaiki dan meningkatkan kualitas pelayanan publik tersebut adalah dengan melakukan evaluasi untuk menilai kinerja pelayanan yang disediakan oleh berbagai Perangkat Daerah melalui pelaksanaan Survei Kepuasan Masyarakat.\nKualitas pelayanan publik merupakan sebuah indikator utama dalam penyelenggaraan pemerintahan yang baik. Berhasil atau tidaknya pelayanan kepada masyarakat tidak akan pernah dapat diketahui tanpa pernah dilakukan suatu pengukuran. Pengukuran kepuasan masyarakat merupakan elemen penting dalam proses evaluasi kinerja dimana tujuan akhir yang hendak dicapai adalah menyediakan pelayanan yang lebih baik, lebih efisien, dan lebih efektif berbasis dari kebutuhan masyarakat.\nSuatu pelayanan dinilai memuaskan apabila pelayanan tersebut dapat memenuhi kebutuhan dan harapan pengguna layanan. Kepuasan masyarakat dapat juga dijadikan acuan bagi berhasil atau tidaknya pelaksanaan program yang dilaksanakan pada suatu penyelenggara pelayanan publik.\n\nSurvei Kepuasan Masyarakat (SKM) adalah pengukuran secara komprehensif kegiatan tentang tingkat kepuasan masyarakat yang diperoleh dari hasil pengukuran atas pendapat masyarakat.\n\nMelalui survei ini diharapkan mendorong partisipasi masyarakat sebagai pengguna layanan dalam menilai kinerja penyelenggara pelayanan publik serta mendorong penyelenggara pelayanan publik untuk meningkatkan kualitas pelayanan dan melakukan pengembangan melalui inovasi-inovasi pelayanan publik.', '/img/skemaskm.png', '{\"opsional\":{\n          \"A\":{\"label\":\"Sangat Baik\",\"bobot\":4},\n          \"B\":{\"label\":\"Baik\",\"bobot\":3},\n          \"C\":{\"label\":\"Kurang Baik\",\"bobot\":2},\n          \"D\":{\"label\":\"Buruk\",\"bobot\":1}\n        },\n        \"bintang\":{\n          \"*\":{\"label\":\"*\",\"bobot\":\"1\"},\n          \"**\":{\"label\":\"**\",\"bobot\":\"2\"},\n          \"***\":{\"label\":\"***\",\"bobot\":\"3\"},\n          \"****\":{\"label\":\"****\",\"bobot\":\"4\"}\n        }\n      }', '2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_pekerjaan`
--

CREATE TABLE `mst_pekerjaan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mst_pekerjaan`
--

INSERT INTO `mst_pekerjaan` (`id`, `nama`, `is_deleted`) VALUES
(1, 'PNS', 0),
(2, 'TNI', 0),
(3, 'POLRI', 0),
(4, 'SWASTA', 0),
(5, 'WIRAUSAHA', 0),
(6, 'LAINNYA', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mst_pendidikan`
--

CREATE TABLE `mst_pendidikan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mst_pendidikan`
--

INSERT INTO `mst_pendidikan` (`id`, `nama`, `is_deleted`) VALUES
(1, 'SD', 0),
(2, 'SMP', 0),
(3, 'SMA/K', 0),
(4, 'D1', 0),
(5, 'D2', 0),
(6, 'D3', 0),
(7, 'S1', 0),
(8, 'S2', 0),
(9, 'S3', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mst_skpd`
--

CREATE TABLE `mst_skpd` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mst_skpd`
--

INSERT INTO `mst_skpd` (`id`, `name`, `created_at`, `updated_at`, `is_deleted`, `created_by`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
(1, 'Kabupaten Sukabumi ', NULL, NULL, 0, 0, 0, NULL, 0),
(2, 'Sekretariat Daerah', NULL, NULL, 0, 0, 0, NULL, 0),
(3, 'Sekretaris Dewan Perwakilan Rakyat Daerah', NULL, NULL, 0, 0, 0, NULL, 0),
(4, 'Inspektur', NULL, NULL, 0, 0, 0, NULL, 0),
(5, 'Badan Perencanaan Pembangunan Daerah', NULL, NULL, 0, 0, 0, NULL, 0),
(6, 'Pelaksana Badan Penanggulangan Bencana Daerah', NULL, NULL, 0, 0, 0, NULL, 0),
(7, 'Badan Pengelolaan Keuangan dan Aset Daerah', NULL, NULL, 0, 0, 0, NULL, 0),
(8, 'Badan Pendapatan Daerah', NULL, NULL, 0, 0, 0, NULL, 0),
(9, 'Badan Kepegawaian dan Pengembangan SDM', NULL, NULL, 0, 0, 0, NULL, 0),
(10, 'Badan Kesatuan Bangsa dan Politik', NULL, NULL, 0, 0, 0, NULL, 0),
(11, 'Dinas Lingkungan Hidup', NULL, NULL, 0, 0, 0, NULL, 0),
(12, 'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu', NULL, NULL, 0, 0, 0, NULL, 0),
(13, 'Dinas Pengendalian Penduduk dan Keluarga Berencana', NULL, NULL, 0, 0, 0, NULL, 0),
(14, 'Dinas Pemberdayaan Masyarakat dan Desa', NULL, NULL, 0, 0, 0, NULL, 0),
(15, 'Dinas Pertanahan dan Tata Ruang', NULL, NULL, 0, 0, 0, NULL, 0),
(16, 'Dinas Pariwisata', NULL, NULL, 0, 0, 0, NULL, 0),
(17, 'Dinas Perindustrian dan ESDM', NULL, NULL, 0, 0, 0, NULL, 0),
(18, 'Dinas Kearsipan dan Perpustakaan', NULL, NULL, 0, 0, 0, NULL, 0),
(19, 'Dinas Pemberdayaan Perempuan dan Perlindungan Anak', NULL, NULL, 0, 0, 0, NULL, 0),
(20, 'Dinas Perhubungan', NULL, NULL, 0, 0, 0, NULL, 0),
(21, 'Dinas Pemadam Kebakaran', NULL, NULL, 0, 0, 0, NULL, 0),
(22, 'Dinas Ketahanan Pangan', NULL, NULL, 0, 0, 0, NULL, 0),
(23, 'Dinas Pendidikan', NULL, NULL, 0, 0, 0, NULL, 0),
(24, 'Dinas Kesehatan', NULL, NULL, 0, 0, 0, NULL, 0),
(25, 'Dinas Pekerjaan Umum', NULL, NULL, 0, 0, 0, NULL, 0),
(26, 'Dinas Perumahan dan Kawasan Permukiman', NULL, NULL, 0, 0, 0, NULL, 0),
(27, 'Dinas Komunikasi, Informatika dan Persandian', NULL, NULL, 0, 0, 0, NULL, 0),
(28, 'Dinas Kependudukan dan Pencatatan Sipil', NULL, NULL, 0, 0, 0, NULL, 0),
(29, 'Dinas Sosial', NULL, NULL, 0, 0, 0, NULL, 0),
(30, 'Dinas Tenaga Kerja dan Transmigrasi', NULL, NULL, 0, 0, 0, NULL, 0),
(31, 'Dinas Perdagangan, Koperasi, Usaha Kecil dan Menengah', NULL, NULL, 0, 0, 0, NULL, 0),
(32, 'Dinas Kebudayaan, Kepemudaan dan Olahraga', NULL, NULL, 0, 0, 0, NULL, 0),
(33, 'Dinas Pertanian', NULL, NULL, 0, 0, 0, NULL, 0),
(34, 'Dinas Peternakan', NULL, NULL, 0, 0, 0, NULL, 0),
(35, 'Dinas Kelautan dan Perikanan', NULL, NULL, 0, 0, 0, NULL, 0),
(36, 'Satuan Polisi Pamong Praja', NULL, NULL, 0, 0, 0, NULL, 0),
(37, 'RSUD Sekarwangi', NULL, NULL, 0, 0, 0, NULL, 0),
(38, 'RSUD Palabuhanratu', NULL, NULL, 0, 0, 0, NULL, 0),
(39, 'Kecamatan Bantargadung', NULL, NULL, 0, 0, 0, NULL, 0),
(40, 'Kecamatan Bojonggenteng', NULL, NULL, 0, 0, 0, NULL, 0),
(41, 'Kecamatan Caringin', NULL, NULL, 0, 0, 0, NULL, 0),
(42, 'Kecamatan Ciambar', NULL, NULL, 0, 0, 0, NULL, 0),
(43, 'Kecamatan Cibadak', NULL, NULL, 0, 0, 0, NULL, 0),
(44, 'Kecamatan Cibitung', NULL, NULL, 0, 0, 0, NULL, 0),
(45, 'Kecamatan Cicantayan', NULL, NULL, 0, 0, 0, NULL, 0),
(46, 'Kecamatan Cicurug', NULL, NULL, 0, 0, 0, NULL, 0),
(47, 'Kecamatan Cidadap', NULL, NULL, 0, 0, 0, NULL, 0),
(48, 'Kecamatan Cidahu', NULL, NULL, 0, 0, 0, NULL, 0),
(49, 'Kecamatan Cidolog', NULL, NULL, 0, 0, 0, NULL, 0),
(50, 'Kecamatan Ciemas', NULL, NULL, 0, 0, 0, NULL, 0),
(51, 'Kecamatan Cikakak', NULL, NULL, 0, 0, 0, NULL, 0),
(52, 'Kecamatan Cikembar', NULL, NULL, 0, 0, 0, NULL, 0),
(53, 'Kecamatan Cikidang', NULL, NULL, 0, 0, 0, NULL, 0),
(54, 'Kecamatan Cimanggu', NULL, NULL, 0, 0, 0, NULL, 0),
(55, 'Kecamatan Ciracap', NULL, NULL, 0, 0, 0, NULL, 0),
(56, 'Kecamatan Cireunghas', NULL, NULL, 0, 0, 0, NULL, 0),
(57, 'Kecamatan Cisaat', NULL, NULL, 0, 0, 0, NULL, 0),
(58, 'Kecamatan Cisolok', NULL, NULL, 0, 0, 0, NULL, 0),
(59, 'Kecamatan Curugkembar', NULL, NULL, 0, 0, 0, NULL, 0),
(60, 'Kecamatan Gegerbitung', NULL, NULL, 0, 0, 0, NULL, 0),
(61, 'Kecamatan Gunungguruh', NULL, NULL, 0, 0, 0, NULL, 0),
(62, 'Kecamatan Jampangkulon', NULL, NULL, 0, 0, 0, NULL, 0),
(63, 'Kecamatan Jampangtengah', NULL, NULL, 0, 0, 0, NULL, 0),
(64, 'Kecamatan Kabandungan', NULL, NULL, 0, 0, 0, NULL, 0),
(65, 'Kecamatan Kadudampit', NULL, NULL, 0, 0, 0, NULL, 0),
(66, 'Kecamatan Kalapanunggal', NULL, NULL, 0, 0, 0, NULL, 0),
(67, 'Kecamatan Kalibunder', NULL, NULL, 0, 0, 0, NULL, 0),
(68, 'Kecamatan Kebonpedes', NULL, NULL, 0, 0, 0, NULL, 0),
(69, 'Kecamatan Lengkong', NULL, NULL, 0, 0, 0, NULL, 0),
(70, 'Kecamatan Nagrak', NULL, NULL, 0, 0, 0, NULL, 0),
(71, 'Kecamatan Nyalindung', NULL, NULL, 0, 0, 0, NULL, 0),
(72, 'Kecamatan Pabuaran', NULL, NULL, 0, 0, 0, NULL, 0),
(73, 'Kecamatan Parakansalak', NULL, NULL, 0, 0, 0, NULL, 0),
(74, 'Kecamatan Parungkuda', NULL, NULL, 0, 0, 0, NULL, 0),
(75, 'Kecamatan Palabuhanratu', NULL, NULL, 0, 0, 0, NULL, 0),
(76, 'Kecamatan Purabaya', NULL, NULL, 0, 0, 0, NULL, 0),
(77, 'Kecamatan Sagaranten', NULL, NULL, 0, 0, 0, NULL, 0),
(78, 'Kecamatan Simpenan', NULL, NULL, 0, 0, 0, NULL, 0),
(79, 'Kecamatan Sukabumi', NULL, NULL, 0, 0, 0, NULL, 0),
(80, 'Kecamatan Sukalarang', NULL, NULL, 0, 0, 0, NULL, 0),
(81, 'Kecamatan Sukaraja', NULL, NULL, 0, 0, 0, NULL, 0),
(82, 'Kecamatan Surade', NULL, NULL, 0, 0, 0, NULL, 0),
(83, 'Kecamatan Tegalbuleud', NULL, NULL, 0, 0, 0, NULL, 0),
(84, 'Kecamatan Waluran', NULL, NULL, 0, 0, 0, NULL, 0),
(85, 'Kecamatan Warungkiara', NULL, NULL, 0, 0, 0, NULL, 0),
(86, 'PDAM Kab. Sukabumi', NULL, NULL, 0, 0, 0, NULL, 0),
(87, 'Perumda BPR', NULL, NULL, 0, 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `srv_grup`
--

CREATE TABLE `srv_grup` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_skpd` int(11) NOT NULL,
  `id_tipe_pertanyaan` bigint(20) NOT NULL,
  `tahun` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `is_publish` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `srv_grup_bobot`
--

CREATE TABLE `srv_grup_bobot` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_grup` bigint(20) NOT NULL,
  `bobot_start` int(11) NOT NULL,
  `bobot_end` int(11) DEFAULT '0',
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `srv_grup_pertanyaan`
--

CREATE TABLE `srv_grup_pertanyaan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_grup` bigint(20) NOT NULL,
  `id_pertanyaan` bigint(20) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `srv_jawaban`
--

CREATE TABLE `srv_jawaban` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_pertanyaan` bigint(20) NOT NULL,
  `pilihan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobot` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `srv_penilaian`
--

CREATE TABLE `srv_penilaian` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nilai_persepsi` int(11) NOT NULL,
  `nilai_interval_from` decimal(6,4) NOT NULL,
  `nilai_interval_to` decimal(6,4) NOT NULL,
  `nilai_interval_konversi_from` decimal(6,2) NOT NULL,
  `nilai_interval_konversi_to` decimal(6,2) NOT NULL,
  `mutu_pelayanan` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kinerja_unit_pelayanan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `srv_penilaian`
--

INSERT INTO `srv_penilaian` (`id`, `nilai_persepsi`, `nilai_interval_from`, `nilai_interval_to`, `nilai_interval_konversi_from`, `nilai_interval_konversi_to`, `mutu_pelayanan`, `kinerja_unit_pelayanan`) VALUES
(1, 1, '1.0000', '2.5996', '25.00', '64.99', 'D', 'Tidak baik'),
(2, 2, '2.6000', '3.0640', '65.00', '76.60', 'C', 'Kurang baik'),
(3, 3, '3.0644', '3.5320', '76.61', '88.30', 'B', 'Baik'),
(4, 4, '3.5324', '4.0000', '88.31', '100.00', 'A', 'Sangat baik');

-- --------------------------------------------------------

--
-- Table structure for table `srv_pertanyaan`
--

CREATE TABLE `srv_pertanyaan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `guid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_tipe_pertanyaan` int(11) NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_skpd` int(11) NOT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `id_pertanyaan_unsur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `srv_pertanyaan_tipe`
--

CREATE TABLE `srv_pertanyaan_tipe` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `guid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `srv_pertanyaan_tipe`
--

INSERT INTO `srv_pertanyaan_tipe` (`id`, `nama`, `is_deleted`, `guid`) VALUES
(1, 'Pilihan Ganda', 0, '4ec06b6b-d590-4d51-a9a3-ea0aa6ff89c1'),
(2, 'Bintang', 0, '89aef3ca-a3ce-4807-b10d-c20228c474b1');

-- --------------------------------------------------------

--
-- Table structure for table `srv_pertanyaan_unsur`
--

CREATE TABLE `srv_pertanyaan_unsur` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `srv_pertanyaan_unsur`
--

INSERT INTO `srv_pertanyaan_unsur` (`id`, `nama`, `guid`, `is_deleted`) VALUES
(1, 'Persyaratan', '19e99819-3f11-4297-b06f-3c1269b424ed', 0),
(2, 'Sistem, Mekanisme dan Prosedur', '46d62e4a-a97d-4859-831a-a35c2d8f4e2e', 0),
(3, 'Waktu Penyelasaian', 'd1180266-edc0-4c69-8fe0-118822887414', 0),
(4, 'Biaya / Tarif', '8f1731bb-8818-4062-a917-cacf9a853c66', 0),
(5, 'Produk Spesifikasi Jenis Pelayanan', '3c4ac5ef-70c0-495a-97c2-ca20ea6eaee9', 0),
(6, 'Kompetensi Pelaksana', 'b31dd4b5-ee1f-4e61-8edd-bc426d4dcd06', 0),
(7, 'Perilaku Pelaksana', '2980bf82-6308-4d02-bca6-c6a647082e6e', 0),
(8, 'Penanganan Pengaduan', '28f86b8c-3ed6-4903-a11e-e0f3d3576529', 0),
(9, 'Sarana', '4101f5da-2539-4ec1-96c1-71276393accb', 0);

-- --------------------------------------------------------

--
-- Table structure for table `srv_survey_hasil`
--

CREATE TABLE `srv_survey_hasil` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_grup` bigint(20) NOT NULL,
  `jml_bobot` int(11) NOT NULL,
  `max_bobot` int(11) DEFAULT '0',
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usia` int(11) NOT NULL,
  `jk` enum('L','P') COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_pendidikan` int(11) NOT NULL,
  `id_pekerjaan` int(11) NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_tlp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kritik_saran` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `srv_survey_hasil_detail`
--

CREATE TABLE `srv_survey_hasil_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_hasil` bigint(20) NOT NULL,
  `id_pertanyaan` bigint(20) NOT NULL,
  `id_jawaban` bigint(20) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_skpd` int(11) DEFAULT '0',
  `is_deleted` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `username`, `id_role`, `id_skpd`, `is_deleted`) VALUES
(1, 'Super Administrator', 'super_administrator@localhost.com', NULL, '$2y$10$lUy7v0Xo5txLGYWjCMcTYOySOqWabKcbobxuWYBtOncReHAUHFmJu', NULL, NULL, NULL, 'admin', 1, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_menu`
--
ALTER TABLE `acl_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acl_permission`
--
ALTER TABLE `acl_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acl_role`
--
ALTER TABLE `acl_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_pekerjaan`
--
ALTER TABLE `mst_pekerjaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_pendidikan`
--
ALTER TABLE `mst_pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_skpd`
--
ALTER TABLE `mst_skpd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `srv_grup`
--
ALTER TABLE `srv_grup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `srv_grup_bobot`
--
ALTER TABLE `srv_grup_bobot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `srv_grup_pertanyaan`
--
ALTER TABLE `srv_grup_pertanyaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `srv_jawaban`
--
ALTER TABLE `srv_jawaban`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `srv_penilaian`
--
ALTER TABLE `srv_penilaian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `srv_pertanyaan`
--
ALTER TABLE `srv_pertanyaan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `srv_pertanyaan_guid_unique` (`guid`);

--
-- Indexes for table `srv_pertanyaan_tipe`
--
ALTER TABLE `srv_pertanyaan_tipe`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `srv_pertanyaan_tipe_guid_unique` (`guid`);

--
-- Indexes for table `srv_pertanyaan_unsur`
--
ALTER TABLE `srv_pertanyaan_unsur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `srv_pertanyaan_unsur_guid_unique` (`guid`);

--
-- Indexes for table `srv_survey_hasil`
--
ALTER TABLE `srv_survey_hasil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `srv_survey_hasil_detail`
--
ALTER TABLE `srv_survey_hasil_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acl_menu`
--
ALTER TABLE `acl_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `acl_permission`
--
ALTER TABLE `acl_permission`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `acl_role`
--
ALTER TABLE `acl_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `mst_pekerjaan`
--
ALTER TABLE `mst_pekerjaan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mst_pendidikan`
--
ALTER TABLE `mst_pendidikan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mst_skpd`
--
ALTER TABLE `mst_skpd`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=473;

--
-- AUTO_INCREMENT for table `srv_grup`
--
ALTER TABLE `srv_grup`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `srv_grup_bobot`
--
ALTER TABLE `srv_grup_bobot`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `srv_grup_pertanyaan`
--
ALTER TABLE `srv_grup_pertanyaan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `srv_jawaban`
--
ALTER TABLE `srv_jawaban`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `srv_penilaian`
--
ALTER TABLE `srv_penilaian`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `srv_pertanyaan`
--
ALTER TABLE `srv_pertanyaan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `srv_pertanyaan_tipe`
--
ALTER TABLE `srv_pertanyaan_tipe`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `srv_pertanyaan_unsur`
--
ALTER TABLE `srv_pertanyaan_unsur`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `srv_survey_hasil`
--
ALTER TABLE `srv_survey_hasil`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `srv_survey_hasil_detail`
--
ALTER TABLE `srv_survey_hasil_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
