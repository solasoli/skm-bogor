<?php

use Illuminate\Database\Seeder;

class PenilaianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('srv_penilaian')->insert([
        [
          'nilai_persepsi' => 1,
          'nilai_interval_from' => 1,
          'nilai_interval_to' => 2.5996,
          'nilai_interval_konversi_from' => 25,
          'nilai_interval_konversi_to' => 64.99,
          'mutu_pelayanan' => 'D',
          'kinerja_unit_pelayanan' => 'Tidak baik',
        ]
      ]);
      DB::table('srv_penilaian')->insert([
        [
          'nilai_persepsi' => 2,
          'nilai_interval_from' => 2.6,
          'nilai_interval_to' => 3.064,
          'nilai_interval_konversi_from' => 65,
          'nilai_interval_konversi_to' => 76.6,
          'mutu_pelayanan' => 'C',
          'kinerja_unit_pelayanan' => 'Kurang baik',
        ]
      ]);
      DB::table('srv_penilaian')->insert([
        [
          'nilai_persepsi' => 3,
          'nilai_interval_from' => 3.0644,
          'nilai_interval_to' => 3.532,
          'nilai_interval_konversi_from' => 76.61,
          'nilai_interval_konversi_to' => 88.3,
          'mutu_pelayanan' => 'B',
          'kinerja_unit_pelayanan' => 'Baik',
        ]
      ]);
      DB::table('srv_penilaian')->insert([
        [
          'nilai_persepsi' => 4,
          'nilai_interval_from' => 3.5324,
          'nilai_interval_to' => 4,
          'nilai_interval_konversi_from' => 88.31,
          'nilai_interval_konversi_to' => 100,
          'mutu_pelayanan' => 'A',
          'kinerja_unit_pelayanan' => 'Sangat baik',
        ]
      ]);
    }
}
