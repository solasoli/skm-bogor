<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => 1,
                'name' => "Super Administrator",
                'email' => "super_administrator@localhost.com", // kolom level harusnya udah ga ada
                'password' => Hash::make('12345'),
                'username' => 'admin',
                'id_role' => 1 // role Super Administrator
            ]
        ]);
    }
}
