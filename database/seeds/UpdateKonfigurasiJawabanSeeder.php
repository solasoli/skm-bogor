<?php

use Illuminate\Database\Seeder;

class UpdateKonfigurasiJawabanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // $default_jawaban = '{"opsional":{"A":{"label":"Sangat Baik","bobot":4},"B":{"label":"Baik","bobot":3},"C":{"label":"Kurang Baik","bobot":2},"D":{"label":"Buruk","bobot":1}},"bintang":{"*":{"label":"*","bobot":"1"},"**":{"label":"**","bobot":"2"},"***":{"label":"***","bobot":"3"},"****":{"label":"****","bobot":"4"},"*****":{"label":"*****","bobot":"5"}}}';
      $default_jawaban = '{"opsional":{
          "A":{"label":"Sangat Baik","bobot":4},
          "B":{"label":"Baik","bobot":3},
          "C":{"label":"Kurang Baik","bobot":2},
          "D":{"label":"Buruk","bobot":1}
        },
        "bintang":{
          "*":{"label":"*","bobot":"1"},
          "**":{"label":"**","bobot":"2"},
          "***":{"label":"***","bobot":"3"},
          "****":{"label":"****","bobot":"4"}
        }
      }';
      DB::update("UPDATE mst_konfigurasi SET default_jawaban='{$default_jawaban}'");
    }
}
