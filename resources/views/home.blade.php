@extends('layouts.app')

@section('content')
    @if(Session::has('message'))
        <p class="alert alert-success">{!! Session::get('message') !!}</p>
    @endif
    <div class="containter">
        <h3>Dashboard</h3>
        <div class="row top_tiles">

            <form class="form-horizontal" method="GET">


                @include('laporan.partials.opd_chart_yearly', compact('ikm_tahunan_asc', 'ikm_tahunan_desc', 'selectedYear'))


                @if(getLoggedInUser()->isAbleToAccessRoute('laporan.ikm_tingkat_kota'))
                    @include('laporan.partials.admin.yearly_option', [
                       'availableYears' => $availableYearForOPDReportYearly,
                       'selectedYear' => $selectedOPDYearlyReportingYear ?? null,
                       'fieldName' => 'opd_reporting_year',
                       'title' => 'Filter Laporan Per Tahun',
                     ])

                    @include('laporan.partials.ikm_per_city_yearly', [
                      'ikmPerCityYearly' => $ikmPerCityYearly,
                      'selectedYear' => $selectedOPDYearlyReportingYear
                    ])

                @endif


                @if(isAdmin())

                    @include('laporan.partials.admin.yearly_option', [
                        'availableYears' => $availableYearForOPDReportYearly,
                        'selectedYear' => $selectedYearForOPDWithEmptySurvey ?? null,
                        'fieldName' => 'empty_survey_year',
                        'title' => "Filter OPD yang belum mempublish survey",
                      ])

                    @include('dashboard.admin.opd_without_published_survey', [
                      'selectedYear' => $selectedYearForOPDWithEmptySurvey
                    ])
                @endif

                @include('dashboard.admin.respondents');
            </form>

        </div>
    </div>
    <script>

        $(function () {
            setTimeout(function () {
                $(".alert-success").hide(1000);
            }, 5000);
        });
    </script>
@endsection
