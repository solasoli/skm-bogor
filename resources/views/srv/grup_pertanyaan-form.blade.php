@extends('layouts.app')

@section('content')
<script type="text/javascript">

  function add_custom_pertanyaan_field(guid, value){
    var val = typeof value != 'undefined' ? value : ''; 
    var cover_cp = $('#cover_cp_' + guid);
    var q = '';
    var no = $('#cover_cp_' + guid + " tr").length;
    q += '<tr>';
    q += '<td>'+ no +'</td>';
    q += '<td><textarea style="width:100%" name="custom_pertanyaan['+guid+'][]">' + val + '</textarea></td>';
    q += '<td></td>';
    q += '</tr>';
    cover_cp.append(q);
  }

  function check_custom_pertanyaan(guid, checked){
    console.log(guid);
    var cover_cp = $('#cover_cp_' + guid);
    var cover_button_addmore_cp = $('#cover_button_addmore_cp_' + guid);
    var q = '';
    var b = '';

    if(checked) {
      q += '<tr>';
      q += '<th width="10px">No</th>';
      q += '<th>Pertanyaan</th>';
      q += '<th width="10px">&nbsp;</th>';
      q += '</tr>';


      b += '<button type="button" class="btn btn-sm btn-dark button_addmore_cp_'+guid+'">+ add more</button>';

      cover_cp.html(q);
      cover_button_addmore_cp.html(b);

      // Add more custom pertanyaan
      $('.button_addmore_cp_'+guid).on('click', function(){
        add_custom_pertanyaan_field(guid);
      });

      // Remove custom pertanyaan
      $("#cover_cp_"+guid).on('click', '.button_remove_cp', function(e){
        $(this).parent('td').parent('tr').remove();
        no--;
      });
    }
    else {
      cover_cp.html('');
      cover_button_addmore_cp.html('');
    }
  }
</script>
<div class="page-title">
  <div class="title_left">
    <h3>Survey</h3>
  </div>
</div>
<div class="clearfix"></div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
                <li>&#8226; {{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session::has('message'))
  <p class="alert alert-success">{!! Session::get('message') !!}</p>
@endif
<form class="form-horizontal" method="POST">
  {{ csrf_field() }}
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <!-- <div class="x_title">
        <h2></h2>
        <div class="clearfix"></div>
      </div> -->
      <div class="x_content">
        <div class="form-group {{ must_show_skpd_form() ? '' : 'hide'}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            OPD <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='skpd' class="form-control select2" id="skpd">
              @foreach($skpd as $id => $row)

                @php
                  $selected = !is_null(old('skpd')) ? old('skpd') : (isset($data) ? $data->id_skpd : '');
                  $selected = $selected == $row->id ? "selected" : "";
                @endphp

                <option value="{{$row->id}}" {{$selected}}>{{$row->name}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Judul Survey <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input name='judul' id='judul' value='{{ !is_null(old('judul')) ? old('judul') : (isset($judul) ? $judul : '') }}' required class="form-control col-md-7 col-xs-12" type="text">
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" style="display: none">
            {{-- Slug <span class="required">*</span> : --}}
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12" style="display: none">
            <input name='slug' id='slug' autocomplete="off" value='{{ !is_null(old('slug')) ? old('slug') : (isset($slug) ? $slug : '') }}' required class="form-control col-md-7 col-xs-12" type="text" style="display: none">
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Tahun <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input name='tahun' autocomplete="off" value='{{ !is_null(old('tahun')) ? old('tahun') : (isset($data) ? $data->tahun : '') }}' required class="form-control col-md-7 col-xs-12" type="text">
          </div>
        </div>
        <div class="form-group {{ isset($konfigurasi->default_pertanyaan_tipe_hide) && $konfigurasi->default_pertanyaan_tipe_hide == 1 ? 'hide' : '' }}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Tipe Pertanyaan <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='tipe_pertanyaan' id="tipe_pertanyaan" class="form-control select2">
              @foreach($pertanyaan_tipe as $idx => $row)
              @php
              $automatic_selected = isset($konfigurasi->default_pertanyaan_tipe_selected) ? $konfigurasi->default_pertanyaan_tipe_selected : 0;
              $selected_pertanyaan_tipe = !is_null(old('tipe_pertanyaan')) && old('tipe_pertanyaan') == $row->guid ? 'selected' : (isset($data) && $data->id_tipe_pertanyaan == $row->id ? 'selected' : ($automatic_selected == $row->id ? 'selected' : ''));
              @endphp
                <option value="{{$row->guid}}" {{$selected_pertanyaan_tipe}}>{{$row->nama}}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row hide">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Penilaian</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table border="0" width="100%" class="table table-striped field_wrapper">
          <tr>
            <th style="width: 200px">Range Nilai</th>
            <th>Predikat</th>
            <th width="5%"></th>
          </tr>
          <?php
          $x = 0;

          // dd(old());
          if(is_array(old("bobot_start"))){
            foreach(old("bobot_start") as $idx => $row){
          ?>
                <tr>
                  <td>
                    <div style="display: inline-block;">
                      <input type="text" required class="form-control digit-only" name="bobot_start[]" value="{{ !is_null(old('bobot_start[]')) ? old('bobot_start[]') : $row }}" style="width:80px">
                    </div>
                    <div style="display: inline-block;">
                      -
                    </div>
                    <div style="display: inline-block;">
                      <input type="text" required class="form-control digit-only" name="bobot_end[]" value="{{ !is_null(old('bobot_end[]')) ? old('bobot_end[]') : old('bobot_end')[$idx] }}" style="width:80px">
                    </div>
                  </td>
                  <td><input type="text" required class="form-control" name="nama[]" value="{{old('nama')[$idx]}}"></td>
                  <td>
                    <?= $idx > 0 ? '<a href="javascript:void(0);" class="remove_button"><span class="fa fa-close"></span></a>' : ''; ?>
                  </td>
                </tr>
          <?php
              $x++;
            }
          } else if(isset($query_bobot) && count($query_bobot) > 0){ // jika form edit
            foreach($query_bobot as $idx => $row){
              $x++;
          ?>
            <tr>
              <td>
                <div style="display: inline-block;">
                  <input type="text" required class="form-control digit-only" name="bobot_start[]" value="{{ !is_null(old('bobot_start[]')) ? old('bobot_start[]') : $row->bobot_start }}" style="width:80px">
                </div>
                <div style="display: inline-block;">
                  -
                </div>
                <div style="display: inline-block;">
                  <input type="text" required class="form-control digit-only" name="bobot_end[]" value="{{ !is_null(old('bobot_end[]')) ? old('bobot_end[]') : $row->bobot_end }}" style="width:80px">
                </div>
              </td>
              <td><input type="text" required class="form-control" name="nama[]" value="{{ !is_null(old('nama[]')) ? old('nama[]') : $row->nama }}"></td>
              <td>
                <?=$idx > 0 ? '<a href="javascript:void(0);" class="remove_button"><span class="fa fa-close"></span></a>' : ''; ?>
              </td>
            </tr>
        <?php }
            } else { // jika form add ?>
          @foreach($penilaian AS $idx => $row)
          <tr>
            <td>
              <div style="display: inline-block;">
                <input type="text" required class="form-control digit-only" name="bobot_start[]" value="{{$row->nilai_interval_konversi_from}}" style="width:80px">
              </div>
              <div style="display: inline-block;">
                -
              </div>
              <div style="display: inline-block;">
                <input type="text" required class="form-control digit-only" name="bobot_end[]" value="{{$row->nilai_interval_konversi_to}}" style="width:80px">
              </div>
            </td>
            <td><input type="text" required class="form-control" name="nama[]" value="{{$row->kinerja_unit_pelayanan}}"></td>
            <td></td>
          </tr>
          @endforeach
        <?php
          $x++;
        } ?>
        </table>
        <button type="button" name="button" class="btn btn-info add_button">+ add more</button>
      </div>
    </div>
  </div>
</div>

@foreach($pertanyaan_unsur as $i => $r)
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Unsur {{$i + 1}} - {{ $r->nama }}</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table border="0" class="table">
          <tr>
            <th valign="top" style="width: 300px; border:none">Buat pertanyaan secara otomatis sebanyak</th>
            <th style="width:10px;border:none">:</th>
            <th style="border:none">
              <input type="number" data-guid="{{$r->guid}}" name="jml_pertanyaan[{{$r->guid}}]" class="form-control col-md-3 jml_pertanyaan digit-only" value="{{ !is_null(old('jml_pertanyaan')[$r->guid]) ? old('jml_pertanyaan')[$r->guid] : (isset($jml_pertanyaan[$r->guid]) ? $jml_pertanyaan[$r->guid] : '0') }}" style="width:80px">
              &nbsp;
              <button type="button" name="generate_pertanyaan" class="generate_pertanyaan btn btn-dark mx-sm-3" data-guid="{{$r->guid}}">Hasilkan</button></td>
            </th>
          </tr>
        </table>
        <table border="0" width="100%" class="table table-striped">
          <tr>
            <th width="5%" class="text-center">No</th>
            <th>Pertanyaan</th>
          </tr>
          <tbody class="field_wrapper2" data-guid="{{$r->guid}}">
            <?php
            $i = 0;
            if(isset(old("id_pertanyaan")[$r->guid])){
              ?>
              <?php
              foreach(old("id_pertanyaan")[$r->guid] as $idx => $row){
                $i++;
                ?>
                <tr>
                  <td align="center"><label class="no_pertanyaan">{{$i}}</label></td>
                  <td>
                    <select class="form-control pertanyaan" name="id_pertanyaan[{{$r->guid}}][]">
                      <option value="{{$row}}" selected="selected">{{old("label_pertanyaan")[$idx]}}</option> <!-- -->
                    </select>
                    <input type="hidden" name="label_pertanyaan[]" class="label_pertanyaan" value="{{old('label_pertanyaan')[$idx]}}">
                  </td>
                </tr>
                <?php
              } }
              else if(isset($query_pertanyaan)){ // jika edit
                ?>
                <?php
                foreach($query_pertanyaan->where("is_custom", 0) AS $idx => $row){
                  if($row->pu_guid == $r->guid){
                  $i++;
                  ?>
                  <tr>
                    <td align="center"><label class="no_pertanyaan">{{$i}}</label></td>
                    <td>
                      <select class="form-control pertanyaan" name="id_pertanyaan[{{$r->guid}}][]" >
                        <option value="{{$row->guid}}" selected>{{$row->pertanyaan}}</option>
                      </select>
                    </td>
                  </tr>
                <?php }}} ?>
              </tbody>
        </table>
        <div id="wrapper_addmore_pertanyaan">
          <button type="button" class="btn btn-info add_pertanyaan" data-guid="{{$r->guid}}">+ add more</button>
        </div>
        <br>
        <!-- Custom Pertanyaan -->
        @php
        $checked_cp = !is_null(old("custom_pertanyaan_{$r->guid}")) && old("custom_pertanyaan_{$r->guid}") ? 'checked' : (isset($query_pertanyaan) && $query_pertanyaan->where('is_custom',1)->where("pu_guid", $r->guid)->count() > 0 ? 'checked' : '');
        @endphp
        <p><input type="checkbox" {{$checked_cp}} name="custom_pertanyaan_{{$r->guid}}" class="custom_pertanyaan" data-guid="{{$r->guid}}"> Buat Pertanyaan Sendiri</p>
        <table border="0" class="table" width="100%">
          <tbody id="cover_cp_{{$r->guid}}">
          </tbody>
        </table>
        <div id="cover_button_addmore_cp_{{$r->guid}}">
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  
  @if(isset($query_pertanyaan)) // jika edit

    check_custom_pertanyaan("{{$r->guid}}", $(".custom_pertanyaan[data-guid='{{$r->guid}}']").prop("checked"));
    @foreach($query_pertanyaan->where("is_custom", 1)->where("pu_guid", $r->guid) as $idx => $row)
      add_custom_pertanyaan_field("{{$r->guid}}", "{{$row->pertanyaan}}");
    
    @endforeach
  @endif

</script>
@endforeach

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <!-- <div class="x_title">
        <h2></h2>
        <div class="clearfix"></div>
      </div> -->
      <div class="x_content">
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <a href='{{url('')}}/srv/grup_pertanyaan' class="btn btn-primary" type="button">Cancel</a>
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
<script>
function updateLabel(el){
  var option_label = $(el).find($("option:selected")).html();
  $(el).parent().find($(".label_pertanyaan")).val(option_label);
}

$(function(){

  // Custom Pertanyaan
  $(".custom_pertanyaan").change(function() {
    var guid = $(this).data('guid');
    check_custom_pertanyaan(guid, $(this).prop("checked"));
    if($(this).prop("checked")){
      add_custom_pertanyaan_field(guid);
    }
  });



  var x_length = [];

  @foreach($pertanyaan_unsur as $i => $r)
  x_length['{{$r->guid}}'] = 1;
  @endforeach

  @if(isset($query_pertanyaan))
  @foreach($query_pertanyaan as $i => $r)
    x_length['{{$r->pu_guid}}']++;
  @endforeach
  @endif

  $("#judul").on('keyup keydown', function(){
    $("#slug").val(($(this).val().toString().toLowerCase()).replace(/\s/g, "-"));
  })

  setTimeout(function() {
    $(".alert-success").hide(1000);
  }, 3000);

  var maxField = 10; //Input fields increment limitation
  var addButton = $('.add_button'); //Add button selector
  var wrapper = $('.field_wrapper'); //Input field wrapper
  var fieldHTML = '<tr>';
  fieldHTML += '<td>';
  fieldHTML += '<div style="display: inline-block;">';
  fieldHTML += '<input type="number" required class="form-control digit-only" name="bobot_start[]" value="0" style="width:80px">';
  fieldHTML += '</div>';
  fieldHTML += '<div style="display: inline-block;">&nbsp;-&nbsp;</div>';
  fieldHTML += '<div style="display: inline-block;">';
  fieldHTML += '<input type="number" required class="form-control digit-only" name="bobot_end[]" value="0" style="width:80px">';
  fieldHTML += '</div>';
  fieldHTML += '</td>';
  fieldHTML += '<td>';
  fieldHTML += '<input type="text" required class="form-control" name="nama[]">';
  fieldHTML += '</td>';
  fieldHTML += '<td><a href="javascript:void(0);" class="remove_button"><span class="fa fa-close"></span></a></td>';

  fieldHTML += '</tr>';
  var is_edit = '<?=isset($query_bobot) ? "1" : "0"?>';
  var x = '<?=isset($x) ? "{$x}" : "1"?>';

  //Once add button is clicked
  $(addButton).click(function(){
    //Check maximum number of input fields
    if(x < maxField){
      x++; //Increment field counter
      $(wrapper).append(fieldHTML); //Add field html
    }
  });

  //Once remove button is clicked
  $(wrapper).on('click', '.remove_button', function(e){
    e.preventDefault();
    $(this).parent('td').parent('tr').remove(); //Remove field html
    x--; //Decrement field counter
  });

  $('.add_pertanyaan').on('click', function(){
    var guid = $(this).data("guid");

    var fieldHTML2 = '<tr>';
    fieldHTML2 += '<td align="center"><label class="no_pertanyaan"></label></td>';
    fieldHTML2 += '<td><select class="form-control pertanyaan" name="id_pertanyaan[' + guid + '][]" onchange="updateLabel(this);"></select><input type="hidden" name="label_pertanyaan[]" class="label_pertanyaan"></td>';
    fieldHTML2 += '</tr>';

    //console.log();
    var x = 1;
    if(typeof x_length[guid] != 'undefined'){
      x = x_length[guid];
      x_length[guid]++;
    }  else {
      x_length[guid] = 1;
    }
    $('.field_wrapper2[data-guid="' + guid + '"]').append(fieldHTML2);
    $('.field_wrapper2[data-guid="' + guid + '"]').find('.no_pertanyaan').last().append(x);
    get_pertanyaan(guid);
  });

  var wrapper2 = $('.field_wrapper2'); //Input field wrapper2


  /* Generate Pertanyaan */
  $('.generate_pertanyaan').click(function(){
    var tipe_pertanyaan = $('#tipe_pertanyaan').find('option:selected').val();
    var jml_pertanyaan = [];

    var guid = $(this).data("guid");

    x_length[guid] = 1;

    // Remove
    $('.field_wrapper2[data-guid="' + guid + '"]').find('.no_pertanyaan').parent('td').parent('tr').remove(); // remove pertanyaan
    $('.field_wrapper2[data-guid="' + guid + '"]').find('.control-label').parent('td').parent('tr').remove(); // remove TH
    $('#wrapper_addmore_pertanyaan').find('p').remove();


    // Auto generate pertanyaan
    $.post('{{url("")}}/srv/grup_pertanyaan/get_pertanyaan_random/' + tipe_pertanyaan, {jml_pertanyaan: $(".jml_pertanyaan[data-guid='" + guid +"']").val(), guid: guid, id_skpd: $('#skpd').val()}) //
    .done(function(data){
      console.log(data.length);

      var x = 1;
      if(typeof x_length[guid] != 'undefined'){
        x = x_length[guid];
        x_length[guid]++;
      }  else {
        x_length[guid] = 1;
      }

      if(jml_pertanyaan > 0 && jml_pertanyaan != '' && data.length > 0){
        $('#wrapper_addmore_pertanyaan').append('<p><br><button type="button" class="btn btn-info" id="button_addmore_pertanyaan" name="button">+ add more</button></p>');
        addmore_pertanyaan_onclick(data.length);
      }

      var fieldHTML2 = '<tr>';
      fieldHTML2 += '<td align="center"><label class="no_pertanyaan"></label></td>';
      fieldHTML2 += '<td><select class="form-control pertanyaan" name="id_pertanyaan[' + guid + '][]" onchange="updateLabel(this);"></select><input type="hidden" name="label_pertanyaan[]" class="label_pertanyaan"></td>';
      fieldHTML2 += '</tr>';


      $.each(data, function(idx, row){
        $('.field_wrapper2[data-guid="' + guid + '"]').append(fieldHTML2);
        $('.field_wrapper2[data-guid="' + guid + '"]').find('.no_pertanyaan').last().append(x);

        var option = new Option(row.nama, row.guid);
        option.selected = true;
        $(".field_wrapper2[data-guid='" + guid + "'] .pertanyaan").append(option);
        $(".field_wrapper2[data-guid='" + guid + "'] .pertanyaan").trigger("change");

        x++;
      })
      get_pertanyaan();
    });
  });

  /* select2 untuk pertanyaan */
  function get_pertanyaan(){
    $('.pertanyaan').select2({
      placeholder: 'Pilih pertanyaan',
      ajax: {
        url: '{{url("")}}/srv/grup_pertanyaan/get_pertanyaan_select2/' + $('#tipe_pertanyaan').find('option:selected').val(),
        dataType: 'json',
        delay: 250,
        type: 'POST',
        data: function (term) {
            return {
              search: term.term,
              guid: $(this.context).closest(".field_wrapper2").data("guid"),
              id_skpd: $("#skpd").find('option:selected').val(),
            };
        },
        processResults: function (data) {
          return {
            results: $.map(data, function (r) {
              return {
                text: r.nama,
                id: r.guid,
              }
            }),
          };
        },
        cache: true
      },
      minimumInputLength: 1,
      templateResult: formatRepo,
      templateSelection: formatRepoSelection,
      escapeMarkup: function (markup) {
        return markup;
      }
    });

    function formatRepo (repo) {
      if (repo.loading) {
        return repo.text;
      }

      var markup = repo.text;
      return markup;
    }

    function formatRepoSelection (repo) {
      return repo.text || repo.id;
    }
  }
  /* end */

  get_pertanyaan();

});
</script>
@endsection
