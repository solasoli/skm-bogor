@extends('layouts.app')

@section('content')

    <div class="page-title">
      <div class="title_left">
        <h3>Pertanyaan</h3>
      </div>
    </div>

    <div class="clearfix"></div>
    @if(Session::has('message'))
      <p class="alert alert-success">{!! Session::get('message') !!}</p>
    @endif
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <!-- <h2>List</h2> -->

            <div class="pull-right">
              @if(can_access_from_url("add"))
                <a class='btn btn-sm btn-success' href='{{url()->current()}}/add'><i class='fa fa-plus'></i> Tambah</a>
              @endif
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table class="table table-bordered table-striped" id="users-table" style="width:100%">
             <thead>
               <tr>
                 <th>Pertanyaan</th>
                 {{--@if(must_show_skpd_form())
                 <th>OPD</th>
                 @endif--}}

                @if((isset($konfigurasi->default_pertanyaan_tipe_hide) && $konfigurasi->default_pertanyaan_tipe_hide == 0) || !isset($konfigurasi->default_pertanyaan_tipe_hide))
                 <th>Tipe Pertanyaan</th>
                 @endif
                 <th>Unsur Pertanyaan</th>
                 <th width="180px">Aksi</th>
               </tr>
             </thead>
           </table>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script>

$(function() {
  $('#users-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{{url()->current()}}/datatables/',
      columns: [
        { data: 'pertanyaan', name: 'p.nama' },
        
        {{--@if(must_show_skpd_form())
        { data: 'skpd', name: 's.name' },
        @endif--}}

        @if((isset($konfigurasi->default_pertanyaan_tipe_hide) && $konfigurasi->default_pertanyaan_tipe_hide == 0) || !isset($konfigurasi->default_pertanyaan_tipe_hide))
        { data: 'tipe_pertanyaan', name: 'pt.nama' },
        @endif

        { data: 'pertanyaan_unsur', name: 'pu.nama' },

        { data: null, searchable: false, orderable: false, render: function ( data, type, row ) {
          var return_button = "";
          return_button += "<a class='btn btn-info btn-xs' href='#' data-pertanyaan-detail='" + data.id + "' data-toggle='modal' data-target='#detailInfo'><i class='fa fa-eye'></i> Lihat</a>";
          return_button += "<a class='btn btn-warning btn-xs' href='{{url()->current()}}/edit/" + data.id + "'><i class='fa fa-pencil'></i> Edit</a>";
          return_button += "<a class='btn btn-danger btn-xs' href='{{url()->current()}}/delete/" + data.id + "' onclick='return confirm(\"Apakah anda ingin menghapus data ini?\")'><i class='fa fa-close'></i> Hapus</a>";
          return return_button == "" ? "-" : return_button;
        }},
      ]
  });

  setTimeout(function() {
    $(".alert-success").hide(1000);
  }, 3000);

  $('#detailInfo').on('show.bs.modal', function(e) {
    var id_pertanyaan = $(e.relatedTarget).data('pertanyaan-detail');

    $.get("{{url()->current()}}/get_pertanyaan_by_id/" + id_pertanyaan, function(data){
      console.log(data);
      var pertanyaan = data[0].pertanyaan;
      var isi = '';

      isi += '<tr>';
      isi += '<th width="15%" class="text-center">Pilihan</th>';
      isi += '<th>Label</th>';
      isi += '<th width="10%" class="text-center">Poin</th>';
      isi += '</tr>';

      $.each(data , function (index, value){
        isi += '<tr>';
        isi += '<td class="text-center">'+ value.pilihan +'</td>';
        isi += '<td>'+ value.jawaban +'</td>';
        isi += '<td class="text-center">'+ value.bobot +'</td>';
        isi += '</tr>';
      });

      var close_button = "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>";

      $('#pertanyaan').append(pertanyaan);
      $('#tdetail').append(isi);
      $('.modal-footer').append(close_button);
    });

    $('#pertanyaan').empty();
    $('#tdetail').empty();
    $('.modal-footer').empty();
  });

});
</script>
<div class="modal fade" id="detailInfo" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div id="pertanyaan"></div><br>
        <table class="table table-bordered table-striped" id="tdetail">
        </table>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
@endsection
