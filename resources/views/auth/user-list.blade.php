@extends('layouts.app')

@section('content')

  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>User</h3>
      </div>
    </div>

    <div class="clearfix"></div>
    @if(Session::has('message'))
      <p class="alert alert-success">{{ Session::get('message') }}</p>
    @endif

    @if(Session::has('error'))
      <p class="alert alert-danger">{{ Session::get('error') }}</p>
    @endif
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>List User</h2>
            <div class="pull-right">
                <a class='btn btn-sm btn-success' href='/user/export_to_excel'><i class='fa fa-file-excel-o'></i> Download</a>
                <a class='btn btn-sm btn-success' href='/user/add'><i class='fa fa-plus'></i> Tambah</a>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table class="table table-bordered" id="users-table">
               <thead>
                   <tr>
                       <th>Nama</th>
                       <th>Email</th>
                       <th>Alamat</th>
                       <th>No. Hp</th>
                       <th>No. Ktp</th>
                       <th>Role</th>
                       <th style='width:100px'></th>
                   </tr>
               </thead>
           </table>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/user/datatables',
        columns: [
            { data: 'name', render: function ( data, type, row ) {
              if(row.login_from_fb ==1){
                return "<i class='fa fa-facebook'></i> " + data;
              } else {
                return data;
              }
            }},
            { data: 'email', name: 'email' },
            { data: 'alamat', name: 'alamat' },
            { data: 'no_hp', name: 'no_hp' },
            { data: 'no_ktp', name: 'no_ktp' },
            { data: 'roles', name: 'roles' },
            { data: null, render: function ( data, type, row ) {
                // Combine the first and last names into a single table field
                var return_button = "<a class='btn btn-warning btn-xs' href='/user/edit/" + data.id + "'><i class='fa fa-pencil'></i> </a> ";
                return_button += "<a class='btn btn-danger btn-xs' href='/user/delete/" + data.id + "'><i class='fa fa-close'></i> </a>";
                return return_button;
            }},
        ]
    });
});
</script>
@endsection
