@extends('layouts.app')

@section('content')

<div class="">
  <div class="page-title">
    <div class="title_left">
      <h3>Item</h3>
    </div>
  </div>

  <div class="clearfix"></div>
  @if ($errors->any())
      <div class="alert alert-danger">
          <ul class="list-unstyled">
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Form Item</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <form class="form-horizontal form-label-left" method = "POST">
            {{ csrf_field() }}
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                Username <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="username" name='username' value='{{ !is_null(old('username')) ? old('username') : (isset($data->username) ? $data->username : '') }}' required="required" class="form-control col-md-7 col-xs-12" type="text" {{ isset($data->id) ? 'disabled' : ''}}>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                Password @if(!isset($data->id))<span class="required">*</span>@endif
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="password" name='password' @if(!isset($data->id))required="required"@endif class="form-control col-md-7 col-xs-12" type="password">
                @if(isset($data->id))
                  <i>Biarkan kosong jika tidak ingin merubah password</i>
                @endif
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                Nama <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="name" name='name' value='{{ !is_null(old('name')) ? old('name') : (isset($data->name) ? $data->name : '') }}' required="required" class="form-control col-md-7 col-xs-12" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                Alamat <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea id="alamat" name='alamat' required="required" class="form-control col-md-7 col-xs-12">{{ !is_null(old('alamat')) ? old('alamat') : (isset($data->alamat) ? $data->alamat : '') }}</textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                No. HP <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="no_hp" name='no_hp' value='{{ !is_null(old('no_hp')) ? old('no_hp') : (isset($data->no_hp) ? $data->no_hp : '') }}' required="required" class="form-control col-md-7 col-xs-12" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                No. KTP <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="no_ktp" name='no_ktp' value='{{ !is_null(old('no_ktp')) ? old('no_ktp') : (isset($data->no_ktp) ? $data->no_ktp : '') }}' required="required" class="form-control col-md-7 col-xs-12" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                E-Mail <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="email" name='email' value='{{ !is_null(old('email')) ? old('email') : (isset($data->email) ? $data->email : '') }}' required="required" class="form-control col-md-7 col-xs-12" type="email">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                Roles <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select id="roles" name='roles' required='required' class='select2_group form-control'>
                  <option value='User' {{ isset($data->roles) && $data->roles == 'User' ? 'selected' : ''}}>User</<option>
                  <option value='Administrator' {{ isset($data->roles) && $data->roles == 'Administrator' ? 'selected' : ''}}>Administrator</<option>
                </select>
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <a href='/user/' class="btn btn-primary" type="button">Cancel</a>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
