
<li>
  <a href='/'>
    <i class="fa fa-dashboard"></i> Dashboard
  </a>
</li>
@php
use App\Menu;
$menu = Menu::where("is_deleted", 0)
->where("id_parent", 0)
->whereIn("id", getAllParentMenuUser())
->where("level", 1)->get();

global $menu_html;    
foreach($menu as $idx => $row){
  //check child
  $menu_html .= getChildMenu($row->id);
}

echo $menu_html;

function getChildMenu($menu_id) {
  $menu_child_html = "";
  $get_child = Menu::where("is_deleted", 0)->where("id_parent", $menu_id)->get();
  $parent_row = Menu::where("is_deleted", 0)->where("id", $menu_id)->first();
  
  if($get_child->count() > 0){
    if(can_access_child($parent_row->slug) === true){
      $menu_child_html .= "<li><a>". ($parent_row->level == 1 ? "<i class='fa fa-book'></i>" :""). $parent_row->nama. "<span class='fa fa-chevron-down'></span></a>
      <ul class='nav child_menu'>";
      foreach($get_child as $ix => $rw){

        if($rw->have_child == 1){
          $menu_child_html .= getChildMenu($rw->id);
        }
        else 
        {
          if(can_access($rw->slug)){
            $menu_child_html .= "<li><a href='{$rw->url}'> {$rw->nama}</a></li>";
          }
        }
      }
      $menu_child_html .= "</ul></li>";
    }
  } 
  else {
    $menu_child_html .= "<li><a href='{$parent_row->url}'>{$parent_row->nama}</a></li>";
  }

  return $menu_child_html;
}
@endphp

{{--
<li>
  <a href='/'>
    <i class="fa fa-dashboard"></i> Dashboard
  </a>
</li>
<li>
  <a href='/dashboard/tentang'>
    <i class="fa fa-info"></i> Tentang SKM
  </a>
</li>
<li>
  <a href='/dashboard/skema'>
    <i class="fa fa-info"></i> Skema SKM
  </a>
</li>
<!-- Contoh Can Access -->
@if(can_access("biaya") || can_access("setup-skema-akunting"))
<li><a><i class="fa fa-book"></i> Pengaturan<span class="fa fa-chevron-down"></span></a>
  <ul class="nav child_menu">
    @if(can_access("biaya"))<li><a href="/master/pengaturan_biaya"> Biaya</a></li>@endif
    @if(can_access("setup-skema-akunting"))<li><a href="/master/setup_skema_akunting"> Setup Skema Akunting</a></li>@endif
  </ul>
</li>
@endif

<li><a><i class="fa fa-book"></i> Survey<span class="fa fa-chevron-down"></span></a>
  <ul class="nav child_menu">
    <li><a href="/srv/pertanyaan">Buat Pertanyaan</a></li>
    <li><a href="/srv/grup_pertanyaan">Buat Survey</a></li>
    <li><a href="/srv/penerbitan">Penerbitan</a></li>
  </ul>
</li>

<li><a><i class="fa fa-book"></i> IKM<span class="fa fa-chevron-down"></span></a>
  <ul class="nav child_menu">
    <li><a href="/laporan/srv_tahunan">Per Tahun</a></li>
    <li><a href="/laporan/srv_bulanan">Per Bulan</a></li>
    <li><a href="/laporan/ikm">Per OPD</a></li>
    <li><a href="/laporan/ikm_tingkat_kota">IKM Tingkat Kota</a></li>
  </ul>
</li>

<li><a><i class="fa fa-cog"></i> Konfigurasi<span class="fa fa-chevron-down"></span></a>
  <ul class="nav child_menu">
    <li><a href="/konfigurasi/tentang">Tentang</a></li>
    <li><a href="/konfigurasi/skema">Skema</a></li>
    <!-- <li><a href="/konfigurasi/default_jawaban">Default Jawaban</a></li> -->
  </ul>
</li>

@if(Auth::user()->id_role == 1)
<li><a><i class="fa fa-book"></i> System<span class="fa fa-chevron-down"></span></a>
  <ul class="nav child_menu">
    <li><a href="/acl/role">Role</a></li>
    <li><a href="/acl/menu">Menu</a></li>
    <li><a href="/acl/permission">Permission</a></li>
    <li><a href="/acl/user">User</a></li>
  </ul>
</li>
@endif
--}}