@component('layouts.head')
@endcomponent

<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0; height:100px;">
              <a href="#" class="site_title" style="border: 0; height:100px; margin-top:20px">
                <span>
                  <!-- Logo Disini -->
                  <img src="{{ asset('img/logo.png') }}" alt="..." style="width:70%;">
                </span>
              </a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="{{ asset('img/user.png') }}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Selamat Datang,</span>
                <h2>
                  <?=Auth::user()->name;?>
                </h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Sistem App</h3>
                <ul class="nav side-menu">
                  @component('layouts.menu')
                  @endcomponent
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                @if(Auth::user()->level != "Anggota")
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <!-- <img src="images/img.jpg" alt=""> -->
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                          <i class="fa fa-sign-out pull-right"></i> Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                    <li>
                      <a href="{{ url('https://drive.google.com/file/d/1rnExA9Bo8JAq2PNcVSiOKTKgB-PY15XV/view?usp=sharing') }}" target="_blank">Download Buku Panduan <i class="fa fa-file-archive-o pull-right"></i></a>
                    </li>
                    <li>
                      <a href="{{ url('https://youtu.be/6WbcRHu_u1o') }}" target="_blank">Video Tutorial User <i class="fa fa-youtube-play pull-right"></i></a>
                    </li>
                    <li>
                      <a href="{{ url('https://youtu.be/FBfhNo6Q3nc') }}" target="_blank">Video Tutorial Admin <i class="fa fa-youtube-play pull-right"></i></a>
                    </li>
                  </ul>
                </li>
                @endif
                <?php
                use App\Http\Controllers\NotifikasiController;
                if(Auth::user()->level == "Anggota"){
                $badges = NotifikasiController::notifikasi_anggota_count();
                $data = NotifikasiController::notifikasi_anggota();
                ?>
                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-bell-o"></i>
                    <span class="badge bg-green">{{$badges > 0 ? $badges : ""}}</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    @if($data->count() > 0)
                    @foreach($data as $idx => $row)
                    <li>
                      <a href="/master/read_notifikasi_anggota/{{$row->id}}/{{$row->id_peminjaman}}">
                        <!-- <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span> -->
                        <span>
                          <span><strong>{{$row->is_read == 0 ? "Baru" : "Sudah dilihat"}}</strong></span>
                          <span class="time"><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($row->created_at))->diffForHumans(); ?></span>
                        </span>
                        <span class="message">
                          {!!$row->msg!!}
                        </span>
                      </a>
                    </li>
                    @endforeach
                    @else
                    <li>
                      <div class="text-center">
                        <strong>Tidak Ada Pemberitahuan</strong>
                      </div>
                    </li>
                    @endif
                    <!-- <li>
                      <div class="text-center">
                        <a>
                          <strong>Lihat Semua Pemberitahuan</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li> -->
                  </ul>
                </li>
              <?php } ?>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>

          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- Bootstrap -->
    <script src="{{ asset('/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('/vendors/fastclick/lib/fastclick.js') }}"></script>
    <!-- NProgress -->
    <script src="{{ asset('/vendors/nprogress/nprogress.js') }}"></script>
    <!-- jQuery custom content scroller -->
    <script src="{{ asset('/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>

    <!-- iCheck -->
    <script src="{{ asset('/vendors/iCheck/icheck.min.js') }}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('/vendors/moment/min/moment-with-locales.js') }}"></script>
    <script src="{{ asset('/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- Switchery -->
    <script src="{{ asset('/vendors/switchery/dist/switchery.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('/vendors/select2/dist/js/select2.min.js') }}"></script>
    <!-- autonumeric -->
    <script src="{{ asset('/vendors/auto-numeric/autoNumeric.js') }}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{ asset('/build/js/custom.js') }}"></script>

    <!-- Rupiah format -->
    <script>
    $(".rupiah-format").autoNumeric('init',{
    	aSep: '.',
      aDec: ',',
    	mDec: 0
    });
      $('select').select2({
        width:"100%"
      });

    $('input.icheck').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });

    function digit_only(cls){
      $("." + cls).keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
          return false;
        }
      });
    }

    digit_only("digit-only");

    $(".number-format").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    /*Digits Only*/
    function digits_only(){
      $(".digits_only").keypress(function(e){
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)){
          return false;
        }
      });
    }

    moment.locale('id'); // momentjs set locale (INDONESIA)
    </script>

    @yield('scripts')
    @stack('scripts')
  </body>
</html>
