@extends('layouts.app')

@section('content')
@if(Session::has('message'))
  <p class="alert alert-success">{!! Session::get('message') !!}</p>
@endif

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Skema SKM</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
      	<img src='{{url($data->skema)}}' class='img img-responsive'>
      </div>
  	</div>
  </div>
</div>

<script>

$(function() {
  setTimeout(function() {
    $(".alert-success").hide(1000);
  }, 5000);
});
</script>
@endsection
