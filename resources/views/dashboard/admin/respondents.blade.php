<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Table Responden</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-bordered table-striped display" id="respondent-table" style="width:100%">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Usia</th>
                        <th>Jenis Kelamin</th>
                        <th>Pekerjaan</th>
                        <th>NO Telp</th>
                        <th>Email</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Chart Responden</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-4">
                    <h4 style="text-align: center">Per Gender</h4>
                    <canvas id="gender-chart"></canvas>
                </div>
                <div class="col-md-4">
                    <h4 style="text-align: center">Per Tingkat Pendidikan</h4>
                    <canvas id="pendidikan-chart"></canvas>
                </div>
                <div class="col-md-4">
                    <h4 style="text-align: center">Per Umur</h4>
                    <canvas id="umur-chart"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <!-- DataTables -->
    <script src="{{ asset('/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script>

        $(function() {
            $('#respondent-table').DataTable({
                dom: 'Bfrtip',
                buttons: ['print'],
                processing: true,
                serverSide: true,
                ajax: '{{route('respondents.datatable')}}',
                columns: [
                    {data: 'nama', name: 'nama'},
                    {data: 'usia', name: 'usia', searchable: false},
                    {data: 'jk', render: function(data, type, row) {
                            return row.jk == 'L' ? 'Laki-Laki' : 'Perempuan'
                        }, searchable:false},
                    {data: 'id_pekerjaan', render: function (data, type, row){
                            switch(row.id_pekerjaan) {
                                case 1:
                                    return 'PNS';
                                case 2:
                                    return 'TNI';
                                case 3:
                                    return 'POLRI';
                                case 4:
                                    return 'SWASTA';
                                case 5:
                                    return 'WIRAUSAHA';
                                default:
                                    return 'LAINNYA';
                            }
                        }, searchable:false},
                    {data: 'no_tlp'},
                    {data: 'email'}
                ]
            });

            const chartColors = {
                red: 'rgb(255, 99, 132)',
                orange: 'rgb(255, 159, 64)',
                yellow: 'rgb(255, 205, 86)',
                green: 'rgb(75, 192, 192)',
                blue: 'rgb(54, 162, 235)',
                purple: 'rgb(153, 102, 255)',
            };

            const availableColor = ['red', 'orange', 'yellow', 'green', 'blue', 'purple', 'grey']

            const getRandomColor = function() {
                const color = chartColors[availableColor[Math.floor(Math.random() * availableColor.length)]];
                console.log(color);
                return color;
            };

            const config = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [
                            {{$respondentChart['gender']['male'] ?? 0}},
                            {{$respondentChart['gender']['female'] ?? 0}},
                        ],
                        backgroundColor: [
                            chartColors.yellow,
                            chartColors.green,
                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        'Pria',
                        'Wanita',
                    ]
                },
                options: {
                    responsive: true
                }
            };

            const ctx = document.getElementById('gender-chart').getContext('2d');
            new Chart(ctx, config);

            const configPendidikan = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [
                            @foreach($respondentChart['pendidikan'] as $value)

                            {{$value['total']}},

                            @endforeach
                        ],
                        backgroundColor: [
                            @foreach($respondentChart['pendidikan'] as $value)

                            getRandomColor(),

                            @endforeach
                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        @foreach($respondentChart['pendidikan'] as $value)

                        '{{$value['label']}}',

                        @endforeach
                    ]
                },
                options: {
                    responsive: true
                }
            };

            const chartPendidikan = document.getElementById('pendidikan-chart').getContext('2d');
            new Chart(chartPendidikan, configPendidikan);

            const configUmur = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [
                            @foreach($respondentChart['umur'] as $value)

                            {{$value['total']}},

                            @endforeach
                        ],
                        backgroundColor: [
                            @foreach($respondentChart['umur'] as $value)

                            getRandomColor(),

                            @endforeach
                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        @foreach($respondentChart['umur'] as $value)

                            'usia {{$value['usia']}}',

                        @endforeach
                    ]
                },
                options: {
                    responsive: true
                }
            };

            const chartUmur = document.getElementById('umur-chart').getContext('2d');
            new Chart(chartUmur, configUmur)
        });
    </script>
@endpush
