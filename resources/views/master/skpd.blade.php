@extends('layouts.app')

@section('content')

<style>
    .head {
        display: flex;
        justify-content:space-between;
    }
</style>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="head">
                <h4>Data SKPD</h4>
                <button class="btn btn-success" data-toggle="modal" data-target="#addSkpd">Tambah +</button>
            </div>
            <hr style="border: 1px solid #eee">
            @if(Session::has('successAdd'))
            <div class="alert alert-success" id="alert" role="alert">
                Data berhasil di tambahkan
            </div>
            @elseif(Session::has('successDel'))
            <div class="alert alert-danger" id="alert" role="alert">
                Data berhasil di hapus
            </div>
            @elseif(Session::has('successUpdt'))
            <div class="alert alert-warning" id="alert" role="alert">
                Data berhasil di edit
            </div>
            @endif
            <table class="table table-bordered table-striped" id="tableSkpd">
                <thead>
                    <tr>
                        <th>Nama SKPD</th>
                        <th width="15%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal add -->
<div class="modal fade" id="addSkpd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form method="post" action="{{url('')}}/master/skpd/add" id="addSkpd">
        {{ csrf_field() }}
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel">Tambah SKPD</h4>
        </div>
        <div class="modal-body">
            <div class="row justify-content-center align-items-center">
                <div class="col-sm-10">
                    <div class="col-sm-3">
                        <label for="skpd">Nama SKPD* &nbsp; : </label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" name="skpd" id="skpd" class="form-control" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </div>
    </form>
  </div>
</div>

<script>
    $(document).ready(() => {
        $('#tableSkpd').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('')}}/master/skpd/getSkpd",
            columns: [
                {data: 'name', name: 'name'},
                {
                    data: 'aksi', 
                    name: 'aksi',
                    orderable: true,
                    searchable: true
                }
            ]
        })

        setTimeout(() => {
            $('#alert').slideUp(300);
        }, 2000)

    })
</script>

@endsection