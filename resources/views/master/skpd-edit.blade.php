@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <h4>Form edit SKPD</h4>
            <hr style="border: 1px solid #ddd;">
            <form method="POST" id="editSkpd">
                {{ csrf_field() }}
                @foreach ($data as $val)
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="skpd">Nama SKPD* &nbsp; : </label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" name="skpd" id="skpd" value="{{$val->name}}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                                <a href="{{url('')}}/master/skpd" class="btn btn-secondary btn-sm">Batal</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(() => {
        var id_skpd = `{{$data[0]->id}}`
        $('#editSkpd').submit(() => {
            $('#editSkpd').attr('action', '{{url("")}}/master/skpd/update/' + id_skpd);
        })
    })
</script>

@endsection