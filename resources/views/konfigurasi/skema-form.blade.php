@extends('layouts.app')

@section('content')

<div class="page-title">
  <div class="title_left">
    <h3>Tentang SKM</h3>
  </div>
</div>
<div class="clearfix"></div>
@if(Session::has('message'))
  <p class="alert alert-success">{!! Session::get('message') !!}</p>
@endif
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Skema SKM</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form class="form-horizontal" method="POST" enctype='multipart/form-data'>
          {{ csrf_field() }}
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">
              Image <span class="required">*</span> :
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input name='image' required="required" class="form-control col-md-7 col-xs-12" type="file">
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){

});
</script>
@endsection
