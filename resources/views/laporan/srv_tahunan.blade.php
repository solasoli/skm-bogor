@extends('layouts.app')

@section('content')
<style>
i.fa {
   font-size: 5em;
}
</style>
<div class="page-title">
  <div class="title_left">
    <h3>Laporan Survey Tahunan</h3>
  </div>
</div>
<div class="clearfix"></div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
                <li>&#8226; {{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session::has('message'))
  <p class="alert alert-success">{!! Session::get('message') !!}</p>
@endif
<form class="form-horizontal" method="POST">
  {{ csrf_field() }}
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Filter</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="form-group {{ must_show_skpd_form() ? '' : 'hide'}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            OPD <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='skpd' class="form-control select2" id="skpd">
              @foreach($skpd as $id => $row)

                @php
                  $selected = !is_null(old('skpd')) ? old('skpd') : (isset($request['skpd']) ? $request['skpd'] : '');
                  $selected = $selected == $row->id ? "selected" : "";
                @endphp

                <option value="{{$row->id}}" {{$selected}}>{{$row->name}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Tahun <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='tahun' class="form-control select2" id="tahun">
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Judul Survey <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control select2" name="survey" id="survey">
              <option value="">- Pilih OPD terlebih dahulu -</option>
            </select>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <!-- <a href='{{url('')}}/srv/pertanyaan' class="btn btn-primary" type="button">Cancel</a> -->
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
</form>

@if(isset($request['survey']))
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Pengisian survey</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        Jumlah Responden Survey <b>{{$selected_survey->nama}}</b> Tahun <b>{{$request['tahun']}}</b> adalah <b>{{$total_survey_tahunan}}</b><br>
        Rincinannya sebagai berikut :
        <canvas id="jml_srv_per_bulan" width="400" height="100"></canvas>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-5 col-sm-5 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Indeks Kepuasan Masyarakat</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        Indeks Kepuasan Masyarakan untuk survey <b>{{$selected_survey->nama}}</b> Tahun <b>{{$request['tahun']}}</b> adalah :<br><br>
        <div class="text-center">
          <?php
          if(strpos($penilaian['show_bintang'], '.') == true){
            $penilaian_x = explode('.', $penilaian['show_bintang']);
            for($i=1; $i<=$penilaian_x[0]; $i++){
              echo "<i class='fa fa-star' style='color:#ffac28;'></i>";
            }
            echo "<i class='fa fa-star-half-full' style='color:#ffac28;'></i>";

            for($i=1; $i<=3-$penilaian_x[0]; $i++){
              echo "<i class='fa fa-star-o' style='color:#ffac28;'></i>";
            }
          } else {
            if($penilaian['show_bintang'] > 0){
              for($i=1; $i<=$penilaian['show_bintang']; $i++){
                echo "<i class='fa fa-star' style='color:#ffac28;'></i>";
              }
              for($i=1; $i<=4-$penilaian['show_bintang']; $i++){
                echo "<i class='fa fa-star-o' style='color:#ffac28;'></i>";
              }
            } else {
              for($i=1; $i<=4; $i++){
                echo "<i class='fa fa-star-o' style='color:#ffac28;'></i>";
              }
            }
          }
          ?>
          <br>
          <h3>{{$penilaian['nilai_interval_konversi'] * 25 }}</h3>
          @php
              if($penilaian['nilai_interval_konversi'] >= 0 && $penilaian['nilai_interval_konversi'] <= 1.75 && $penilaian['nilai_interval_konversi'] != null)
                $nilai_interval_konversi_status = 'Tidak Memuaskan';
            elseIf($penilaian['nilai_interval_konversi'] > 1.75 && $penilaian['nilai_interval_konversi'] <= 2.50)
                $nilai_interval_konversi_status = 'Kurang Memuaskan';
            elseIf($penilaian['nilai_interval_konversi'] > 2.50 && $penilaian['nilai_interval_konversi'] <= 3.25)
                $nilai_interval_konversi_status = 'Memuaskan';
            elseIf($penilaian['nilai_interval_konversi'] > 3.25 && $penilaian['nilai_interval_konversi'] <= 4.00)
                $nilai_interval_konversi_status = 'Sangat Memuaskan';
            elseif($penilaian['nilai_interval_konversi'] == null)
                $nilai_interval_konversi_status = 'Tidak Ada Data';
            else
                $nilai_interval_konversi_status = '';
          @endphp
              <h3>{{$nilai_interval_konversi_status}}</h3>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-7 col-sm-7 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Total Score per Unsur Pertanyaan</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        Total Score per Unsur Pertanyaan dari survey <b>{{$selected_survey->nama}}</b> Tahun <b>{{$request['tahun']}}</b><br>
        adalah sebagai berikut :
        <canvas id="score_per_unsur"></canvas>
      </div>
    </div>
  </div>
</div>


<script src="{{ asset('/vendors/Chart.js/dist/Chart.js') }}"></script>
<script src="{{ asset('/vendors/Chart.js/dist/Chart.min.js') }}"></script>
<script>
$(function(){
  var jml_srv_per_bulan = document.getElementById('jml_srv_per_bulan').getContext('2d');
  var chart_1 = new Chart(jml_srv_per_bulan, {
      type: 'bar',
      data: {
          labels: [
            @for($i=1; $i<=12; $i++)
            '{{bulan_indonesia($i)}}',
            @endfor
          ],
          datasets: [{
            label: 'Nilai IKM : {{array_sum($hit_survey_bulanan) > 0 ? array_sum($hit_survey_bulanan) / $jumlah_available_bulan : 0 }}',
            data: [
              @for($i=1; $i<=12; $i++)
              {{$hit_survey_bulanan[$i]}},
              @endfor
            ],
            backgroundColor: [
              @for($i=1; $i<=12; $i++)
              'rgba(54, 162, 235, 0.2)',
              @endfor
            ],
            borderColor: [
              @for($i=1; $i<=12; $i++)
              'rgba(54, 162, 235, 1)',
              @endfor
            ],
            borderWidth: 1
          }]
      },
      options : {
       scales: {
           yAxes: [{
               ticks: {
                  max: 4,
                   beginAtZero: true,
                   userCallback: function(label, index, labels) {
                       // when the floored value is the same as the value we have a whole number
                       if (Math.floor(label) === label) {
                           return label;
                       }

                   },
               }
           }],
       },
   }
  });

  var score_per_unsur = document.getElementById('score_per_unsur').getContext('2d');
  var chart_2 = new Chart(score_per_unsur, {
      type: 'horizontalBar',
      data: {
          labels: [
            @foreach($total_bobot_per_unsur AS $row)
            '{{$row->unsur}}',
            @endforeach
          ],
          datasets: [{
            label: 'Total Score : {{ $total_bobot_per_unsur->sum('score') > 0 ? number_format($total_bobot_per_unsur->sum('score') / $total_bobot_per_unsur->count(), 2) : 0}}',
            data: [
              @foreach($total_bobot_per_unsur AS $row)
              '{{$row->score}}',
              @endforeach
            ],
            backgroundColor: [
              @foreach($total_bobot_per_unsur AS $row)
              'rgba(54, 162, 235, 0.2)',
              @endforeach
            ],
            borderColor: [
              @foreach($total_bobot_per_unsur AS $row)
              'rgba(54, 162, 235, 1)',
              @endforeach
            ],
            borderWidth: 1
          }]
      },
      options : {
       scales: {
           xAxes: [{
               ticks: {
                   beginAtZero: true,
                   userCallback: function(label, index, labels) {
                       // when the floored value is the same as the value we have a whole number
                       if (Math.floor(label) === label) {
                           return label;
                       }

                   },
               }
           }],
           yAxes: [{
               ticks: {
                  max: 4,
                   beginAtZero: true
               }
           }],
       },
   }
  });
});
</script>
@endif
<script>
$(function(){

  function get_survey(){
    $.post("/srv/grup_pertanyaan/get_grup_by_id_skpd", {"skpd" : $("#skpd").val(), "tahun": $("#tahun").val()},
    function(res){
      if(res.data != null){
        var option = '';
        var request = '{{isset($request["survey"]) ? $request["survey"] : ""}}';

        $.each(res.data, function(idx, val){
          if(request != '' && request == val.id){
            var selected = 'selected';
          } else {
            var selected = '';
          }
          option += '<option value="'+ val.id +'" '+selected+'>'+ val.nama +'</option>';
          $(".bulan-cover").show();
        });
        $("#survey").html(option);
      }
    });

  }


  get_tahun();
  function get_tahun(){
    $(".bulan-cover").hide();
    $.get('{{url("")}}/laporan/srv_bulanan/get_tahun_select2/' + $('#skpd').val(), function(res){
      console.log(res);
      if(res != null){
        var option = '';
        var request = '{{isset($request["tahun"]) ? $request["tahun"] : ""}}';

        $.each(res, function(idx, val){
          if(request != '' && request == val.id){
            var selected = 'selected';
          } else {
            var selected = '';
          }
          option += '<option value="'+ val.tahun +'" '+ selected +'>'+ val.tahun +'</option>';
        });
        $("#tahun").html(option);
      }
    }).done(function(){
      get_survey();
    });
  }

  $("#skpd").on("change", function(){
    get_tahun();
  });

  $("#tahun").on("change", function(){
    get_survey();
  });


});
</script>
@endsection
