@extends('layouts.app')

@section('content')
    <div class="page-title">
        <div class="text-center">
            <h3>Unsur Pelayanan Survey Pertahun</h3>
            
        </div>
    </div>

    <div class="clearfix"></div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="list-unstyled">
                @foreach ($errors->all() as $error)
                    <li>&#8226; {{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <p class="alert alert-success">{!! Session::get('message') !!}</p>
    @endif

    <form class="form-horizontal" method="GET" style="margin-top: 50px">
        @include('laporan.partials.opd_options', [
          'title' => 'Filter',
          'requireSurvey' => true,
          'showPrintButton' => true
        ])
    </form>

    @if($selectedSurvey)
        <div class="container x_panel" id="section-to-print">
            <div class="text-center">
            <h4>Nilai Rata-rata Pelayanan Survey Per Tahun</h4>
            <h4>{{ $selectedSkpdData->name }}</h4>
            <h4>{{ $selectedSurveyData->nama }}</h4>
            <h4>Tahun {{ $selectedYear }}</h4>
            </div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <td rowspan="2">No</td>
                    <td rowspan="2">Unsur Layanan</td>
                    <td colspan="12">Nilai Unsur Pelayanan</td>
                    <td rowspan="2">Rata-rata terbilang</td>
                    <td rowspan="2">Kinerja Unit Pelayanan</td>
                </tr>
                <tr>
                    <td>Jan</td>
                    <td>Feb</td>
                    <td>Mar</td>
                    <td>Apr</td>
                    <td>May</td>
                    <td>Jun</td>
                    <td>Jul</td>
                    <td>Aug</td>
                    <td>Sep</td>
                    <td>Oct</td>
                    <td>Nov</td>
                    <td>Des</td>
                </tr>
                </thead>
                <tbody>
                @foreach($unsurPertanyaan as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$value->nama}}</td>
                        @foreach($monthlyUnsurPertanyaan as $monthlyValue)
                            @php 
                            $valueMonthly = $monthlyValue->data->where('unsurPertanyaan.id', $value->id)->first();
                            @endphp
                            <td>{{ !empty(trim($valueMonthly->value)) ? $valueMonthly->value : '-'}}</td>
                        @endforeach
                        <td>Rata-Rata</td>
                        <td>{{$value->kinerja_unit}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="2">Nilai Survey</td>
                    @foreach($monthlyUnsurPertanyaan as $value)
                        <td>{{ round($value->nilaiSurvey / (count($unsurPertanyaan) - 1), 2) }}</td>
                    @endforeach
                    <td> </td>
                    <td> </td>
                </tr>
                <tr>
                    <td colspan="2">Nilai SKM</td>
                    @foreach($monthlyUnsurPertanyaan as $value)
                        <td>{{ round(($value->nilaiSurvey / (count($unsurPertanyaan) - 1)) * 25, 2) }}</td>
                    @endforeach
                    <td> </td>
                    <td> </td>
                </tr>
                <tr>
                    <td colspan="2">Mutu Pelayanan</td>
                    @foreach($monthlyUnsurPertanyaan as $value)
                        <td>{{$value->mutuPelayanan}}</td>
                    @endforeach
                    <td> </td>
                    <td> </td>
                </tr>
                <tr>
                    <td colspan="2">Kinerja Unit Pelayanan</td>
                    @foreach($monthlyUnsurPertanyaan as $value)
                        <td>{{$value->kinerjaUnit}}</td>
                    @endforeach
                    <td> </td>
                    <td> </td>
                </tr>
                </tbody>
            </table>
        </div>
    @endif
@endsection

@push('css')
    <style type="text/css">
        @media print {
            body * {
                visibility: hidden;
            }
            #section-to-print, #section-to-print * {
                visibility: visible;
            }
            #section-to-print {
                position: absolute;
                left: 0;
                top: 0;
            }
        }
    </style>
@endpush
