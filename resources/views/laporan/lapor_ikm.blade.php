@extends('layouts.app')
@section('content')

<style>
i.fa {
   font-size: 5em;
}
.all-opd input {
  height: 15px;
  width: 15px;
  margin-top: 15px;
}
</style>
<div class="page-title">
  <div class="title_left">
    <h3>Laporan IKM</h3>
  </div>
</div>
<div class="clearfix"></div>

<div class="cover-content">
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Filter</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
      <form class="form-horizontal" id="form-laporIKM" method="POST">
        @csrf
        <div class="form-group {{ must_show_skpd_form() ? '' : 'hide'}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            OPD <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='skpd' class="form-control select2" id="skpd">
                @foreach($opd AS $idx => $value)
                  <option value="{{ $value->id }}">{{ $value->name }}</option>
                @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Tahun <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='tahun' class="form-control select2" id="tahun">
                
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Survey <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control select2" name="survey" id="survey">
              <option value="">- Pilih OPD terlebih dahulu -</option>
            </select>
            <div class="all-opd">
              <input type="checkbox" value="0" id="seluruh_survey" name="seluruh-survey">
              <label for="seluruh_survey"> Seluruh Survey</label>
            </div>
          </div>
        </div>

        <div class="form-group bulan-cover">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Periode <span class="required">*</span> :
          </label>
          
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                <div class="col-sm-5">
                    <input type="date" id="from" class="form-control" required="on">
                </div>
                <div class="col-sm-1">
                    <h5>S/d</h5>
                </div>
                <div class="col-sm-6">
                    <input type="date" id="to" class="form-control" required="on">
                </div>
            </div>
           </div>
        </div>


        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            
             <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>

<style>
    .display-lapor-ikm {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    @media print {
      .body * {
        visibility: hidden;
      }
      #print-content, #print-content * {
        visibility: visible;
      }
      #print-content {
        position: absolute;
        left: 0;
        top: -55vh;
      }
      .both {
        display: flex;
        justify-content: space-between;
      }
      .nilaiIKM, .nilaiSurvey {
        width: 50%;
      }
      .mutu-layanan {
        font-size: 16px;
      }
      .pernyataan {
        margin-top: 40px;
        font-size: 20px;

      }
    }
</style>

<div class="row row-laporIKM" hidden style="font-family: sans-serif">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title display-lapor-ikm">
        <h2>Laporan IKM</h2>
        <a class="btn btn-info" onclick="window.print()">Print <i class="fa fa-print" style="font-size: 16px;"></i></a>
      </div>
      <div class="x_content"> 
        <div class="col-md-12" id="print-content">
            <h5 class="heading text-center">INDEKS KEPUASAN MASYARAKAT (IKM)</h5> 
            <h5 class="heading text-center head-opd"></h5>
            <h5 class="heading text-center">Kota Bogor</h5>
            <h5 class="heading text-center head-tahun"></h5>

            <hr style="border: 0.5px solid #ddd;">

            <div class="row both">
                <div class="col-sm-6">
                  <div style="border: 1px solid #ddd;">
                    <h3 class="text-center" style="border-bottom: 1px solid #ddd;padding: 1px; margin-top: 5px; height: 90px; padding-top:15px; line-height: 45px;">NILAI IKM</h3>
                    <h3 class="text-center nilaiIKM" style="font-size:9vw; font-weight:800; height: 380px; padding: 1px; color:#999; margin-top:-11px; padding-top:120px"></h3>
                    <h3 class="text-center mutu-layanan" style="padding-bottom: 30px;margin-top: -60px;"></h3>
                  </div>
                </div>
                <div class="col-sm-6 nilaiSurvey">
                  <div style="border: 1px solid #ddd; padding: 10px">
                    <div class="row" style="border-bottom: 1px solid #ddd;padding: 10px;">
                      <div class="col-sm-12 layanan"></div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4" style="width:34%">
                          <h4>Jumlah</h4>
                      </div>
                      <div class="col-sm-2" style="width:10%">
                          <h4>:</h4>
                      </div>
                      <div class="col-sm-6" style="width:56%">
                          <h4 class="jml_data_survey"></h4>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-4" style="width:33%">
                          <h4>Jenis Kelamin (L/P)</h4>
                      </div>
                      <div class="col-sm-2" style="width:16%">
                          <h4>:</h4>
                      </div>
                      <div class="col-sm-2" style="width:18%">
                          <h4>L</h4>
                      </div>
                      <div class="col-sm-2" style="width:13%">
                          <h4>:</h4>
                      </div>
                      <div class="col-sm-2" style="width:20%">
                          <h4 class="jk"></h4>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-6" style="width:49%"></div>
                      <div class="col-sm-2" style="width:18%">
                          <h4>P</h4>
                      </div>
                      <div class="col-sm-2" style="width:13%">
                          <h4>:</h4>
                      </div>
                      <div class="col-sm-2" style="width:20%">
                          <h4 class="perempuan"></h4>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-4" style="width:33%">
                          <h4>Pendidikan</h4>
                      </div>
                      <div class="col-sm-2" style="width:16%">
                          <h4>:</h4>
                      </div>
                      <div class="col-sm-2" style="width:18%">  
                          <h4>SD</h4>
                      </div>
                      <div class="col-sm-2" style="width:13%">
                          <h4>:</h4>
                      </div>
                      <div class="col-sm-2" style="width:20%">
                          <h4 class="sd"></h4>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-6" style="width:49%"></div>
                      <div class="col-sm-2" style="width:18%">
                          <h4>SMP</h4>
                      </div>
                      <div class="col-sm-2" style="width:13%">
                          <h4>:</h4>
                      </div>
                      <div class="col-sm-2" style="width:20%">
                          <h4 class="smp"></h4>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-6" style="width:49%"></div>
                      <div class="col-sm-2" style="width:18%">
                          <h4>SMA</h4>
                      </div>
                      <div class="col-sm-2" style="width:13%">
                          <h4>:</h4>
                      </div>
                      <div class="col-sm-2" style="width:20%">
                          <h4 class="sma"></h4>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-6" style="width:49%"></div>
                      <div class="col-sm-2" style="width:18%">
                          <h4>DIII</h4>
                      </div>
                      <div class="col-sm-2" style="width:13%">
                          <h4>:</h4>
                      </div>
                      <div class="col-sm-2" style="width:20%">
                          <h4 class="d3"></h4>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-6" style="width:49%"></div>
                      <div class="col-sm-2" style="width:18%">
                          <h4>S1</h4>
                      </div>
                      <div class="col-sm-2" style="width:13%">
                          <h4>:</h4>
                      </div>
                      <div class="col-sm-2" style="width:20%">
                          <h4 class="s1"></h4>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-6" style="width:49%"></div>
                      <div class="col-sm-2" style="width:18%">
                          <h4>S2</h4>
                      </div>
                      <div class="col-sm-2" style="width:13%">
                          <h4>:</h4>
                      </div>
                      <div class="col-sm-2" style="width:20%">
                          <h4 class="s2"></h4>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-4">
                          <h4>Periode Survey</h4>
                      </div>
                      <div class="col-sm-2">
                          <h4>:</h4>
                      </div>
                      <div class="col-sm-6">
                        <h4 class="periode"></h4>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          <div class="row">
            <div class="col-sm-12">
              <h4 class="text-center pernyataan">Terimakasih atas penilaian yang telah anda berikan masukan anda sangat bermanfaat untuk kemajuan unit kami agar terus memperbaiki dan meningkatkan kualitas pelayanan bagi masyarakat</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<script>
  $(function() {
    function get_survey(){
      $.post("/srv/grup_pertanyaan/get_grup_by_id_skpd", {"skpd" : $("#skpd").val(), "tahun": $("#tahun").val()},
      function(res){
        if(res.data != null){
          var option = '';
          var request = '{{isset($request["survey"]) ? $request["survey"] : ""}}';

          $.each(res.data, function(idx, val){
            if(request != '' && request == val.id){
              var selected = 'selected';
            } else {
              var selected = '';
            }
            option += '<option value="'+ val.id +'" '+selected+'>'+ val.nama +'</option>';
          });
          $("#survey").html(option);
        }
      });

    }

    get_tahun();
    function get_tahun(){
      $.get('{{url("")}}/laporan/lapor_ikm/get_tahun/' + $('#skpd').val(), function(res){
        if(res != null){
          var option = '';
          var request = '{{isset($request["tahun"]) ? $request["tahun"] : ""}}';
          
          $.each(res, function(idx, val){
            if(request != '' && request == val.id){
              var selected = 'selected';
            } else {
              var selected = '';
            }
            option += '<option value="'+ val.tahun +'" '+ selected +'>'+ val.tahun +'</option>';
          });
          $("#tahun").html(option);
        }
      }).done(function(){
        get_survey();
      });
    }


    $("#skpd").on("change", function(){
      get_tahun();
    });

    $("#tahun").on("change", function(){
      get_survey();
    });

    // check seluruh survey
    $('#seluruh_survey').click((e) => {
      if(e.currentTarget.checked == true) {
        $('#survey').attr('disabled', 'on');
        $('#seluruh_survey').attr('value', 1);
      }else {
        $('#survey').removeAttr('disabled');
        $('#seluruh_survey').attr('value', 0);
      }
    })

    // ketika form disubmit
    $('#form-laporIKM').submit(function(e) {

      // jika seluruh survey tidak dipilih
      if($('#seluruh_survey').val() == 0) {
        e.preventDefault();
        $('.periode').html(from + ' s/d ' + to);

        function get_data_survey() {

          var from = $('#from').val();
          var to = $('#to').val();
          
          // from
          from = from.split("-");
          yearFrom = from[0];
          monthFrom = from[1];
          dayFrom = from[2];

          // to
          to = to.split("-");
          yearTo = to[0];
          monthTo = to[1];
          dayTo = to[2];

          var dari = dayFrom + '-' + monthFrom + '-' + yearFrom;
          var sampai = dayTo + '-' + monthTo + '-' + yearTo;
          $('.periode').html(dari + ' s/d ' + sampai);

          // get json data survey
          $.get('{{url("")}}/laporan/lapor_ikm/get_data_survey/' + $('#survey').val() + '/' + $('#tahun').val() + '/' + $('#skpd').val(), function(res) {
            if(res != null) {
              
              if(nilaiIKM >= 25 && nilaiIKM <= 43.75 && nilaiIKM != null) {
                var status = 'Tidak Memuaskan';
              }else if(nilaiIKM >= 43.76 && nilaiIKM <= 62.50) {
                var status = 'Kurang Memuaskan';
              }else if(nilaiIKM >= 62.51 && nilaiIKM <=81.25) {
                var status = 'Memuaskan';
              }else if(nilaiIKM >= 81.26 && nilaiIKM <= 100) {
                var status = 'Sangat Memuaskan';
              }else if(nilaiIKM == null) {
                var status = 'Tidak Ada Data';
              }else {
                var status = '';
              }

              var nilaiIKM = res[1].nilai_interval_konversi * 25;
              // cek apakah ada nilai
              if(nilaiIKM > 0) {
                $('.nilaiIKM').html(nilaiIKM.toFixed(2));
                $('.mutu-layanan').html('Mutu Layanan : ' + status);
              }else if(nilaiIKM == 0) {
                $('.nilaiIKM').html("0");
                $('.mutu-layanan').html('Tidak ada nilai');
              }



              $.each(res[0], function(idx, val) {
                $('.jml_data_survey').html(val.jumlah_survey + ' Orang');
                // jenis kelamin
                $('.jk').html(val.Laki_laki);
                $('.perempuan').html(val.Perempuan);
                
                // pendidikan
                $('.sd').html(val.sd);
                $('.smp').html(val.smp);
                $('.sma').html(val.sma);
                $('.d3').html(val.d3);
                $('.s1').html(val.s1);
                $('.s2').html(val.s2);
              })
            }
          })
        }
        get_data_survey();

        $('.row-laporIKM').removeAttr('hidden');
        $('#form-laporIKM').attr('action', '{{ url("laporan/lapor_ikm") }}');
      
        $('.head-tahun').html($('#tahun').find('option:selected').html());
        $('.head-opd').html($('#skpd').find('option:selected').html());
      
        $('.layanan').html(`<h4 class="text-center">${ $('#survey').find('option:selected').html() }</h4>`);
        
      // jika seluruh survey dipilih
      }else if($('#seluruh_survey').val() == 1) {
        e.preventDefault();
        $('.periode').html(from + ' s/d ' + to);

        function get_data_seluruh_survey() {

          var from = $('#from').val();
          var to = $('#to').val();
          
          // from
          from = from.split("-");
          yearFrom = from[0];
          monthFrom = from[1];
          dayFrom = from[2];

          // to
          to = to.split("-");
          yearTo = to[0];
          monthTo = to[1];
          dayTo = to[2];

          var dari = dayFrom + '-' + monthFrom + '-' + yearFrom;
          var sampai = dayTo + '-' + monthTo + '-' + yearTo;
          $('.periode').html(dari + ' s/d ' + sampai);
        $.get('{{url("")}}/laporan/lapor_ikm/get_data_seluruh_survey/' + $('#tahun').val() + '/' + $('#skpd').val(), function(res) {
          $.each(res, (idx, val) => {
            if(res != null) {
              var nilaiIKM = res[1].nilai_interval_konversi * 25;
              $('.nilaiIKM').html(nilaiIKM.toFixed(2));

              if(nilaiIKM >= 25 && nilaiIKM <= 43.75 && nilaiIKM != null) {
                var status = 'Tidak Memuaskan';
              }else if(nilaiIKM >= 43.76 && nilaiIKM <= 62.50) {
                var status = 'Kurang Memuaskan';
              }else if(nilaiIKM >= 62.51 && nilaiIKM <=81.25) {
                var status = 'Memuaskan';
              }else if(nilaiIKM >= 81.26 && nilaiIKM <= 100) {
                var status = 'Sangat Memuaskan';
              }else if(nilaiIKM == null) {
                var status = 'Tidak Ada Data';
              }else {
                var status = '';
              }

              $('.mutu-layanan').html('Mutu Layanan : ' + status);
              $.each(res[0], function(idx, val) {
                console.log(val);
                $('.jml_data_survey').html(val.jumlah_survey + ' Orang');
                // jenis kelamin
                $('.jk').html(val.Laki_laki);
                $('.perempuan').html(val.Perempuan);
                
                // pendidikan
                $('.sd').html(val.sd);
                $('.smp').html(val.smp);
                $('.sma').html(val.sma);
                $('.d3').html(val.d3);
                $('.s1').html(val.s1);
                $('.s2').html(val.s2);
              })
            }
          })
        })
      }
      get_data_seluruh_survey();

      $('.row-laporIKM').removeAttr('hidden');
      $('#form-laporIKM').attr('action', '{{ url("laporan/lapor_ikm") }}');
    
      $('.head-tahun').html($('#tahun').find('option:selected').html());
      $('.head-opd').html($('#skpd').find('option:selected').html());
    
      $('.layanan').html(`<h4 class="text-center">${ $('#survey').find('option:selected').html() }</h4>`);
    }
  })
        
})
  
</script>

@endsection