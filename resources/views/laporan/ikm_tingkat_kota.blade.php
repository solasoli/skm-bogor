@extends('layouts.app')

@section('content')
<style>
i.fa {
   font-size: 5em;
}
</style>
<div class="page-title">
  <div class="title_left">
    <h3>Laporan IKM Tingkat Kota</h3>
  </div>
</div>
<div class="clearfix"></div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
                <li>&#8226; {{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session::has('message'))
  <p class="alert alert-success">{!! Session::get('message') !!}</p>
@endif


<form class="form-horizontal" method="GET">
    @include('laporan.partials.admin.ikm_per_city_yearly_options', [
      'availableYears' => $option_tahun,
      'selectedYear' => $request['tahun'] ?? null,
      'title' => 'Filter',
      'fieldName' => 'tahun'
    ])
</form>

@if(isset($request['tahun']))
    @include('laporan.partials.ikm_per_city_yearly', [
      'selectedYear' => $request['tahun'],
      'ikmPerCityYearly' => $ikmPerCityYearly
    ])
@endif
<script>
$(function(){


});
</script>
@endsection
