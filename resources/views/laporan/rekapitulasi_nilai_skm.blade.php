@extends('layouts.app')

@section('content')
    <div class="page-title">
        <div class="text-center">
            <h3>Rekapitulasi Nilai Survey Kepuasan Masyarakat</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="list-unstyled">
                @foreach ($errors->all() as $error)
                    <li>&#8226; {{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <p class="alert alert-success">{!! Session::get('message') !!}</p>
    @endif

    <form class="form-horizontal" method="GET" style="margin-top: 50px">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Filter</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                Tahun <span class="required">*</span> :
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name='tahun' class="form-control select2" id="tahun_per_opd">
                                    @for($i = 2018 ; $i <= now()->year ; $i++)
                                        <option  {{$selectedYear == $i ? 'selected': null}} value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <div class="btn btn-warning" onclick="window.print()">Print</div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>

    @if($selectedYear)
        <div class="container x_panel" id="section-to-print">
            <div class="text-center">
                <h3>Rekapitulasi Nilai Survey Kepuasan Masyarakat Per OPD</h3>
            </div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <td rowspan="2">No</td>
                    <td rowspan="2">Perangkat Daerah</td>
                    <td rowspan="2">Nilai SKM</td>
                    <td colspan="4">Kategori Penilaian</td>
                    <td rowspan="2">Keterangan</td>
                </tr>
                <tr>
                    <td>(81,26 - 100,00) SANGAT MEMUASKAN</td>
                    <td>(62,51 - 81,25) MEMUASKAN</td>
                    <td>(43,76 - 62,50) KURANG MEMUASKAN</td>
                    <td>(25 - 43,75) TIDAK MEMUASKAN</td>
                </tr>
                </thead>
                <tbody>
                    @if($selectedYear)
                    @php
                    $jumlah = 0;
                    @endphp
                    @foreach($skpd  as $key => $value)
                        @php
                            $calculatedValue = $bobot->where('id', $value->id)->first();
                            $calculatedValue = $calculatedValue != null ? round($calculatedValue->bobot * 100, 2) : 0;
                            $jumlah += $calculatedValue;
                            
                            $jumlah = round($jumlah,2);
                        @endphp
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td> {{$value->name}} </td>
                            <td>{{$calculatedValue}}</td>
                            <td> {{($calculatedValue >= 81.26 && $calculatedValue <= 100) ? '√' : ''}}</td>
                            <td> {{($calculatedValue >= 62.51 && $calculatedValue <= 81.25) ? '√' : ''}}</td>
                            <td> {{($calculatedValue >= 43.76 && $calculatedValue <= 62.50) ? '√' : ''}}</td>
                            <td> {{($calculatedValue >= 25 && $calculatedValue <= 43.75) ? '√' : ''}}</td>
                            <td style="min-width: 200px"></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="2">Jumlah Nilai Kota Bogor</td>
                        <td> {{$jumlah }}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2">Nilai Rata-Rata SKM Kota Bogor</td>
                        <td>{{ round($jumlah > 0 ? $jumlah / $skpd->count() : 0, 2) }}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    @endif
@endsection

@push('css')
    <style type="text/css">
        @media print {
            body * {
                visibility: hidden;
            }
            #section-to-print, #section-to-print * {
                visibility: visible;
            }
            #section-to-print {
                position: absolute;
                left: 0;
                top: 0;
            }
        }
    </style>
@endpush
