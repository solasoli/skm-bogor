@php

/** @var string $selectedSkpd */
/** @var array $skpd */

@endphp

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{{$title}}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="form-group" id="opd">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        OPD <span class="required">*</span> :
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select name='skpd' class="form-control select2" id="skpd">
                            @foreach($skpd as $id => $row)

                                @php
                                    $selected = $selectedSkpd == $row->id ? "selected" : "";
                                @endphp

                                <option value="{{$row->id}}" {{$selected}}>{{$row->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Tahun <span class="required">*</span> :
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select name='tahun' class="form-control select2" id="tahun_per_opd">
                        </select>
                    </div>
                </div>

                @if($requireSurvey ?? false)
                    <div class="form-group" id="survey">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                            Survey <span class="required">*</span> :
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name='survey' class="form-control select2" id="skpd_survey">
                            </select>
                        </div>
                    </div>
                @endif

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit</button>
                        @if($showPrintButton ?? false)
                            <div class="btn btn-warning" onclick="window.print()">Print</div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        get_tahun();
        function get_tahun(){
            $(".bulan-cover").hide();
            defaultSelectedSkpd = {{getLoggedInUser()->id_skpd}};
            selectedSkpd =  $('#skpd').val() == null ? defaultSelectedSkpd  : $('#skpd').val();
            $.get('{{url("")}}/laporan/srv_bulanan/get_tahun_select2/' + selectedSkpd, function(res){
                if(res != null){
                    var option = '';
                    var request = '{{$selectedYear}}';

                    $.each(res, function(idx, val){
                        if(request != '' && request == val.id){
                            var selected = 'selected';
                        } else {
                            var selected = '';
                        }
                        option += '<option value="'+ val.tahun +'" '+ selected +'>'+ val.tahun +'</option>';
                    });
                    $("#tahun_per_opd").html(option);

                    @if($requireSurvey ?? false)
                    if($('#tahun_per_opd').val())
                        getSurvey();
                    @endif
                }
            });
        }

        $("#skpd").on("change", function(){
            get_tahun();
            @if($requireSurvey ?? false)
            $("#skpd_survey").html(null);
            @endif
        });


        @if($requireSurvey ?? false)

        $("#tahun_per_opd").on("change", function() {
            getSurvey();
            @if($requireSurvey ?? false)
            $("#skpd_survey").html(null);
            @endif
        });


        function getSurvey() {
            selectedSkpd = $('#skpd').val();
            selectedYear = $('#tahun_per_opd').val();

            $.get(`{{url('laporan/opds')}}/${selectedSkpd}/survey?tahun=${selectedYear}`, function(res) {
                var option = '';
                var selectedSurvey = {{ $selectedSurvey ?? 'null' }};

                res.surveys.forEach(function(survey) {
                    var selected = selectedSurvey == survey.id ? 'selected' : null;
                    option += `<option value="${survey.id}" ${selected}>${survey.nama}</option>`;
                });
                $("#skpd_survey").html(option);

            });
        }

        @endif

    });
</script>
