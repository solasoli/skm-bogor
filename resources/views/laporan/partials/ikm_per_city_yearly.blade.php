<style>
    .carousel-indicators {
        position: relative;
        left: auto;
        margin-left: auto;
        width: auto;
    }
    .carousel-indicators .active {
        background-color: inherit;
    }
    
    .carousel-indicators li {
        background-color: inherit;
    }

    .carousel-list {
        justify-content: center; display: flex;
    }
</style>
@php
$ikmPerCityYearlyChunk = $ikmPerCityYearly->chunk(10);

@endphp

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>IKM Tingkat Kabupaten Tahun {{$selectedYear}}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <h4 align='center'><b>Rata-Rata Total IKM: {{round((($ikmPerCityYearly->sum('ikm') * 25) / $ikmPerCityYearly->count()), 2)}}</b></h4>
                <hr>
                <div id="carousel-example-generic" class="carousel slide">
                    
                    <ol class='carousel-list'>
                        @for($i = 1; $i <= count($ikmPerCityYearlyChunk); $i++)
                        <li data-target="#carousel-example-generic" data-slide-to="{{$i-1}}" class="btn btn-primary {{ $i == 1 ? 'active' : ''}}">{{ $i }}</li>
                        @endfor
                    </ol>

                    <div class="carousel-inner" role="listbox">
                        @for($i = 1; $i <= count($ikmPerCityYearlyChunk); $i++)
                        <div class="item {{$i == 1 ? 'active' : ''}}">
                            <canvas id="ikm_yearly_{{$i}}"></canvas>
                        </div>
                        @endfor
                    </div>
                                  
                </div>  
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('/vendors/Chart.js/dist/Chart.js') }}"></script>
<script src="{{ asset('/vendors/Chart.js/dist/Chart.min.js') }}"></script>
<script>
    $(function(){
        $(".carousel-list li").on('click', function(){
            $(".carousel-list li.active").removeClass('active');
            $(this).addClass('active');
        })


        @for($i = 0; $i < count($ikmPerCityYearlyChunk); $i++)
        var ikm_asc_{{ $i + 1 }} = document.getElementById('ikm_yearly_{{$i +1}}').getContext('2d');
        var chart_{{$i+1}} = new Chart(ikm_asc_{{ $i + 1 }}, {
            type: 'horizontalBar',
            data: {
                labels: [
                    @foreach($ikmPerCityYearlyChunk[$i] AS $row)
                        '{{$row->getSkpd()->name}}',
                    @endforeach
                ],
                datasets: [{
                    label: "",
                    data: [
                        @foreach($ikmPerCityYearlyChunk[$i] AS $row)
                            '{{$row->getIkm()}}',
                        @endforeach
                    ],
                    backgroundColor: [
                        @foreach($ikmPerCityYearlyChunk[$i] AS $row)
                            'rgba(54, 162, 235, 0.2)',
                        @endforeach
                    ],
                    borderColor: [
                        @foreach($ikmPerCityYearlyChunk[$i] AS $row)
                            'rgba(54, 162, 235, 1)',
                        @endforeach
                    ],
                    borderWidth: 1
                }]
            },
            options:{
                scales: {
                    xAxes : [{
                        ticks : {
                            max : 4,
                            min : 0
                        }
                    }]
                }
            }
        });
        @endfor
    });
</script>
