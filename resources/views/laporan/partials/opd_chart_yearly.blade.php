<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Survey dan IKM terendah tahun {{$selectedYear}}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                Berikut adalah urutan survey & IKM-nya berdasarkan score IKM, mulai dari yang terendah:
                <canvas id="ikm_asc"></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Survey dan IKM tertinggi tahun {{$selectedYear}}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                Berikut adalah urutan survey & IKM-nya berdasarkan score IKM, mulai dari yang tertinggi:
                <canvas id="ikm_desc"></canvas>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{ asset('/vendors/Chart.js/dist/Chart.js') }}"></script>
    <script src="{{ asset('/vendors/Chart.js/dist/Chart.min.js') }}"></script>
    <script>
        $(function(){
            var ikm_asc = document.getElementById('ikm_asc').getContext('2d');
            var chart_1 = new Chart(ikm_asc, {
                type: 'horizontalBar',
                data: {
                    labels: [
                        @foreach($ikm_tahunan_asc AS $row)
                            '{{$row->getSrvGroup()->nama}}',
                        @endforeach
                    ],
                    datasets: [{
                        label: 'Total IKM',
                        data: [
                            @foreach($ikm_tahunan_asc AS $row)
                                '{{$row->getIkm()}}',
                            @endforeach
                        ],
                        backgroundColor: [
                            @foreach($ikm_tahunan_asc AS $row)
                                'rgba(54, 162, 235, 0.2)',
                            @endforeach
                        ],
                        borderColor: [
                            @foreach($ikm_tahunan_asc AS $row)
                                'rgba(54, 162, 235, 1)',
                            @endforeach
                        ],
                        borderWidth: 1
                    }]
                },
                options:{
                    scales: {
                        xAxes : [{
                            ticks : {
                                max : 4,
                                min : 0
                            }
                        }]
                    }
                }
            });
            //
            var ikm_desc = document.getElementById('ikm_desc').getContext('2d');
            var chart_2 = new Chart(ikm_desc, {
                type: 'horizontalBar',
                data: {
                    labels: [
                        @foreach($ikm_tahunan_desc AS $row)
                            '{{$row->getSrvGroup()->nama}}',
                        @endforeach
                    ],
                    datasets: [{
                        label: 'Total IKM',
                        data: [
                            @foreach($ikm_tahunan_desc AS $row)
                                '{{$row->getIkm()}}',
                            @endforeach
                        ],
                        backgroundColor: [
                            @foreach($ikm_tahunan_desc AS $row)
                                'rgba(54, 162, 235, 0.2)',
                            @endforeach
                        ],
                        borderColor: [
                            @foreach($ikm_tahunan_desc AS $row)
                                'rgba(54, 162, 235, 1)',
                            @endforeach
                        ],
                        borderWidth: 1
                    }]
                },
                options:{
                    scales: {
                        xAxes : [{
                            ticks : {
                                max : 4,
                                min : 0
                            }
                        }]
                    }
                }
            });
        });
    </script>
@endpush
