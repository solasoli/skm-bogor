@extends('layouts.app')

@section('content')
    <div class="page-title">
        <div class="text-center">
            <h3>Rekapitulasi Nilai Survey Kepuasan Masyarakat Per Bulan</h3>
        </div>
    </div>

    <div class="clearfix"></div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="list-unstyled">
                @foreach ($errors->all() as $error)
                    <li>&#8226; {{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <p class="alert alert-success">{!! Session::get('message') !!}</p>
    @endif

    <form class="form-horizontal" method="GET" style="margin-top: 60px;">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Filter</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                Tahun <span class="required">*</span> :
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                @php
                                $nowYear = now()->year
                                @endphp

                                <select name='tahun' class="form-control select2" id="tahun_per_opd">

                                @for($i = 2018 ; $i <= $nowYear ; $i++)
                                    <option  {{$selectedYear == $i ? 'selected': null}} value="{{$i}}">{{$i}}</option>
                                @endfor
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                Bulan Mulai <span class="required">*</span> :
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <select name='monthFrom' class="form-control select2" id="tahun_per_opd">

                                    @for($i = 1 ; $i <= 12 ; $i++)
                                        <option {{$selectedMonthFrom == $i ? 'selected' : null}} value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                Bulan Akhir <span class="required">*</span> :
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <select name='monthTo' class="form-control select2" id="tahun_per_opd">

                                    @for($i = 1 ; $i <= 12 ; $i++)
                                        <option {{$selectedMonthTo == $i ? 'selected' : null}} value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <div class="btn btn-warning" onclick="window.print()">Print</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    @if(isset($_GET['tahun']))
    <div class="container x_panel" id="section-to-print">
        <div class="text-center">
            <h4>Rekapitulasi Nilai Survey Kepuasan Masyarakat Per Bulan</h4>
            @php
            function mouth($data)
            {
            switch ($data) {
            case '1': echo "January"; break; case '2': echo "February"; break; case '3': echo "March"; break; case '4': echo "April"; break; case '5': echo "May"; break; case '6': echo "June"; break; case '7': echo "July"; break; case '8': echo "August"; break; case '9': echo "September"; break; case '10': echo "October"; break; case '11': echo "November"; break; case '12': echo "December"; break;
            }
            }
            @endphp
            <h5>Periode {{ mouth($selectedMonthFrom) }} - {{ mouth($selectedMonthTo) }}</h5>
            <h5>Tahun {{ $selectedYear }}</h5>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <td rowspan="2">No</td>
                <td rowspan="2">Unsur Layanan</td>
                <td class="text-center" colspan="{{$monthlyUnsurPertanyaan->count()}}">Nilai Unsur Pelayanan</td>
            </tr>
            <tr>
                @foreach($monthlyUnsurPertanyaan as $value)
                    <td>{{date("F", mktime(0, 0, 0, $value->month, 10)) }}</td>
                @endforeach
            </tr>
            </thead>
            <tbody>
                @foreach($unsurPertanyaan as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$value->nama}}</td>
                        @foreach($monthlyUnsurPertanyaan as $monthlyValue)
                            @php 
                            $valueMonthly = $monthlyValue->data->where('unsurPertanyaan.id', $value->id)->first();
                            @endphp
                            <td>{{ !empty(trim($valueMonthly->value)) ? $valueMonthly->value : '-'}}</td>
                        @endforeach
                    </tr>
                @endforeach
                <tr>
                    <td colspan="2">Nilai Survey</td>
                    @foreach($monthlyUnsurPertanyaan as $value)
                        <td>{{ round($value->nilaiSurvey / (count($unsurPertanyaan) - 1), 2) }}</td>
                    @endforeach
                    <td> </td>
                    <td> </td>
                </tr>
                <tr>
                    <td colspan="2">Nilai SKM</td>
                    @foreach($monthlyUnsurPertanyaan as $value)
                        <td>{{ round(($value->nilaiSurvey / (count($unsurPertanyaan) - 1)) * 25, 2) }}</td>
                    @endforeach
                    <td> </td>
                    <td> </td>
                </tr>
                <tr>
                    <td colspan="2">Mutu Pelayanan</td>
                    @foreach($monthlyUnsurPertanyaan as $value)
                        <td>{{$value->mutuPelayanan}}</td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="2">Kinerja Unit Pelayanan</td>
                    @foreach($monthlyUnsurPertanyaan as $value)
                        <td>{{$value->kinerjaUnit}}</td>
                    @endforeach
                </tr>
            </tbody>
        </table>
    </div>
    @endif
    
@endsection

@push('css')
    <style type="text/css">
        @media print {
            body * {
                visibility: hidden;
            }
            #section-to-print, #section-to-print * {
                visibility: visible;
            }
            #section-to-print {
                position: absolute;
                left: 0;
                top: 0;
            }
        }
    </style>
@endpush
