@extends('layouts.app')
@section('content')

<div class="page-title">
    <div class="text-center">
        <h3>Unsur Pelayanan Survey</h3>
    </div>
</div>

<form class="form-horizontal" id="form-tertimbang" method="POST" style="margin-top: 50px;">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Unsur Tertimbang</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                            Tahun <span class="required">*</span> :
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name='tahun' class="form-control select2" id="tahun">
                            @foreach($tahun AS $idx => $val)
                            <option value="{{$val->tahun}}">{{$val->tahun}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<style>
    .gif-load {
        display: none;
        justify-content:center;
        align-items: center;
        flex-direction: column;
        padding: 40px;
        position: fixed;
        width: 100%;
        height: 100%;
        left: 0;
        top: 0;
        background-color: rgba(0, 0, 0, .5);
        z-index: 999;
        color: #FFF;
    }
    .gif-load h4 {
        margin-top: 50px;
    }
    .result {
        display: none;
        flex-direction: column;
    }
</style>

<div class="result">
    <div class="container x_panel">
        <div class="text-center">
        <h4>Nilai Unsur Pelayanan</h4>
        
        <h4>Tahun 2020</h4>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>No</th>
                <th>Unsur Pelayanan</th>
                <th>NRR</th>
                <th>NRR Tertimbang</th>
                <th>IKM UNSUR</th>
            </tr>
            @foreach($unsur as $val)
                @if($val->id != 10)
                <tr>
                    <td>{{$val->id}}</td>
                    <td>{{$val->nama}}</td>
                    <td class="nrr{{$val->id}}"></td>
                    <td class="nrr_tertimbang{{$val->id}}"></td>
                    <td class="ikm_unsur{{$val->id}}"></td>
                </tr>
                @endif
            @endforeach
            <tr>
                <th colspan="3">Jumlah NRR Tertimbang</th>
                <th class="jml_nrr_tertimbang"></th>
                <th></th>
            </tr>
            <tr>
                <th colspan="4">IKM Pelayanan</th>
                <th class="ikm_pelayanan"></th>
            </tr>
            </thead>
        </table>
    </div>
</div>

<!-- gif load for wait data successfully -->
<div class="gif-load">
    <img style="width: 100px" src="{{ asset('') }}img/loader.gif" alt="">
    <h4>Sedang mendapatkan seluruh data Kota Bogor</h4>
</div>

<script>
    $(document).ready(() => {

        var unsurLength = `{{count($unsur) - 1}}`;
        
        var persyaratan = [];
        var mekanisme = [];
        var penyelesaian = [];
        var biaya = [];
        var produk = [];
        var kompetensi = [];
        var perilaku = [];
        var penanganan = [];
        var sarana = [];
           
        
        
    $('#form-tertimbang').submit((e) => {
        e.preventDefault()
        
        // clear table after click submit
        for(let i=0; i<unsurLength; i++) {
            $(`.nrr${i+1}`).html('');
            $(`.nrr_tertimbang${i+1}`).html('');
            $(`.ikm_unsur${i+1}`).html('');
        }
        $('.jml_nrr_tertimbang').html('');
        $('.ikm_pelayanan').html('');
        

        // set display load and table
        $('.gif-load').css('display', 'flex');
        $('.result').css('display', 'none');
        
        var tahun = $('#tahun').val()
        $.ajax({
            url: '{{url("")}}/laporan/unsur_tertimbang/' + tahun,
            success: function(res) {

                // gouping unsur and push score to array
                $.each(res, (idx, val) => {
                    if(val.id_unsur == 1) {
                        persyaratan.push(val.score)
                    }else if(val.id_unsur == 2) {
                        mekanisme.push(val.score)
                    }else if(val.id_unsur == 3) {
                        penyelesaian.push(val.score)
                    }else if(val.id_unsur == 4) {
                        biaya.push(val.score);
                    }else if(val.id_unsur == 5) {
                        produk.push(val.score)
                    }else if(val.id_unsur == 6) {
                        kompetensi.push(val.score)
                    }else if(val.id_unsur == 7) {
                        perilaku.push(val.score)
                    }else if(val.id_unsur == 8) {
                        penanganan.push(val.score)
                    }else if(val.id_unsur == 9) {
                        sarana.push(val.score)
                    }
                })

                var nilaiUnsurPersyaratan = 0;
                var nilaiUnsurMekanisme = 0;
                var nilaiUnsurPenyelesaian = 0;
                var nilaiUnsurBiaya = 0;
                var nilaiUnsurProduk = 0;
                var nilaiUnsurKompetensi = 0;
                var nilaiUnsurPerilaku = 0;
                var nilaiUnsurPananganan = 0;
                var nilaiUnsurSarana = 0;

                var nilaiUnsurTertimbangPersyaratan = 0;
                var nilaiUnsurTertimbangMekanisme = 0;
                var nilaiUnsurTertimbangPenyelesaian = 0;
                var nilaiUnsurTertimbangBiaya = 0;
                var nilaiUnsurTertimbangProduk = 0;
                var nilaiUnsurTertimbangKompetensi = 0;
                var nilaiUnsurTertimbangPerilaku = 0;
                var nilaiUnsurTertimbangPananganan = 0;
                var nilaiUnsurTertimbangSarana = 0;

                // fungsi penjumlahan score (grouping)
                function sumScore(val, place) {
                    for(let i = 0; i < val.length; i++) {
                        place += parseFloat(val[i]);
                    }
                    place = place / val.length;

                    return place;
                }

                // fungsi mendapatkan nilai tertimbang
                function getNilaiTertimbang(place, val) {
                    place = val * 0.1111;

                    return place
                }

                nilaiUnsurPersyaratan += sumScore(persyaratan, nilaiUnsurPersyaratan);
                nilaiUnsurMekanisme += sumScore(mekanisme, nilaiUnsurMekanisme);
                nilaiUnsurPenyelesaian += sumScore(penyelesaian, nilaiUnsurPenyelesaian);
                nilaiUnsurBiaya += sumScore(biaya, nilaiUnsurBiaya);
                nilaiUnsurProduk += sumScore(produk, nilaiUnsurProduk);
                nilaiUnsurKompetensi += sumScore(kompetensi, nilaiUnsurKompetensi);
                nilaiUnsurPerilaku += sumScore(perilaku, nilaiUnsurPerilaku);
                nilaiUnsurPananganan += sumScore(penanganan, nilaiUnsurPananganan);
                nilaiUnsurSarana += sumScore(sarana, nilaiUnsurSarana);

                nilaiUnsurTertimbangPersyaratan += getNilaiTertimbang(nilaiUnsurTertimbangPersyaratan, nilaiUnsurPersyaratan);
                nilaiUnsurTertimbangMekanisme += getNilaiTertimbang(nilaiUnsurTertimbangMekanisme, nilaiUnsurMekanisme);
                nilaiUnsurTertimbangPenyelesaian += getNilaiTertimbang(nilaiUnsurTertimbangPenyelesaian, nilaiUnsurPenyelesaian);
                nilaiUnsurTertimbangBiaya += getNilaiTertimbang(nilaiUnsurTertimbangBiaya, nilaiUnsurBiaya);
                nilaiUnsurTertimbangProduk += getNilaiTertimbang(nilaiUnsurTertimbangProduk, nilaiUnsurProduk);
                nilaiUnsurTertimbangKompetensi += getNilaiTertimbang(nilaiUnsurTertimbangKompetensi, nilaiUnsurKompetensi);
                nilaiUnsurTertimbangPerilaku += getNilaiTertimbang(nilaiUnsurTertimbangPerilaku, nilaiUnsurPerilaku);
                nilaiUnsurTertimbangPananganan += getNilaiTertimbang(nilaiUnsurTertimbangPananganan, nilaiUnsurPananganan);
                nilaiUnsurTertimbangSarana += getNilaiTertimbang(nilaiUnsurTertimbangSarana, nilaiUnsurSarana);

                var arrayNilaiUnsurTertimbang = [];
                var arrayNilaiRatarata = [];
                var getNilaiNrr = 0;

                // memasukan nilai tertimbang ke array
                arrayNilaiUnsurTertimbang.push(
                    nilaiUnsurTertimbangPersyaratan,
                    nilaiUnsurTertimbangMekanisme,
                    nilaiUnsurTertimbangPenyelesaian,
                    nilaiUnsurTertimbangBiaya,
                    nilaiUnsurTertimbangProduk,
                    nilaiUnsurTertimbangKompetensi,
                    nilaiUnsurTertimbangPerilaku,
                    nilaiUnsurTertimbangPananganan,
                    nilaiUnsurTertimbangSarana)
                
                // memasukan nilai unsur ke array
                arrayNilaiRatarata.push(
                    nilaiUnsurPersyaratan, 
                    nilaiUnsurMekanisme,
                    nilaiUnsurPenyelesaian,
                    nilaiUnsurBiaya,
                    nilaiUnsurProduk,
                    nilaiUnsurKompetensi,
                    nilaiUnsurPerilaku,
                    nilaiUnsurPananganan,
                    nilaiUnsurSarana
                )
                
                // menjumlahakan nilai unsur tertimbang
                for(let i = 0; i < arrayNilaiUnsurTertimbang.length; i++) {
                    getNilaiNrr += parseFloat(arrayNilaiUnsurTertimbang[i]);
                }

                // get nilai ikm pelayanan
                var nilaiIkmPelayanan = getNilaiNrr * 25;

                // cek apakah data telah didapatkan atau belum
                if(nilaiIkmPelayanan != null) {

                    // set display setelah data didapatkan
                    $('.gif-load').css('display', 'none');
                    $('.result').css('display', 'flex');
                    
                    // masukan nilai nrr kedalam table
                    for(let i=0; i<arrayNilaiRatarata.length; i++) {
                        $(`.nrr${i+1}`).append(arrayNilaiRatarata[i].toFixed(2));
                    }

                    // masukan nilai nrr tertimbang kedalam table
                    for(let i=0; i<arrayNilaiUnsurTertimbang.length; i++) {
                        $(`.nrr_tertimbang${i+1}`).append(arrayNilaiUnsurTertimbang[i].toFixed(2));
                    }

                    // masukan nilai ikm unsur kedalam table
                    for(let i=0; i<arrayNilaiRatarata.length; i++) {
                        $(`.ikm_unsur${i+1}`).append((arrayNilaiRatarata[i]*25).toFixed(2))
                    }    

                    // masukan nilai jml nrr tertimbang kedalam table
                    $('.jml_nrr_tertimbang').append(getNilaiNrr.toFixed(2));

                    // masukan nilai ikm pelayanan kedalam table
                    $('.ikm_pelayanan').append(nilaiIkmPelayanan.toFixed(2));
                }
            }
        })
    })

})
</script>
@endsection