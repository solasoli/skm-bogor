@extends('layouts.app')

@section('content')
<style>
i.fa {
   font-size: 5em;
}
</style>
<div class="page-title">
  <div class="title_left">
    <h3>Laporan Survey Bulanan</h3>
  </div>
</div>
<div class="clearfix"></div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
                <li>&#8226; {{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session::has('message'))
  <p class="alert alert-success">{!! Session::get('message') !!}</p>
@endif
<form class="form-horizontal" method="POST">
  {{ csrf_field() }}
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Filter</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="form-group {{ must_show_skpd_form() ? '' : 'hide'}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            OPD <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='skpd' class="form-control select2" id="skpd">
              @foreach($skpd as $id => $row)

                @php
                  $selected = !is_null(old('skpd')) ? old('skpd') : (isset($request['skpd']) ? $request['skpd'] : '');
                  $selected = $selected == $row->id ? "selected" : "";
                @endphp

                <option value="{{$row->id}}" {{$selected}}>{{$row->name}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Tahun <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='tahun' class="form-control select2" id="tahun">
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Judul Survey <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control select2" name="survey" id="survey">
              <option value="">- Pilih OPD terlebih dahulu -</option>
            </select>
          </div>
        </div>

        <div class="form-group bulan-cover" style="display: none">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Bulan <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='bulan' class="form-control select2" id="bulan">
              @php
                $bulan = get_list_bulan_indonesia();
              @endphp

              @foreach($bulan as $idx => $row)
                @php
                $selected = isset($request['bulan']) && ($idx + 1)  == $request['bulan'] ? 'selected' : '';
                @endphp
                <option value='{{ ($idx + 1) / 1}}' {{$selected}}>{{$row}}</option>
              @endforeach
            </select>
          </div>
        </div>


        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <!-- <a href='{{url('')}}/srv/pertanyaan' class="btn btn-primary" type="button">Cancel</a> -->
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
</form>

@if(isset($request['survey']))
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Pengisian survey</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        Jumlah Responden Survey <b>{{$selected_survey->nama}}</b> Tahun <b>{{$request['tahun']}}</b> bulan <b>{{bulan_indonesia($request['bulan']/1)}}</b> adalah <b>{{$total_survey_tahunan}}</b><br>
        Rincinannya sebagai berikut :
        <canvas id="jml_srv_per_bulan" width="400" height="100"></canvas>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-5 col-sm-5 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Indeks Kepuasan Masyarakat</h2>
        <div class="clearfix"></div>
      </div>
      <div class="text-center">
        Indeks Kepuasan Masyarakan untuk survey <b>{{$selected_survey->nama}}</b> Bulan <b>{{bulan_indonesia($request['bulan'])}}</b> Tahun <b>{{$request['tahun']}}</b> adalah :<br><br>
        <div class="text-center">
          <?php
          if(strpos($penilaian['show_bintang'], '.') == true){
            $penilaian_x = explode('.', $penilaian['show_bintang']);
            for($i=1; $i<=$penilaian_x[0]; $i++){
              echo "<i class='fa fa-star' style='color:#ffac28;'></i>";
            }
            echo "<i class='fa fa-star-half-full' style='color:#ffac28;'></i>";

            for($i=1; $i<=3-$penilaian_x[0]; $i++){
              echo "<i class='fa fa-star-o' style='color:#ffac28;'></i>";
            }
          } else {
            if($penilaian['show_bintang'] > 0){
              for($i=1; $i<=$penilaian['show_bintang']; $i++){
                echo "<i class='fa fa-star' style='color:#ffac28;'></i>";
              }
              for($i=1; $i<=4-$penilaian['show_bintang']; $i++){
                echo "<i class='fa fa-star-o' style='color:#ffac28;'></i>";
              }
            } else {
              for($i=1; $i<=4; $i++){
                echo "<i class='fa fa-star-o' style='color:#ffac28;'></i>";
              }
            }
          }
          ?>
          <br>
          <h3>{{$penilaian['nilai_interval_konversi'] * 25 }}</h3>
          @php
              if($penilaian['nilai_interval_konversi'] >= 0 && $penilaian['nilai_interval_konversi'] <= 1.75 && $penilaian['nilai_interval_konversi'] != null)
                $nilai_interval_konversi_status = 'Tidak Memuaskan';
            elseIf($penilaian['nilai_interval_konversi'] > 1.75 && $penilaian['nilai_interval_konversi'] <= 2.50)
                $nilai_interval_konversi_status = 'Kurang Memuaskan';
            elseIf($penilaian['nilai_interval_konversi'] > 2.50 && $penilaian['nilai_interval_konversi'] <= 3.25)
                $nilai_interval_konversi_status = 'Memuaskan';
            elseIf($penilaian['nilai_interval_konversi'] > 3.25 && $penilaian['nilai_interval_konversi'] <= 4.00)
                $nilai_interval_konversi_status = 'Sangat Memuaskan';
            elseif($penilaian['nilai_interval_konversi'] == null)
                $nilai_interval_konversi_status = 'Tidak Ada Data';
            else
                $nilai_interval_konversi_status = '';
          @endphp
              <h3>{{$nilai_interval_konversi_status}}</h3>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-7 col-sm-7 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Total Score per Unsur Pertanyaan</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        Total Score per Unsur Pertanyaan dari survey <b>{{$selected_survey->nama}}</b> Bulan <b>{{bulan_indonesia($request['bulan'])}}</b> Tahun <b>{{$request['tahun']}}</b><br>
        adalah sebagai berikut :
        <canvas id="score_per_unsur"></canvas>
      </div>
    </div>
  </div>
</div>

<style>
  .head-responden {
    display: flex;
    justify-content: space-between;
  }
  .group {
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 8px;
  }
  .group .col-sm-3 {
    display: flex;
    justify-content: flex-end;
  }
  .group:last-child {
    margin-top: 20px;
    display: flex;
    align-items: center;
  }
</style>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <div class="head-responden">
            <h2>Identitas Responden</h2>
            
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#printResponden">
            <i class="fa fa-print" style="font-size: 16px;"></i>  print
            </button>

            <!-- Modal -->
            <div class="modal fade" id="printResponden" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <form method="POST" id="form_data_responden">
                    @csrf
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Print Data Responden</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      
                        <div class="row group">
                          <div class="col-sm-3">
                            <label for="skpd">OPD <span>*</span> : </label>
                          </div>
                          <div class="col-sm-9">
                            <select name='skpd' class="form-control select2" id="data_print_skpd">
                              @foreach($skpd as $id => $row)
                                @php
                                  $selected = !is_null(old('skpd')) ? old('skpd') : (isset($request['skpd']) ? $request['skpd'] : '');
                                  $selected = $selected == $row->id ? "selected" : "";
                                @endphp
                                <option value="{{$row->id}}" {{$selected}}>{{$row->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="row group">
                          <div class="col-sm-3">
                            <label for="tahun">Tahun <span>*</span> : </label>
                          </div>
                          <div class="col-sm-9">
                            <select name='tahun' class="form-control select2" id="data_print_tahun"></select>
                          </div>
                        </div>

                        <div class="row group">
                          <div class="col-sm-3">
                            <label for="survey">Judul Survey <span>*</span> : </label>
                          </div>
                          <div class="col-sm-9">
                            <select class="form-control select2" name="survey" id="data_print_survey">
                              <option value="">- Pilih OPD terlebih dahulu -</option>
                            </select>
                          </div>
                        </div>

                        <div class="row group">
                          <div class="col-sm-3">
                            <label for="bulan">Bulan <span>*</span> : </label>
                          </div>
                          <div class="col-sm-9">
                            <select name='bulan' class="form-control select2" id="data_print_bulan">
                              @php
                                $bulan = get_list_bulan_indonesia();
                              @endphp
                              @foreach($bulan as $idx => $row)
                                @php
                                $selected = isset($request['bulan']) && ($idx + 1)  == $request['bulan'] ? 'selected' : '';
                                @endphp
                                <option value='{{ ($idx + 1) / 1}}' {{$selected}}>{{$row}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="row group">
                          <div class="col-sm-6">
                            <input type="checkbox" name="all_responden" id="all_responden" onclick="checkAllResponden()" value="1">
                            <label for="all_responden">Cetak Seluruh Data Responden</label><br>
                            <input type="checkbox" name="all_survey" id="all_survey" onclick="checkAllSurvey()" value="1">
                            <label for="all_survey">Cetak Seluruh Data per-survey</label>
                          </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary"><i class="fa fa-print" style="font-size: 16px;"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

        </div>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="table table-bordered table-striped" id="users-table" style="width:100%">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Alamat</th>
              <th>Usia</th>
              <th>JK</th>
              <th>Pendidikan</th>
              <th>Pekerjaan</th>
              <th>E-mail</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
<div class="col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Kritik dan Saran</h2>

      <div class="clearfix"></div>
    </div>

    <style>
      #list_saran {
        width: 100%;
        height: 500px;
        overflow-y: scroll; /* Add the ability to scroll */
      }

      /* Hide scrollbar for Chrome, Safari and Opera */
      #list_saran::-webkit-scrollbar {
          display: none;
      }

      /* Hide scrollbar for IE, Edge and Firefox */
      #list_saran {
        -ms-overflow-style: none;  /* IE and Edge */
        scrollbar-width: none;  /* Firefox */
      }
    </style>

    <div class="x_content">
      <div class="dashboard-widget-content">
        <ul class="list-unstyled timeline widget" id="list_saran">
          @foreach($kritiksaran as $idx => $row)
          <li>
            <div class="block">
              <div class="block_content">
                <h2 class="title">
                  <p>{{ ucwords($row->kritik_saran) }}</p>
                </h2>
                <div class="byline">
                  <span>Pada {{ date("d" , strtotime($row->created_at)) . " " . bulan_indonesia(date("m", strtotime($row->created_at))) . " " .date("Y", strtotime($row->created_at)). " ".date("H:i", strtotime($row->created_at))}}</span> oleh <a>{{ $row->nama }}</a>
                </div>
              </div>
            </div>
          </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>
</div>

</div>

<script src="{{ asset('/vendors/Chart.js/dist/Chart.js') }}"></script>
<script src="{{ asset('/vendors/Chart.js/dist/Chart.min.js') }}"></script>
<script>
$(function(){
  @php
  $end_date = date("t", strtotime(date("{$request['tahun']}-{$request['bulan']}-d}")));
  @endphp
  var jml_srv_per_bulan = document.getElementById('jml_srv_per_bulan').getContext('2d');
  var chart_1 = new Chart(jml_srv_per_bulan, {
      type: 'bar',
      data: {
          labels: [
            @for($i=1; $i<= $end_date; $i++)
            '{{$i}}',
            @endfor
          ],
          datasets: [{
            label: 'Nilai IKM',
            data: [
              @for($i=1; $i<=$end_date; $i++)
              {{isset($hit_survey_bulanan[$i]) ? $hit_survey_bulanan[$i] : 0}},
              @endfor
            ],
            backgroundColor: [
              @for($i=1; $i<=$end_date; $i++)
              'rgba(54, 162, 235, 0.2)',
              @endfor
            ],
            borderColor: [
              @for($i=1; $i<=$end_date; $i++)
              'rgba(54, 162, 235, 1)',
              @endfor
            ],
            borderWidth: 1
          }]
      },
      options : {
       scales: {
           yAxes: [{
               ticks: {
                   max: 4,
                   beginAtZero: true,
                   userCallback: function(label, index, labels) {
                       // when the floored value is the same as the value we have a whole number
                       if (Math.floor(label) === label) {
                           return label;
                       }

                   },
               }
           }],
       },
   }
  });

  var score_per_unsur = document.getElementById('score_per_unsur').getContext('2d');
  var chart_2 = new Chart(score_per_unsur, {
      type: 'horizontalBar',
      data: {
          labels: [
            @foreach($total_bobot_per_unsur AS $row)
            '{{$row->unsur}}',
            @endforeach
          ],
          datasets: [{
            label:  'Total Score : {{ $total_bobot_per_unsur->sum('score') > 0 ? number_format($total_bobot_per_unsur->sum('score') / $total_bobot_per_unsur->count(), 2) : 0}}',
            data: [
              @foreach($total_bobot_per_unsur AS $row)
              '{{$row->score}}',
              @endforeach
            ],
            backgroundColor: [
              @foreach($total_bobot_per_unsur AS $row)
              'rgba(54, 162, 235, 0.2)',
              @endforeach
            ],
            borderColor: [
              @foreach($total_bobot_per_unsur AS $row)
              'rgba(54, 162, 235, 1)',
              @endforeach
            ],
            borderWidth: 1
          }]
      },
      options : {
       scales: {
           xAxes: [{
               ticks: {
                   beginAtZero: true,
                   userCallback: function(label, index, labels) {
                       // when the floored value is the same as the value we have a whole number
                       if (Math.floor(label) === label) {
                           return label;
                       }

                   },
               }
           }],
           yAxes: [{
               ticks: {
                  max: 4,
                   beginAtZero: true
               }
           }],
       },
   }
  });
});
</script>
@endif
<script>
$(function(){

  function get_survey(){
    $.post("/srv/grup_pertanyaan/get_grup_by_id_skpd", {"skpd" : $("#skpd").val(), "tahun": $("#tahun").val()},
    function(res){
      if(res.data != null){
        var option = '';
        var request = '{{isset($request["survey"]) ? $request["survey"] : ""}}';

        $.each(res.data, function(idx, val){
          if(request != '' && request == val.id){
            var selected = 'selected';
          } else {
            var selected = '';
          }
          option += '<option value="'+ val.id +'" '+selected+'>'+ val.nama +'</option>';
          $(".bulan-cover").show();
        });
        $("#survey").html(option);
      }
    });

  }


  get_tahun();
  function get_tahun(){
    $(".bulan-cover").hide();
    $.get('{{url("")}}/laporan/srv_bulanan/get_tahun_select2/' + $('#skpd').val(), function(res){
      console.log(res);
      if(res != null){
        var option = '';
        var request = '{{isset($request["tahun"]) ? $request["tahun"] : ""}}';

        $.each(res, function(idx, val){
          if(request != '' && request == val.id){
            var selected = 'selected';
          } else {
            var selected = '';
          }
          option += '<option value="'+ val.tahun +'" '+ selected +'>'+ val.tahun +'</option>';
        });
        $("#tahun").html(option);
      }
    }).done(function(){
      get_survey();
    });
  }

  $("#skpd").on("change", function(){
    get_tahun();
  });

  $("#tahun").on("change", function(){
    get_survey();
  });

  $('#users-table').DataTable({
      processing: true,
      serverSide: false,
      ajax: {
        url: '{{url("")}}/laporan/srv_bulanan/get_pensurvey/{{ isset($request['survey']) ? $request['survey'] : 0 }}',
        data: {
          'tahun': "{{isset($request['tahun']) ? $request['tahun'] : ''}}",
          'bulan': "{{isset($request['bulan']) ? $request['bulan'] : ''}}"
        }
      },
      columns: [
        { data: 'nama', name: 'sh.nama' },
        { data: 'alamat', name: 'alamat' },
        { data: 'usia', name: 'usia' },
        { data: null, searchable: false, orderable: false, render: function ( data, type, row ) {
          return data.jk == 'L' ? 'Laki-Laki' : "Perempuan";
        }},
        { data: 'pendidikan', name: 'p.nama' },
        { data: 'pekerjaan', name: 'pj.nama' },
        { data: 'email', name: 'email' },
      ]
  });

});


// data print 
$(function(){

function get_survey(){
  $.post("/srv/grup_pertanyaan/get_grup_by_id_skpd", {"skpd" : $("#data_print_skpd").val(), "tahun": $("#data_print_tahun").val()},
  function(res){
    if(res.data != null){
      var option = '';
      var request = '{{isset($request["survey"]) ? $request["survey"] : ""}}';

      $.each(res.data, function(idx, val){
        if(request != '' && request == val.id){
          var selected = 'selected';
        } else {
          var selected = '';
        }
        option += '<option value="'+ val.id +'" '+selected+'>'+ val.nama +'</option>';
      });
      $("#data_print_survey").html(option);
    }
  });

}


get_tahun();
function get_tahun(){
  $.get('{{url("")}}/laporan/srv_bulanan/get_tahun_select2/' + $('#data_print_skpd').val(), function(res){
    console.log(res);
    if(res != null){
      var option = '';
      var request = '{{isset($request["tahun"]) ? $request["tahun"] : ""}}';

      $.each(res, function(idx, val){
        if(request != '' && request == val.id){
          var selected = 'selected';
        } else {
          var selected = '';
        }
        option += '<option value="'+ val.tahun +'" '+ selected +'>'+ val.tahun +'</option>';
      });
      $("#data_print_tahun").html(option);
    }
  }).done(function(){
    get_survey();
  });
}

$("#data_print_skpd").on("change", function(){
  get_tahun();
});

$("#data_print_tahun").on("change", function(){
  get_survey();
});

});



function checkAllResponden() {

  const check_all_responden = document.querySelector('#all_responden');
  
  if(check_all_responden.checked == true) {
    $('#data_print_tahun').attr('disabled', 'on');
    $('#data_print_survey').attr('disabled', 'on');
    $('#data_print_bulan').attr('disabled', 'on');
    $('#all_survey').attr('checked', false);
  } else {
    $('#all_survey').attr('checked', true);
    $('#data_print_tahun').removeAttr('disabled');
    $('#data_print_survey').removeAttr('disabled');
    $('#data_print_bulan').removeAttr('disabled');
  }

}

function checkAllSurvey() {
  const check_all_survey = document.querySelector('#all_survey');

  if(check_all_survey.checked == true) {
    $('#data_print_tahun').removeAttr('disabled');
    $('#data_print_bulan').attr('disabled', 'on');
    $('#data_print_survey').removeAttr('disabled');
    $('#all_responden').attr('checked', false);
  }else {
    $('#all_responden').attr('checked', true);
    $('#data_print_skpd').removeAttr('disabled');
    $('#data_print_tahun').removeAttr('disabled');
    $('#data_print_survey').removeAttr('disabled');
    $('#data_print_bulan').removeAttr('disabled');
  }
}



$('#form_data_responden').submit(function() {
    var form_print = $('#form_data_responden');
    form_print.attr('action', '{{ url("/laporan/print_responden_bulanan") }}');
    form_print.attr('target', '_blank');
});

</script>
@endsection
