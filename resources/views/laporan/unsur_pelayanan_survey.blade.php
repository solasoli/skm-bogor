@extends('layouts.app')

@section('content')
    <div class="page-title">
        <div class="text-center">
            <h3>Unsur Pelayanan Survey</h3>
        </div>
    </div>

    <!--<div class="clearfix"></div>-->

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="list-unstyled">
                @foreach ($errors->all() as $error)
                    <li>&#8226; {{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <p class="alert alert-success">{!! Session::get('message') !!}</p>
    @endif

    <form class="form-horizontal" method="GET" id="form_filter" style="margin-top: 50px;">
        @include('laporan.partials.opd_options', [
          'title' => 'Filter',
          'requireSurvey' => true,
          'showPrintButton' => true
        ])
    </form>

    @if($selectedSurvey)
        <div class="container x_panel" id="section-to-print">
            <div class="text-center">
            <h4>Nilai Unsur Pelayanan</h4>
            <h4>{{ $selectedSkpdData->name }}</h4>
            <h4>{{ $selectedSurveyData->nama }}</h4>
            <h4>Tahun {{ $selectedYear }}</h4>
            </div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <td>No</td>
                    <td>Unsur Layanan</td>
                    <td>Nilai Unsur Pelayanan</td>
                    <td>Mutu Pelayanan</td>
                </tr>
                </thead>
                <tbody>
                @foreach($unsurPertanyaan as $key => $value)
                    @php
                    $serviceValue = $value->getAverageServiceValue([$selectedSurvey]);
                    @endphp
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$value->nama}}</td>
                        <td>{{ !empty(trim($serviceValue)) ? $serviceValue : '-' }}</td>
                        <td>{{$value->getAverageServiceStatus([$selectedSurvey])}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection



@push('css')
    <style type="text/css">
        @media print {
            body * {
                visibility: hidden;
            }
            #section-to-print, #section-to-print * {
                visibility: visible;
            }
            #section-to-print {
                position: absolute;
                left: 0;
                top: 0;
            }
        }
    </style>
@endpush

